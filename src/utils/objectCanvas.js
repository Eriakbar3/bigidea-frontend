export class Sticky {
	constructor(context, width, height, x, y) {
		this.context = context;
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;
	}
	template() {
		this.context.beginPath();
		this.context.shadowBlur = 5;
		this.context.shadowColor = 'rgba(0, 0, 0, 0.25)';
		this.context.fillStyle = '#BFF2D6';
		this.context.fillRect(this.x, this.y, this.width, this.height);
	}
}

export function DrawSticky(element, ctx) {
	const { x1, y1, x2, y2, style } = element;
	ctx.save();
	ctx.beginPath();
	ctx.shadowBlur = 5;
	ctx.shadowColor = 'rgba(0, 0, 0, 0.25)';
	ctx.fillStyle = style.fill;
	ctx.fillRect(x1, y1, x2 - x1, y2 - y1);
	ctx.restore();
}

export function drawPencil(element, ctx) {
	const { points, style } = element;
	ctx.save();
	ctx.beginPath();
	ctx.strokeStyle = style.stroke;
	ctx.lineCap = style.lineCap;
	ctx.lineWidth = style.lineWidth;
	for (let i = 0; i < points.length; i++) {
		const { x, y } = points[i];
		if (i === 0) ctx.moveTo(x, y);
		else {
			ctx.lineTo(x, y);
		}
	}
	ctx.stroke();
	ctx.restore();
}

export function Grid(context, bw, bh, p) {
	context.beginPath();
	context.shadowBlur = 0;
	for (var x = 0; x <= bw; x += 40) {
		context.moveTo(0.5 + x + p, p);
		context.lineTo(0.5 + x + p, bh + p);
	}
	for (x = 0; x <= bh; x += 40) {
		context.moveTo(p, 0.5 + x + p);
		context.lineTo(bw + p, 0.5 + x + p);
	}

	context.strokeStyle = '#F6F6F6';
	context.stroke();
}

export function Grid2(ctx, left, top, right, bottom) {
	ctx.clearRect(left, top, right - left, bottom - top);
	ctx.beginPath();
	ctx.shadowBlur = 0;
	ctx.lineWidth = 0.5;

	for (let x = left; x < right; x += 70) {
		ctx.moveTo(0.5 + x, top);
		ctx.lineTo(0.5 + x, bottom);
	}
	for (let y = top; y < bottom; y += 70) {
		ctx.moveTo(left, 0.5 + y);
		ctx.lineTo(right, 0.5 + y);
	}

	ctx.strokeStyle = '#AAAAAA';
	ctx.stroke();
}

export const drawLine = (x1, y1, x2, y2, ctx, style) => {
	ctx.save();
	ctx.lineWidth = style.lineWidth;
	ctx.strokeStyle = style.stroke;
	ctx.beginPath();
	ctx.moveTo(x1, y1);
	ctx.lineTo(x2, y2);
	ctx.stroke();
	ctx.restore();
};

export const drawRectangle = (element, ctx) => {
	const { x1, y1, x2, y2, style } = element;
	const w = x2 - x1;
	const h = y2 - y1;
	ctx.save();
	ctx.beginPath();
	ctx.lineWidth = style.lineWidth;
	ctx.strokeStyle = style.stroke;
	ctx.fillStyle = style.fill;
	ctx.fillRect(x1, y1, w, h);
	ctx.strokeRect(x1, y1, w, h);
	ctx.restore();
};

export const drawRoundedRectangle = (element, ctx) => {
	const { x1, y1, x2, y2, style } = element;
	const w = x2 - x1;
	const h = y2 - y1;
	ctx.save();
	ctx.lineWidth = style.lineWidth;
	ctx.strokeStyle = style.stroke;
	ctx.fillStyle = style.fill;
	ctx.beginPath();
	ctx.roundRect(x1, y1, w, h, (h < w ? h : w) * 0.2);
	ctx.fill();
	ctx.stroke();
	ctx.restore();
};

export const drawEllipse = (x, y, rx, ry, ctx, style) => {
	ctx.save();
	ctx.beginPath();
	ctx.lineWidth = style.lineWidth;
	ctx.strokeStyle = style.stroke;
	ctx.fillStyle = style.fill;
	ctx.ellipse(x, y, rx, ry, 0, 0, 2 * Math.PI);
	ctx.fill();
	ctx.stroke();
	ctx.restore();
};

export const drawTriangle = (element, ctx) => {
	const { x1, y1, x2, y2, style } = element;
	const w = x2 - x1;
	const h = y2 - y1;
	ctx.save();
	ctx.lineWidth = style.lineWidth;
	ctx.strokeStyle = style.stroke;
	ctx.fillStyle = style.fill;
	ctx.beginPath();
	ctx.moveTo(x1 + w / 2, y1);
	ctx.lineTo(x1 + w, y1 + h);
	ctx.lineTo(x1, y1 + h);
	ctx.closePath();
	ctx.fill();
	ctx.stroke();
	ctx.restore();
};

export const drawTrapezoid = (x, y, w, h, ctx, style) => {
	ctx.save();
	ctx.lineWidth = style.lineWidth;
	ctx.strokeStyle = style.stroke;
	ctx.fillStyle = style.fill;
	ctx.beginPath();
	ctx.moveTo(x + w / 6, y);
	ctx.lineTo(x + (5 / 6) * w, y);
	ctx.lineTo(x + w, y + h);
	ctx.lineTo(x, y + h);
	ctx.closePath();
	ctx.fill();
	ctx.stroke();
	ctx.restore();
};

export const drawArrowLeft = (x, y, w, h, ctx, style) => {
	ctx.save();
	ctx.lineWidth = style.lineWidth;
	ctx.strokeStyle = style.stroke;
	ctx.fillStyle = style.fill;
	ctx.beginPath();
	ctx.moveTo(x, h / 2 + y);
	ctx.lineTo((3 / 10) * w + x, y);
	ctx.lineTo((3 / 10) * w + x, h / 5 + y);
	ctx.lineTo(x + w, h / 5 + y);
	ctx.lineTo(x + w, (4 / 5) * h + y);
	ctx.lineTo((3 / 10) * w + x, (4 / 5) * h + y);
	ctx.lineTo((3 / 10) * w + x, y + h);
	ctx.closePath();
	ctx.fill();
	ctx.stroke();
	ctx.restore();
};

export const drawArrowRight = (x, y, w, h, ctx, style) => {
	ctx.save();
	ctx.lineWidth = style.lineWidth;
	ctx.strokeStyle = style.stroke;
	ctx.fillStyle = style.fill;
	ctx.beginPath();
	ctx.moveTo(x + w, h / 2 + y);
	ctx.lineTo((7 / 10) * w + x, y);
	ctx.lineTo((7 / 10) * w + x, h / 5 + y);
	ctx.lineTo(x, h / 5 + y);
	ctx.lineTo(x, (4 / 5) * h + y);
	ctx.lineTo((7 / 10) * w + x, (4 / 5) * h + y);
	ctx.lineTo((7 / 10) * w + x, y + h);
	ctx.closePath();
	ctx.fill();
	ctx.stroke();
	ctx.restore();
};

export const drawStar = (x, y, w, h, ctx, style) => {
	ctx.save();
	ctx.lineWidth = style.lineWidth;
	ctx.strokeStyle = style.stroke;
	ctx.fillStyle = style.fill;
	ctx.beginPath();
	ctx.moveTo(w / 2 + x, y);
	ctx.lineTo((3 / 5) * w + x, (3 / 8) * h + y);
	ctx.lineTo(w + x, (3 / 8) * h + y);
	ctx.lineTo((7 / 10) * w + x, (5 / 8) * h + y);
	ctx.lineTo((4 / 5) * w + x, h + y);
	ctx.lineTo(w / 2 + x, (3 / 4) * h + y);
	ctx.lineTo(w / 5 + x, h + y);
	ctx.lineTo((3 / 10) * w + x, (5 / 8) * h + y);
	ctx.lineTo(x, (3 / 8) * h + y);
	ctx.lineTo((2 / 5) * w + x, (3 / 8) * h + y);
	ctx.closePath();
	ctx.fill();
	ctx.stroke();
	ctx.restore();
};

export const drawParallelogram = (x, y, w, h, ctx, style) => {
	ctx.save();
	ctx.lineWidth = style.lineWidth;
	ctx.strokeStyle = style.stroke;
	ctx.fillStyle = style.fill;
	ctx.beginPath();
	ctx.moveTo(w / 4 + x, y);
	ctx.lineTo(w + x, y);
	ctx.lineTo((3 / 4) * w + x, h + y);
	ctx.lineTo(x, h + y);
	ctx.closePath();
	ctx.fill();
	ctx.stroke();
	ctx.restore();
};

export const drawHexagon = (x, y, w, h, ctx, style) => {
	ctx.save();
	ctx.lineWidth = style.lineWidth;
	ctx.strokeStyle = style.stroke;
	ctx.fillStyle = style.fill;
	ctx.beginPath();
	ctx.moveTo(w / 4 + x, y);
	ctx.lineTo((3 / 4) * w + x, y);
	ctx.lineTo(w + x, h / 2 + y);
	ctx.lineTo((3 / 4) * w + x, h + y);
	ctx.lineTo(w / 4 + x, h + y);
	ctx.lineTo(x, h / 2 + y);
	ctx.closePath();
	ctx.fill();
	ctx.stroke();
	ctx.restore();
};
