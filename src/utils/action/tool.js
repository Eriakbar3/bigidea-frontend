import {
	DrawSticky,
	drawArrowLeft,
	drawArrowRight,
	drawEllipse,
	drawHexagon,
	drawLine,
	drawParallelogram,
	drawPencil,
	drawRectangle,
	drawRoundedRectangle,
	drawStar,
	drawTrapezoid,
	drawTriangle
} from '../objectCanvas';

export const drawElement = (element, ctx, isWriting) => {
	const { x1, y1, x2, y2, type, style } = element;

	switch (type) {
		case 'line':
			drawLine(x1, y1, x2, y2, ctx, style);
			break;
		case 'rectangle':
			drawRectangle(element, ctx);
			if (element.text && !isWriting)
				writeText(
					{
						...element,
						x1: x1 + 8,
						y1: y1 + 8,
						x2: x2 - 8,
						y2: y2 - 8
					},
					ctx
				);
			break;
		case 'rounded rectangle':
			drawRoundedRectangle(element, ctx);
			if (element.text && !isWriting)
				writeText(
					{
						...element,
						x1: x1 + 8,
						y1: y1 + 8,
						x2: x2 - 8,
						y2: y2 - 8
					},
					ctx
				);
			break;
		case 'triangle':
			drawTriangle(element, ctx);
			if (element.text && !isWriting)
				writeText(
					{
						...element,
						x1: x1 + 4 + (x2 - x1) / 4,
						y1: y1 + 4 + (y2 - y1) / 2,
						x2: x2 - (x2 - x1) / 4,
						y2: y2 - 8
					},
					ctx
				);
			break;
		case 'trapezoid':
			drawTrapezoid(x1, y1, x2 - x1, y2 - y1, ctx, style);
			if (element.text && !isWriting)
				writeText(
					{
						...element,
						x1: x1 + (x2 - x1) / 6,
						y1: y1 + 8,
						x2: x2 - (x2 - x1) / 6,
						y2: y2 - 8
					},
					ctx
				);
			break;
		case 'arrow left':
			drawArrowLeft(x1, y1, x2 - x1, y2 - y1, ctx, style);
			break;
		case 'arrow right':
			drawArrowRight(x1, y1, x2 - x1, y2 - y1, ctx, style);
			if (element.text && !isWriting)
				writeText(
					{
						...element,
						x1: x1 + 8,
						y1: y1 + 8 + (y2 - y1) / 5,
						x2: x2 - 8 - ((x2 - x1) * 3) / 20,
						y2: y2 - 8 - (y2 - y1) / 5
					},
					ctx
				);
			break;
		case 'star':
			drawStar(x1, y1, x2 - x1, y2 - y1, ctx, style);
			if (element.text && !isWriting)
				writeText(
					{
						...element,
						x1: x1 + 8 + ((x2 - x1) * 3) / 10,
						y1: y1 + 8 + ((y2 - y1) * 3) / 8,
						x2: x2 - 8 - ((x2 - x1) * 3) / 10,
						y2: y2 - 8 - (y2 - y1) / 4
					},
					ctx
				);
			break;
		case 'parallelogram':
			drawParallelogram(x1, y1, x2 - x1, y2 - y1, ctx, style);
			if (element.text && !isWriting)
				writeText(
					{
						...element,
						x1: x1 + 4 + (x2 - x1) / 4,
						y1: y1 + 8,
						x2: x2 - 4 - (x2 - x1) / 4,
						y2: y2 - 8
					},
					ctx
				);
			break;
		case 'hexagon':
			drawHexagon(x1, y1, x2 - x1, y2 - y1, ctx, style);
			if (element.text && !isWriting)
				writeText(
					{
						...element,
						x1: x1 + (x2 - x1) / 4,
						y1: y1 + 8,
						x2: x2 - (x2 - x1) / 4,
						y2: y2 - 8
					},
					ctx
				);
			break;
		case 'circle':
			drawEllipse(
				x1 + (x2 - x1) / 2,
				y1 + (y2 - y1) / 2,
				Math.abs((x2 - x1) / 2),
				Math.abs((y2 - y1) / 2),
				ctx,
				style
			);
			if (element.text && !isWriting)
				writeText(
					{
						...element,
						x1: x1 + (x2 - x1) / 4 / Math.sqrt(2),
						y1: y1 + (y2 - y1) / 4 / Math.sqrt(2),
						x2: x2 - (x2 - x1) / 4 / Math.sqrt(2),
						y2: y2 - (y2 - y1) / 4 / Math.sqrt(2)
					},
					ctx
				);
			break;
		case 'text':
			if (!isWriting) writeText(element, ctx);
			break;
		case 'sticky':
			DrawSticky(element, ctx);
			if (element.text && !isWriting)
				writeText(
					{
						...element,
						x1: x1 + 8,
						y1: y1 + 8,
						x2: x2 - 8,
						y2: y2 - 8
					},
					ctx
				);
			break;
		case 'pencil':
			drawPencil(element, ctx);
			break;
		default:
			throw new Error(`Type not recognised: ${element.type}`);
	}
};

export const writeText = (element, ctx) => {
	const { x1, y1, x2, y2, text, style } = element;
	const { font, fontSize, color, fontWeight } = style;
	const width = x2 - x1;
	ctx.save();
	ctx.beginPath();
	ctx.rect(x1, y1, x2 - x1, y2 - y1);
	ctx.clip();
	ctx.textBaseline = 'top';
	ctx.fillStyle = color ? color : '#1E1E1E';
	ctx.font = `${fontWeight === 500 ? 'bold' : ''} ${
		fontSize ? fontSize : 16
	}px ${font ? font : 'sans-serif'}`;
	const lines = wrapText(ctx, text, width);
	let y = y1;
	for (const line of lines) {
		ctx.fillText(line, x1, y);
		y += fontSize ? fontSize : 16;
	}
	ctx.restore();
};

function wrapText(context, rawtext, maxWidth) {
	let lines = [];
	for (const text of rawtext.split('\n')) {
		let words = text.split(' '),
			line = '',
			i,
			test,
			metrics;

		for (i = 0; i < words.length; i++) {
			test = words[i];
			metrics = context.measureText(test);
			while (metrics.width > maxWidth) {
				// Determine how much of the word will fit
				test = test.substring(0, test.length - 1);
				metrics = context.measureText(test);
			}
			if (words[i] != test) {
				words.splice(i + 1, 0, words[i].substr(test.length));
				words[i] = test;
			}

			test = line + words[i] + ' ';
			metrics = context.measureText(test);

			if (metrics.width > maxWidth && i > 0) {
				lines.push(line);
				line = words[i] + ' ';
			} else {
				line = test;
			}
		}
		lines.push(line);
	}
	return lines;
}

const templateStyle = {
	fill: '#E4E4E4',
	stroke: '#1E1E1E',
	lineWidth: 1
};

const templateTextStyle = {
	font: 'sans-serif',
	color: '#1E1E1E',
	fontSize: 16,
	fontWeight: 400
};

const templatePencilStyle = {
	stroke: '#1E1E1E',
	lineWidth: 1,
	lineCap: 'butt'
};

export const createElement = (id, x1, y1, x2, y2, type, style) => {
	switch (type) {
		case 'line':
			return { id, x1, y1, x2, y2, type, style: templateStyle };
		case 'rectangle':
		case 'rounded rectangle':
		case 'triangle':
		case 'trapezoid':
		case 'arrow left':
		case 'arrow right':
		case 'star':
		case 'parallelogram':
		case 'hexagon':
		case 'circle':
		case 'sticker':
			return {
				id,
				x1,
				y1,
				x2,
				y2,
				type,
				text: '',
				style: { ...templateStyle, ...templateTextStyle }
			};
		case 'pencil':
			return {
				id,
				type,
				points: [{ x: x1, y: y1 }],
				style: templatePencilStyle
			};
		case 'text':
			return {
				id,
				type,
				x1,
				y1,
				x2,
				y2,
				text: '',
				style: templateTextStyle
			};
		case 'sticky':
			return { id, type, x1, y1, x2, y2, text: '', style };
		default:
			throw new Error(`Type not recognised: ${type}`);
	}
};
