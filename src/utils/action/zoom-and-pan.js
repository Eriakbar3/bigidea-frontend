const scaleMultiplayer = 0.1;

export const handleZoomIn = (dispatch) => {
	// dispatch.canvas.setZoomPoint({
	//     x: window.innerWidth / 2,
	// 	y: window.innerHeight / 2
	// });
	dispatch.canvas.scaleUp(scaleMultiplayer, {
		x: window.innerWidth / 2,
		y: window.innerHeight / 2
	});
};

export const handleZoomOut = (dispatch) => {
	// dispatch.canvas.setZoomPoint({
	//     x: window.innerWidth / 2,
	// 	y: window.innerHeight / 2
	// });
	dispatch.canvas.scaleDown(scaleMultiplayer, {
		x: window.innerWidth / 2,
		y: window.innerHeight / 2
	});
};

export const handleWheel = (e, dispatch) => {
	e.preventDefault();
	const wheelDelta = e.wheelDelta;
	const wheelDeltaX = e.wheelDeltaX;
	const wheelDeltaY = e.wheelDeltaY;
	const { clientX, clientY } = e;
	if (Math.abs(wheelDelta) < 180) {
		if (Math.abs(wheelDeltaX) > 0) {
			dispatch.canvas.setOffset({
				x: wheelDeltaX,
				y: 0
			});
		}
		if (Math.abs(wheelDeltaY) > 0) {
			dispatch.canvas.setOffset({
				x: 0,
				y: wheelDeltaY
			});
		}
	} else {
		// dispatch.canvas.setZoomPoint({ x: clientX, y: clientY });
		if (wheelDelta > 0)
			dispatch.canvas.scaleUp(scaleMultiplayer, {
				x: clientX,
				y: clientY
			});
		if (wheelDelta < 0)
			dispatch.canvas.scaleDown(scaleMultiplayer, {
				x: clientX,
				y: clientY
			});
	}
};
