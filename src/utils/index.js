export const getInitialName = (name) => {
	if (name) {
		const nameSplit = name.toUpperCase().split(' ');
		return `${nameSplit[0][0]}${
			nameSplit.length > 1 ? nameSplit[1][0] : ''
		}`;
	}
};

export function zoomAndPanCanvas(scale, x, y, canvas, ctx) {
	let matrix = ctx.getTransform();

	matrix = matrix.scale(scale, scale);

	const canvasWidth = canvas.width;
	const canvasHeight = canvas.height;

	const newCanvasWidth = canvasWidth * scale;
	const newCanvasHeight = canvasHeight * scale;

	const xOffset = (newCanvasWidth - canvasWidth) / 2;
	const yOffset = (newCanvasHeight - canvasHeight) / 2;

	x -= xOffset;
	y -= yOffset;

	matrix = matrix.translate(x, y);

	ctx.setTransform(matrix);
}

const distance = (a, b) => {
	return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
};

export const nearPoint = (x, y, x1, y1, name) => {
	return Math.abs(x - x1) < 5 && Math.abs(y - y1) < 5 ? name : null;
};

export const positionWithinElement = (x, y, element) => {
	let { x1, y1, x2, y2, type } = element;
	if (type !== 'line') {
		if (type === 'pencil') {
			const xArr = element.points.map((el) => el.x);
			const yArr = element.points.map((el) => el.y);
			x1 = Math.min(...xArr);
			y1 = Math.min(...yArr);
			x2 = Math.max(...xArr);
			y2 = Math.max(...yArr);
		}
		const topLeft = nearPoint(x, y, x1, y1, 'tl');
		const topRight = nearPoint(x, y, x2, y1, 'tr');
		const bottomLeft = nearPoint(x, y, x1, y2, 'bl');
		const bottomRight = nearPoint(x, y, x2, y2, 'br');
		const inside =
			x >= x1 && x <= x2 && y >= y1 && y <= y2 ? 'inside' : null;
		return topLeft || topRight || bottomLeft || bottomRight || inside;
	} else {
		const a = { x: x1, y: y1 };
		const b = { x: x2, y: y2 };
		const c = { x, y };
		const start = nearPoint(x, y, x1, y1, 'start');
		const end = nearPoint(x, y, x2, y2, 'end');
		const offset = distance(a, b) - (distance(a, c) + distance(b, c));
		const inside = Math.abs(offset) < 1 ? 'inside' : null;
		return start || end || inside;
	}
};

export const getElementAtPosition = (x, y, elements) => {
	return elements
		.map((element) => ({
			...element,
			position: positionWithinElement(x, y, element)
		}))
		.find((element) => element.position !== null);
};

export const getMousePosisitonOnSelectedElement = (x, y, element) => {
	return positionWithinElement(x, y, element);
};

export const findElements = (x, y, elements) => {
	for (let element of elements) {
		let { x1, y1, x2, y2, type } = element;
		if (
			type === 'rectangle' ||
			type === 'rounded rectangle' ||
			type === 'text' ||
			type === 'sticky'
		) {
			if (x + 1 >= x1 && x <= x2 && y + 1 >= y1 && y <= y2)
				return element;
		}
		if (type === 'line') {
			const a = { x: x1, y: y1 };
			const b = { x: x2, y: y2 };
			const c = { x, y };
			const offset = distance(a, b) - (distance(a, c) + distance(b, c));
			if (Math.abs(offset) < 1) return element;
		}
		if (type === 'triangle') {
			const w = x2 - x1;
			const h = y2 - y1;
			const m = (h / w) * 2;
			x1 = x1 + w / 2;
			const l1 = y - m * x >= y1 - m * x1 - 2;
			const l2 = y + m * x >= y1 + m * x1 - 2;
			const l3 = y <= h + y1;
			if (l1 && l2 && l3) return element;
		}
		if (type === 'circle') {
			const a = (x2 - x1 + 1) / 2;
			const b = (y2 - y1 + 1) / 2;
			x1 = x1 + (x2 - x1) / 2;
			y1 = y1 + (y2 - y1) / 2;
			if (((x - x1) / a) ** 2 + ((y - y1) / b) ** 2 <= 1) return element;
		}
		if (type === 'trapezoid') {
			const w = x2 - x1;
			const h = y2 - y1;
			const m = (h / w) * 6;
			const xl3 = x1 + w / 6;
			const xl4 = x1 + (w * 5) / 6;
			const l1 = y + 1 >= y1;
			const l2 = y <= h + y1;
			const l3 = y + 4 + m * x >= y1 + m * xl3;
			const l4 = y - m * x >= y1 - m * xl4 - 2;
			if (l1 && l2 && l3 && l4) return element;
		}
		if (type === 'parallelogram') {
			const w = x2 - x1;
			const h = y2 - y1;
			const m = (h / w) * 4;
			const xl3 = x1 + w / 4;
			const xl4 = x1 + w;
			const l1 = y + 2 >= y1;
			const l2 = y <= h + y1;
			const l3 = y + m * x >= y1 + m * xl3 - 2;
			const l4 = y + m * x <= y1 + m * xl4;
			if (l1 && l2 && l3 && l4) return element;
		}
		if (type === 'hexagon') {
			const w = x2 - x1;
			const h = y2 - y1;
			const m = (h / w) * 2;
			const xl3 = x1 + w / 4;
			const xl4 = x1 + w;
			const xl5 = x1;
			const xl6 = x1 + (w * 3) / 4;
			const l1 = y + 1 >= y1;
			const l2 = y <= h + y1;
			const l3 = y + m * x >= y1 + m * xl3 - 2;
			const l4 = y + m * x <= h / 2 + y1 + m * xl4;
			const l5 = y - m * x <= h / 2 + y1 - m * xl5;
			const l6 = y - m * x >= y1 - m * xl6 - 2;
			if (l1 && l2 && l3 && l4 && l5 && l6) return element;
		}
		if (type === 'arrow right') {
			const w = x2 - x1;
			const h = y2 - y1;
			const m = (h / w) * (5 / 3);
			const xl3 = x1 + w;
			const xl4 = x1 - 1 + (w * 7) / 10;
			const l1 = x <= xl4 ? y + 1 >= y1 + h / 5 : y >= y1;
			const l2 = x <= xl4 ? y <= y1 + (h * 4) / 5 : y <= y1 + h;
			const l3 = y + m * x <= h / 2 + y1 + m * xl3;
			const l4 = y - m * x >= y1 - m * xl4 - 3;
			const l5 = x >= x1;
			if (l1 && l2 && l5 && l3 && l4) return element;
		}
		if (type === 'star') {
			const w = x2 - x1;
			const h = y2 - y1;
			const m1 = (15 * h) / (4 * w);
			const m2 = (5 * h) / (6 * w);
			const m3 = m1;
			const m4 = m2;
			const l2 = y >= (3 * h) / 8 + y1 - 1;
			const l1 = y - m1 * x >= y1 - m1 * (x1 + w / 2) - 2;
			const l9 = y >= (3 * h) / 8 + y1 - 1;
			const l10 = y + m1 * x >= y1 + m1 * (x1 + w / 2) - 2;
			const l3 = y + m2 * x <= (3 * h) / 8 + y1 + m2 * x2;
			const l8 = y - m2 * x <= (3 * h) / 8 + y1 - m2 * x1 + 1;
			const l4 =
				y - m3 * x >= (5 * h) / 8 + y1 - m3 * (x1 + (7 * w) / 10);
			const l7 =
				y + m3 * x >= (5 * h) / 8 + y1 + m3 * (x1 + (3 * w) / 10) - 4;
			const l5 = y - m4 * x <= (6 * h) / 8 + y1 - m4 * (x1 + w / 2);
			const l6 = y + m4 * x <= (6 * h) / 8 + y1 + m4 * (x1 + w / 2);
			const l12 = l2 ? l2 : l1;
			const l109 = l9 ? l9 : l10;
			const l56 = x >= x1 + w / 2 ? l5 : l6;
			const l34 = y <= (5 * h) / 8 + y1 ? l3 : l4;
			const l78 = y >= (5 * h) / 8 + y1 ? l7 : l8;
			if (l56 && l34 && l78 && l12 && l109) return element;
		}
		if (type === 'pencil') {
			const { points } = element;
			if (
				points.find(
					(point) =>
						Math.abs(point.x - x) <= 2 && Math.abs(point.y - y) <= 2
				)
			)
				return element;
		}
	}
	return null;
};

export const adjustElementCoordinate = ({ x1, y1, x2, y2, type }) => {
	if (type !== 'line') {
		const minX = Math.min(x1, x2);
		const maxX = Math.max(x1, x2);
		const minY = Math.min(y1, y2);
		const maxY = Math.max(y1, y2);
		return { x1: minX, y1: minY, x2: maxX, y2: maxY };
	} else {
		if (x1 < x2 || (x1 === x2 && y1 < y2)) {
			return { x1, y1, x2, y2 };
		} else return { x1: x2, y1: y2, x2: x1, y2: y1 };
	}
};

export const cursorForPosition = (type) => {
	switch (type) {
		case 'tl':
		case 'br':
		case 'start':
		case 'end':
			return 'nwse-resize';
		case 'tr':
		case 'bl':
			return 'nesw-resize';
		default:
			return 'move';
	}
};

export const resizedCoordinates = (clientX, clientY, position, coordinates) => {
	const { x1, y1, x2, y2 } = coordinates;
	switch (position) {
		case 'tl':
		case 'start':
			return { x1: clientX, y1: clientY, x2, y2 };
		case 'tr':
			return { x1, y1: clientY, x2: clientX, y2 };
		case 'bl':
			return { x1: clientX, y1, x2, y2: clientY };
		case 'br':
		case 'end':
			return { x1, y1, x2: clientX, y2: clientY };
		default:
			return null;
	}
};
