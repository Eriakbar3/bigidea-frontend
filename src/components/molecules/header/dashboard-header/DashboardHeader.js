import React from 'react';
import InputSearch from '../../../atoms/input/InputSearch';
import Header from '../../../atoms/header/Header';
import UserProfileDropdown from '../../dropdown/user-profile/UserProfileDropdown';
import DashboardHelpDropdown from '../../dropdown/dashboard-help/DashboardHelpDropdown';
import DashboardNotificationDropdown from '../../dropdown/dashboard-notification/DashboardNotificationDropdown';

export default function DashboardHeader() {
	return (
		<Header>
			<img
				src={'/image/logo/Logo.svg'}
				alt="Big Idea"
				className="h-[34px] my-auto"
			/>
			<div className="flex gap-[14px]">
				<InputSearch placeholder="Search" />
				<svg
					className="my-auto"
					width="1"
					height="30"
					viewBox="0 0 1 30"
					fill="none"
					xmlns="http://www.w3.org/2000/svg">
					<rect width="1" height="30" fill="#EFEFEF" />
				</svg>
				<div className="my-auto mx-[10px]">
					<DashboardHelpDropdown />
				</div>
				<div className="my-auto mx-[10px]">
					<DashboardNotificationDropdown />
				</div>
				<svg
					className="my-auto"
					width="1"
					height="30"
					viewBox="0 0 1 30"
					fill="none"
					xmlns="http://www.w3.org/2000/svg">
					<rect width="1" height="30" fill="#EFEFEF" />
				</svg>
				<div className="my-auto">
					<UserProfileDropdown />
				</div>
			</div>
		</Header>
	);
}
