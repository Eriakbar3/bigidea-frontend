import React from 'react';
import './index.css';
import Logo from '../../../../assets/image/logo.png';
export default function Header() {
	return (
		<div className="head">
			<div className="logo-container">
				<img className="logo" src={Logo} />
			</div>
			<div className="breadcrumb">
				<div className="chev-text">
					<label>Team name</label>
				</div>
				<div className="chev-icon">
					<i className="bi-ic-chevron-right"></i>
				</div>
				<div className="chev-text">
					<label>Workspace name</label>
				</div>
				<div className="chev-icon">
					<i className="bi-ic-chevron-right"></i>
				</div>
				<div className="chev-text">
					<label>Whiteboard Name</label>
				</div>
			</div>
			<div className="head-right">
				<div className="btn-icon">
					<i className="bi-ic-save"></i>
				</div>
				<div className="btn-icon">
					<i className="bi-ic-download"></i>
				</div>
				<button className="btn-primary">Share</button>
				<div className="btn-icon">
					<i className="bi-ic-bell"></i>
				</div>
				<div className="btn-icon">
					<i className="bi-ic-help-circle"></i>
				</div>
			</div>
		</div>
	);
}
