import React from 'react';
import { useDispatch } from 'react-redux';
import ListDropdown from '../../../atoms/dropdown/ListDropdown';

export default function EditRole(props) {
	const dispatch = useDispatch();
	const menu = [
		{
			name : 'Change Role to '+props.role,
			key : 0
		},
		{
			name : 'Remove from Team',
			key : 1
		},
		
	];
	
	const handleEditRole = () => {
		dispatch.team.editRoleMember({
			"role":props.role.toLowerCase(),
			"id":props.id
		});
	};
	return (
		<ListDropdown
			menu={props.role_user === 'admin' ? (
				menu.map((i) => (
					<div
						className={i.key === 0 ? 'disable-edit-role' : 'cursor-pointer'}
						key={i.key} 
						onClick={i.key === 0 ? '' : () => props.setModalRemove()}
						>
						{i.name}
					</div>
				))
			) : (
				menu.map((i) => (
					<div
						key={i.key} 
						onClick={i.key === 0 ? () => handleEditRole() : () => props.setModalRemove()}
						>
						{i.name}
					</div>
				))
			)}
			role={props.role}>
			<div 
                className="bi-ic-more-vertical text-neutral-80 hover:text-primary "
                onClick={()=>props.onClick()}
            ></div>
		</ListDropdown>
	);
}
