import React, {useState} from 'react';
import ListDropdown from '../../../atoms/dropdown/ListDropdown';
import Tooltip from '../../../atoms/tooltip/Tooltip';
import ModalFeedback from '../../modal/modal-feedback';
import PopupFeedback from '../../modal/popup-feedback';

export default function DashboardHelpDropdown() {
	const [modalFeedback, setModalFeedback] = useState(false)
	const [popupFeedback, setPopupFeedback] = useState(false)
	const menu = [
		{
			name: 'Reset onboarding',
			url: ''
		},
		{
			name: 'Submit feedback',
			url: ''
		},
	];

	return (
		<>
			<ListDropdown
				menu={menu.map((i) => (
					<div
						key={i.name} 
						onClick={
							i.name === 'Submit feedback' ? () => setModalFeedback(true) 
														 : null}>
						{i.name}
					</div>
				))}>
				<Tooltip text="Help" placement="bottom">
					<div className="bi-ic-help-circle text-neutral-80 hover:text-primary"></div>
				</Tooltip>
			</ListDropdown>
			{modalFeedback && (
				<ModalFeedback
					cancel={() => setModalFeedback(false)}
					setPopupFeedback={() => setPopupFeedback(true)}
				/>
			)}
			{popupFeedback && (
				<PopupFeedback
					setPopupFeedback={() => setPopupFeedback(false)}/>
			)}
		</>
	);
}
