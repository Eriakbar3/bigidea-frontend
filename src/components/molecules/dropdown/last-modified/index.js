import React from 'react';
import { Link } from 'react-router-dom';
import ListDropdown from '../../../atoms/dropdown/ListDropdown';
import './style.css';
// import Tooltip from '../../../atoms/tooltip/Tooltip';

export default function LastModifiedDropdown() {
    const menu = [
        {
            name: 'Sort by',
            url: ''
        },
        {
            name: 'Name',
            url: ''
        },
        {
            name: 'Recently viewed',
            url: ''
        },
        {
            name: 'Last modified',
            url: ''
        },
        {
            name: 'Last modified by me',
            url: ''
        },
        {
            name: 'Order',
            url: ''
        },
        {
            name: 'Ascending',
            url: ''
        },
        {
            name: 'Descending',
            url: ''
        },
    ];

    return (
        <ListDropdown
            menu={menu.map((i) => (
                <Link to={i.url} key={i.name}>
                    {i.name}
                </Link>
            ))}>
            <div className='last-modified cursor-pointer'>
                <div className='bi-ic-sort mt-[2px] text-neutral-80'></div>
                <p className='text-m-reg text-neutral-100 mr-[10px] ml-[10px]'>
                    Last modified
                </p>
                <div className='bi-ic-chevron-down mt-[2px] text-neutral-80'></div>
            </div>
        </ListDropdown>
    );
}
