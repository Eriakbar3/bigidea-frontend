import React from 'react';
import Dropdown from '../../../atoms/dropdown/Dropdown';
import Tooltip from '../../../atoms/tooltip/Tooltip';
import './style.css';

export default function DashboardNotificationDropdown() {
	// const notification =[]

	const content = (
		<div className="notification">
			<div className="notif-header">
				<p className="text-l-med my-auto">Notifications</p>
				<p className="text-m-med text-neutral-60 my-auto">
					<span className="bi-ic-check align-middle mr-[10px]"></span>
					Mark as read
				</p>
			</div>
			<hr />
			<p className="text-center text-m-reg text-neutral-60 mt-[20px]">
				You don’t have any notifications yet. @mention someone from your
				team on Whiteboard or Project Management and check for updates
				here.
			</p>
		</div>
	);

	return (
		<Dropdown content={content}>
			<Tooltip text="Notification" placement="bottom">
				<div className="my-auto bi-ic-bell text-neutral-80 hover:text-primary mx-[10px]"></div>
			</Tooltip>
		</Dropdown>
	);
}
