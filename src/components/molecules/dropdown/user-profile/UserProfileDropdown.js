import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import Avatar from '../../../atoms/avatar';
import Button from '../../../atoms/button';
import Dropdown from '../../../atoms/dropdown/Dropdown';
import Tooltip from '../../../atoms/tooltip/Tooltip';

export default function UserProfileDropdown() {
	const dispatch = useDispatch();
	const { userData } = useSelector((state) => state.auth);
	const isLoading = useSelector(
		(state) => state.loading.effects.auth.signOut
	);

	const handleSignOut = () => {
		dispatch.team.emptyListTeam()
		dispatch.project.emptyProject()
		dispatch.auth.signOut()
	}
	const content = (
		<div className="p-[24px]">
			<Link to={`/my-profile`}>
				<div className="flex gap-[16px] mb-[22px]">
					<Avatar
						name={userData.name}
						width="50px"
						color={userData.image.color}
						fontStyle="heading-s-med"
						image_url={
							userData.image.url
								? `https://idea.bigbox.co.id/minio/profile/user/${userData.image.url}`
								: null
						}
					/>
					<div>
						<p className="head-s-med text-neutral-100">
							{userData.name}
						</p>
						<p className="text-l-reg text-neutral-80">
							{userData.email}
						</p>
					</div>
				</div>
			</Link>
			<svg
				className="mb-[24px]"
				width="302"
				height="1"
				viewBox="0 0 302 1"
				fill="none"
				xmlns="http://www.w3.org/2000/svg">
				<rect width="302" height="1" fill="#EFEFEF" />
			</svg>
			<Button
				htmlType="submit"
				type="secondary"
				fluid={true}
				isLoading={isLoading}
				onClick={() => handleSignOut()}>
				<span className="bi-ic-log-out align-middle mr-[8px]"></span>
				Sign Out
			</Button>
		</div>
	);
	return (
		<Dropdown content={content}>
			<Tooltip text="Profile" placement="bottom">
				<Avatar
					name={userData.name}
					width="32px"
					color={userData.image.color}
					fontStyle="text-m-med"
					image_url={
						userData.image.url
							? `https://idea.bigbox.co.id/minio/profile/user/${userData.image.url}`
							: null
					}
				/>
			</Tooltip>
		</Dropdown>
	);
}
