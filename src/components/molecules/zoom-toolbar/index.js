import React from 'react';
import './style.css';

export default function ZoomToolbar({ zoomIn, zoomOut, scale }) {
	return (
		<div className="zoom-grid-toolbar">
			<div className="grid-toolbar">
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					fill="none"
					xmlns="http://www.w3.org/2000/svg">
					<path
						fillRule="evenodd"
						d="M20 2H4C2.89543 2 2 2.89543 2 4V20C2 21.1046 2.89543 22 4 22H20C21.1046 22 22 21.1046 22 20V4C22 2.89543 21.1046 2 20 2ZM4 20V16H8V20H4ZM10 20H14V16H10V20ZM10 14H14V10H10V14ZM10 8H14V4H10V8ZM8 4V8H4V4H8ZM8 10V14H4V10H8ZM16 20V16H20V20H16ZM16 10V14H20V10H16ZM20 8H16V4H20V8Z"
						fill="#565656"
					/>
				</svg>
				<svg
					className="my-auto"
					width="21"
					height="22"
					viewBox="0 0 21 22"
					fill="none"
					xmlns="http://www.w3.org/2000/svg">
					<path
						d="M5.35543 7.50775C5.67896 7.16604 6.20349 7.16604 6.52701 7.50775L10.9118 12.139L15.2966 7.50775C15.6201 7.16604 16.1447 7.16604 16.4682 7.50775C16.7917 7.84946 16.7917 8.40347 16.4682 8.74518L11.4976 13.9952C11.1741 14.3369 10.6495 14.3369 10.326 13.9952L5.35543 8.74518C5.03191 8.40347 5.03191 7.84946 5.35543 7.50775Z"
						fill="#565656"
					/>
				</svg>
			</div>
			<div className="zoom-toolbar">
				<span className="bi-ic-minus my-auto" onClick={zoomOut}></span>
				<span className="bi-ic-plus my-auto" onClick={zoomIn}></span>
				<p className="text-l-med">{`${Math.floor(scale * 100)} %`}</p>
			</div>
		</div>
	);
}
