import React from 'react';
import Emoji100 from '../../../../assets/image/sticker/Emoji100.svg';
import Emojistar from '../../../../assets/image/sticker/Emojistar.svg';
import Emojimoney from '../../../../assets/image/sticker/Emojimoney.svg';
import Emojicup from '../../../../assets/image/sticker/Emojicup.svg';
import Emojisnoring from '../../../../assets/image/sticker/Emojisnoring.svg';
import Emojithumbup from '../../../../assets/image/sticker/Emojithumbup.svg';
import Emojithumbdown from '../../../../assets/image/sticker/Emojithumbdown.svg';
import Emojimetal from '../../../../assets/image/sticker/Emojimetal.svg';
import Emojipeace from '../../../../assets/image/sticker/Emojipeace.svg';
import Emojicrossfinger from '../../../../assets/image/sticker/Emojicrossfinger.svg';
import Greendot from '../../../../assets/image/sticker/Green dot.svg';
import Tealdot from '../../../../assets/image/sticker/Teal dot.svg';
import Bluedot from '../../../../assets/image/sticker/Blue dot.svg';
import Lavenderdot from '../../../../assets/image/sticker/Lavender dot.svg';
import Purpledot from '../../../../assets/image/sticker/Purple dot.svg';
import Pinkdot from '../../../../assets/image/sticker/Pink dot.svg';
import Peachdot from '../../../../assets/image/sticker/Peach dot.svg';
import Reddot from '../../../../assets/image/sticker/Red dot.svg';
import Orangedot from '../../../../assets/image/sticker/Orange dot.svg';
import Yellowdot from '../../../../assets/image/sticker/Yellow dot.svg';
import Vectorlovegreen from '../../../../assets/image/sticker/Vectorlovegreen.svg';
import Vectorloveteal from '../../../../assets/image/sticker/Vectorloveteal.svg';
import Vectorloveblue from '../../../../assets/image/sticker/Vectorloveblue.svg';
import Vectorlovelavender from '../../../../assets/image/sticker/Vectorlovelavender.svg';
import Vectorlovepurple from '../../../../assets/image/sticker/Vectorlovepurple.svg';
import Vectorlovepink from '../../../../assets/image/sticker/Vectorlovepink.svg';
import Vectorlovepeach from '../../../../assets/image/sticker/Vectorlovepeach.svg';
import Vectorlovered from '../../../../assets/image/sticker/Vectorlovered.svg';
import Vectorloveorange from '../../../../assets/image/sticker/Vectorloveorange.svg';
import Vectorloveyellow from '../../../../assets/image/sticker/Vectorloveyellow.svg';

export default function StickerDropdown() {
	return (
		<div className="sticker-dropdown">
			<img src={Emoji100} />
			<img src={Emojistar} />
			<img src={Emojimoney} />
			<img src={Emojicup} />
			<img src={Emojisnoring} />
			<img src={Emojithumbup} />
			<img src={Emojithumbdown} />
			<img src={Emojimetal} />
			<img src={Emojipeace} />
			<img src={Emojicrossfinger} />
			<img src={Greendot} />
			<img src={Tealdot} />
			<img src={Bluedot} />
			<img src={Lavenderdot} />
			<img src={Purpledot} />
			<img src={Pinkdot} />
			<img src={Peachdot} />
			<img src={Reddot} />
			<img src={Orangedot} />
			<img src={Yellowdot} />
			<img src={Vectorlovegreen} />
			<img src={Vectorloveteal} />
			<img src={Vectorloveblue} />
			<img src={Vectorlovelavender} />
			<img src={Vectorlovepurple} />
			<img src={Vectorlovepink} />
			<img src={Vectorlovepeach} />
			<img src={Vectorlovered} />
			<img src={Vectorloveorange} />
			<img src={Vectorloveyellow} />
		</div>
	);
}
