import React from 'react';
import { StickyNote } from '../../../atoms/icon';
import './style.css';
import { useDispatch } from 'react-redux';

export default function StickynoteDropdown() {
	const colors = [
		'#F2AFA6',
		'#4BC0C0',
		'#FC9A53',
		'#BFF2D6',
		'#F3E274',
		'#8CD0F1',
		'#FFE199',
		'#C2DCF8',
		'#FFF5D1',
		'#C2B8F5',
		'#DDCCBB',
		'#C3B4CF',
		'#AAAAAA',
		'#1E1E1E'
	];

	const dispatch = useDispatch();
	return (
		<div className="stickynote-dropdown">
			{colors.map((i) => (
				<div
					className="tool-icon"
					key={i}
					onClick={() => {
						dispatch.canvas.setTool('sticky');
						dispatch.canvas.setCursor('crosshair');
						dispatch.canvas.setInitEl({
							style: {
								fill: i,
								font: 'sans-serif',
								color: '#1E1E1E',
								fontSize: 16,
								fontWeight: 400
							}
						});
					}}>
					<StickyNote color={i} />
				</div>
			))}
		</div>
	);
}
