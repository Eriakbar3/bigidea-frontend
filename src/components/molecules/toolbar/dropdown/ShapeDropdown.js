import React from 'react';
import { useDispatch } from 'react-redux';
import {
	CircleIcon,
	HexagonIcon,
	LineIcon,
	ParallelogramIcon,
	RightArrowIcon,
	RoundedSquareIcon,
	SquareIcon,
	StarIcon,
	TrapezoidIcon,
	TriangleIcon
} from '../../../atoms/icon';
import './style.css';

export default function ShapeDropdown() {
	const dispatch = useDispatch();

	const handleClick = (shape) => {
		dispatch.canvas.setTool(shape);
		// dispatch.canvas.setAction('drawing');
		dispatch.canvas.setCursor('crosshair');
	};

	return (
		<div className="shape-dropdown">
			<div className="tools">
				<div
					className="tools-icon"
					onClick={() => handleClick('rectangle')}>
					<SquareIcon />
				</div>
				<div
					className="tools-icon"
					onClick={() => handleClick('rounded rectangle')}>
					<RoundedSquareIcon />
				</div>
				<div
					className="tools-icon"
					onClick={() => handleClick('hexagon')}>
					<HexagonIcon />
				</div>
				<div className="tools-icon" onClick={() => handleClick('line')}>
					<LineIcon />
				</div>
				<div
					className="tools-icon"
					onClick={() => handleClick('arrow right')}>
					<RightArrowIcon />
				</div>
			</div>
			<div className="tools">
				<div
					className="tools-icon"
					onClick={() => handleClick('circle')}>
					<CircleIcon />
				</div>
				<div
					className="tools-icon"
					onClick={() => handleClick('triangle')}>
					<TriangleIcon />
				</div>
				<div
					className="tools-icon"
					onClick={() => handleClick('trapezoid')}>
					<TrapezoidIcon />
				</div>
				<div
					className="tools-icon"
					onClick={() => handleClick('parallelogram')}>
					<ParallelogramIcon />
				</div>
				<div className="tools-icon" onClick={() => handleClick('star')}>
					<StarIcon />
				</div>
			</div>
		</div>
	);
}
