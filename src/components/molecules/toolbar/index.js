import React from 'react';
import { useDispatch } from 'react-redux';
import CanvasToolbarDropdown from '../../atoms/dropdown/CanvasToolbarDropdown';
import {
	FlowchartIcon,
	FrameIcon,
	PencilIcon,
	SelectIcon,
	SquareIcon,
	StickerIcon,
	StickyIcon,
	TempalateIcon,
	TextIcon,
	UploadIcon
} from '../../atoms/icon';
import ShapeDropdown from './dropdown/ShapeDropdown';
import StickerDropdown from './dropdown/StickerDropdown';
import StickynoteDropdown from './dropdown/StickynoteDropdown';

export default function ToolBar() {
	const dispatch = useDispatch();

	return (
		<div
			className="tools-object"
			onClick={() => dispatch.canvas.setSelectedElement(null)}>
			<div className="tools" id="tl">
				<div
					className="tools-icon"
					onClick={() => {
						dispatch.canvas.setCursor('default');
						dispatch.canvas.setTool('select');
					}}>
					<SelectIcon />
				</div>
				<div
					className="tools-icon"
					onClick={() => {
						dispatch.canvas.setTool('text');
						dispatch.canvas.setCursor('text');
					}}>
					<TextIcon />
				</div>
				<CanvasToolbarDropdown content={<StickynoteDropdown />}>
					<div className="tools-icon">
						<StickyIcon />
					</div>
				</CanvasToolbarDropdown>
				<CanvasToolbarDropdown content={<ShapeDropdown />}>
					<div className="tools-icon">
						<SquareIcon />
					</div>
				</CanvasToolbarDropdown>
				<div className="tools-icon">
					<TempalateIcon />
				</div>
			</div>
			<div className="tools" id="tr">
				<div
					className="tools-icon"
					onClick={() => {
						dispatch.canvas.setTool('pencil');
						dispatch.canvas.setCursor('crosshair');
					}}>
					<PencilIcon />
				</div>
				<div className="tools-icon">
					<FlowchartIcon />
				</div>
				<CanvasToolbarDropdown content={<StickerDropdown />}>
					<div className="tools-icon">
						<StickerIcon />
					</div>
				</CanvasToolbarDropdown>
				<div className="tools-icon">
					<FrameIcon />
				</div>
				<div className="tools-icon">
					<UploadIcon />
				</div>
			</div>
		</div>
	);
}
