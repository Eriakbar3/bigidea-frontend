import React from "react";
import "./style.css"
import InputInvitation from "../../../atoms/input/inputInvitation";

export default function ModalInvitation(props) {
    const {
        role, roles, onChangeRole, onChangeEmail,
        cancel, action, handleCopyToken
    } = props

    return (
        <div className="my-modal">
            <div className="content">
                <header>
                    <p className="head-s-med text-neutral-100">
                        Invite Member
                    </p>
                </header>
                <div >
                    <div className="mt-[20px]">
                        <p className='text-m-reg text-neutral-100 mb-[4px]' >
                            Email (you can invite multiple)
                        </p>
                        <div>
                            <InputInvitation
                                value={role}
                                options={roles}
                                onChangeRole={(e) => onChangeRole(e)}
                                onChangeEmail={(e) => onChangeEmail(e)}
                            />
                        </div>
                    </div>
                </div>
                <div className="flex" >
                    <div 
                        className="bi-ic-link cursor-pointer text-primary" 
                        onClick={() => handleCopyToken()}>
                        <span className='text-m-med ml-[9px]'>
                            Copy Team Code
                        </span>
                    </div>
                </div>
                <footer>
                    <button
                        onClick={() => cancel()}
                        className=" bg-surface-primary cursor-pointer"
                        style={{
                            width: "85px",
                            height: "40px",
                            boxShadow: '0px 1px 2px rgba(9, 45, 115, 0.12)',
                            borderRadius: '5px'
                        }}>
                        <p className="text-primary text-l-med">Cancel</p>
                    </button>
                    <button
                        onClick={() => action()}
                        className=" bg-primary cursor-pointer"
                        type='button'
                        style={{
                            width: "116px",
                            height: "40px",
                            boxShadow: '0px 1px 2px rgba(9, 45, 115, 0.12)',
                            borderRadius: '5px'
                        }}>
                        <p className="text-neutral-10 text-l-med">
                            Send Invite
                        </p>
                    </button>
                </footer>
            </div>
        </div>
    )
} 