import React from 'react';
import '../../../styles/boxModal.css';

export default function BoxModalDeleteAccount(props) {
	return (
		<div className="my-modal-v2">
			<div
				className="content"
				style={{
					width: props.width
				}}>
				<div
					className="flex justify-center"
					style={{
						padding: '28px 0 0 0'
					}}>
					<p className="text-l-med text-neutral-100">{props.title}</p>
				</div>
				<div
					style={{
						textAlign: props.textAlign,
						padding: '0 24px'
					}}>
					{props.content}
				</div>
				{props.footer && (
					<footer>
						<button
							onClick={() => props.cancel()}
							className=" bg-surface-primary cursor-pointer"
							style={{
								width: props.widthButton,
								height: props.heightButton,
								boxShadow: '0px 1px 2px rgba(9, 45, 115, 0.12)',
								borderRadius: '5px'
							}}>
							<p className="text-primary text-m-med">Cancel</p>
						</button>
						<button
							onClick={() => props.action()}
							className=" bg-primary cursor-pointer"
							type="button"
							style={{
								width: props.widthButton,
								height: props.heightButton,
								boxShadow: '0px 1px 2px rgba(9, 45, 115, 0.12)',
								borderRadius: '5px'
							}}>
							<p className="text-neutral-10 text-m-med">
								{props.actionName}
							</p>
						</button>
					</footer>
				)}
			</div>
		</div>
	);
}
