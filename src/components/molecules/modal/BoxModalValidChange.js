import React from 'react';
import '../../../styles/boxModal.css';

export default function BoxModalValidChange(props) {
	return (
		<div className="my-modal-v2">
			<div
				className="content"
				style={{
					width: '424px'
				}}>
				<div
					className="flex justify-center"
					style={{
						padding: '28px 0 0 0'
					}}>
					<p className="text-l-med text-neutral-100">{props.title}</p>
				</div>
				<div
					style={{
						textAlign: 'center',
						padding: '0 24px'
					}}>
					{props.content}
				</div>
				{props.footer && (
					<footer>
						<button
							onClick={() => props.cancel()}
							className=" bg-surface-primary cursor-pointer"
							style={{
								boxShadow: '0px 1px 2px rgba(9, 45, 115, 0.12)',
								borderRadius: '5px'
							}}>
							<p className="text-primary text-m-med px-[16px]">
								Cancel
							</p>
						</button>
						<button
							onClick={() => props.discard()}
							className=" bg-surface-primary cursor-pointer"
							style={{
								boxShadow: '0px 1px 2px rgba(9, 45, 115, 0.12)',
								borderRadius: '5px'
							}}>
							<p className="text-primary text-m-med px-[16px]">
								Discard
							</p>
						</button>
						<button
							onClick={() => props.action()}
							className=" bg-primary cursor-pointer"
							type="submit"
							style={{
								boxShadow: '0px 1px 2px rgba(9, 45, 115, 0.12)',
								borderRadius: '5px'
							}}>
							<p className="text-neutral-10 text-m-med px-[16px] py-[6px]">
								Yes, save it
							</p>
						</button>
					</footer>
				)}
			</div>
		</div>
	);
}
