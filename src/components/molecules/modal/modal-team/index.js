import React from "react";
import "./style.css"
import ProjectListButton from "../../project-list-btn";

export default function ModalTeam(props) {
    return (
        <div className="my-modal">
            <div className="content">
                <header className="flex justify-between">
                    <p className="head-s-med text-neutral-100">
                        Create / Join Team
                    </p>
                    <svg 
                        onClick={() => props.cancel()}
                        className="-mr-[15px] mt-[6px] cursor-pointer" 
                        width="20" 
                        height="20" 
                        viewBox="0 0 20 20" 
                        fill="none" 
                        xmlns="http://www.w3.org/2000/svg">
                        <path 
                            d="M15.5892 4.41083C15.9147 4.73626 15.9147 5.2639 15.5892 5.58934L5.58925 15.5893C5.26381 15.9148 4.73617 15.9148 4.41073 15.5893C4.0853 15.2639 4.0853 14.7363 4.41073 14.4108L14.4107 4.41083C14.7362 4.08539 15.2638 4.08539 15.5892 4.41083Z" 
                            fill="#737373" />
                        <path 
                            d="M4.41073 4.41083C4.73617 4.08539 5.26381 4.08539 5.58925 4.41083L15.5892 14.4108C15.9147 14.7363 15.9147 15.2639 15.5892 15.5893C15.2638 15.9148 14.7362 15.9148 14.4107 15.5893L4.41073 5.58934C4.0853 5.2639 4.0853 4.73626 4.41073 4.41083Z" 
                            fill="#737373" />
                    </svg>
                </header>
                <div className="mt-[17px]"
                    onClick={() => {props.setCreateModal()}}>
                    <ProjectListButton
                        icon="/image/icons/ic-create-team.svg"
                        title="Create Team"
                        subtitle="Create your new team!"
                    />
                </div>
                <div className="mt-[12px]"
                    onClick={() => {props.setJoinModal()}}>
                    <ProjectListButton
                        icon="/image/icons/ic-join-team.svg"
                        title="Join Team"
                        subtitle="Join the existing team with this code!"
                    />
                </div>
                <footer></footer>
            </div>
        </div>
    )
} 