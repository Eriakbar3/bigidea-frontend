import React from "react";
import "./style.css"

export default function ModalCreateTeam(props) {
    return (
        <div className="my-modal">
            <div className="content">
                <header className="flex justify-between">
                    <p className="head-s-med text-neutral-100">
                        Create Team
                    </p>
                </header>
                <form onSubmit={props.handleSubmit}>
                    <p className="text-neutral-100 text-m-reg mt-[20px] mb-[4px]">
                        Team name (required)
                    </p>
                    <input
                        type="text"
                        name="name"
                        placeholder="Team name"
                        value={props.teamName}
                        onChange={(e) => props.setTeamName(e.target.value)}
                        required
                    />
                    <p className="text-neutral-100 text-m-reg mt-[20px] mb-[4px]">
                        Team description
                    </p>
                    <textarea
                        className="h-[90px]"
                        type="text"
                        name="desc"
                        placeholder="Team description"
                        value={props.teamDesc}
                        onChange={(e) => props.setTeamDesc(e.target.value)}
                    />
                    <footer>
                        <button
                            onClick={() => props.cancel()}
                            className=" bg-surface-primary cursor-pointer"
                            style={{
                                padding: "8px 16px 8px 16px",
                                boxShadow: '0px 1px 2px rgba(9, 45, 115, 0.12)',
                                borderRadius: '5px'
                            }}>
                            <p className="text-primary text-l-med">Cancel</p>
                        </button>
                        <button
                            className=" bg-primary cursor-pointer"
                            type='submit'
                            style={{
                                padding: "8px 16px 8px 16px",
                                boxShadow: '0px 1px 2px rgba(9, 45, 115, 0.12)',
                                borderRadius: '5px'
                            }}>
                            <p className="text-neutral-10 text-l-med">
                                Create
                            </p>
                        </button>
                    </footer>
                </form>
            </div>
        </div>
    )
} 