import React, { useState } from "react";
import "./style.css"
import InputInvitation from "../../../atoms/input/inputInvitation";
import Avatar from "../../../atoms/avatar";

export default function ModalInviteWhiteboard(props) {
    const {
        role, roles, onChangeRole, onChangeEmail, cancel,
        // action, handleCopyToken
    } = props
    const [hideOptionsWhiteboard, setHideOptionsWhiteboard] = useState(true);
    const [hideOptionsUser, setHideOptionsUser] = useState(true);
    const [roleUser, setRoleUser] = useState('Can edit')
    const [roleWhiteboard, setRoleWhiteboard] = useState('Anyone with the link')
    const roles_whiteboard = [
        'Anyone with the link',
        'Only people invited to this file'
    ];
    const role_user = [
        'Can edit',
        'Can view'
    ];

    return (
        <div className="my-modal-invite-canvas">
            <div className="content">
                <header>
                    <p className="head-s-med text-neutral-100">
                        Invite to Online Whiteboard
                    </p>
                </header>
                <div >
                    <div className="mt-[20px]">
                        <p className='text-m-reg text-neutral-100 mb-[4px]' >
                            Email (you can invite multiple)
                        </p>
                        <div>
                            <InputInvitation
                                value={role}
                                options={roles}
                                onChangeRole={(e) => onChangeRole(e)}
                                onChangeEmail={(e) => onChangeEmail(e)}
                            />
                        </div>
                    </div>
                </div>
                <div className="flex justify-between">
                    <div
                        className='cursor-pointer text-neutral-80'
                        onClick={() => setHideOptionsWhiteboard(hideOptionsWhiteboard ? false : true)}
                        onBlur={() => {
                            setTimeout(function () {
                                setHideOptionsWhiteboard(false);
                            }, 500);
                        }}>
                        <div className='flex' >
                            <div className={roleWhiteboard === "Anyone with the link" ?
                                "bi-ic-link" : "bi-ic-lock"}>
                            </div>
                            <p className='text-m-reg text-neutral-100 mr-[12px] ml-[9px]'>
                                {roleWhiteboard}
                            </p>
                            <div className='bi-ic-chevron-down mt-[2px]'></div>
                        </div>
                    </div>
                    <div
                        className='cursor-pointer text-neutral-80'
                        onClick={() => setHideOptionsUser(hideOptionsUser ? false : true)}
                        onBlur={() => {
                            setTimeout(function () {
                                setHideOptionsUser(false);
                            }, 500);
                        }}>
                        <div className='flex' >
                            <p className='text-m-reg text-neutral-100 mr-[12px] ml-[9px]'>
                                {roleUser}
                            </p>
                            <div className='bi-ic-chevron-down mt-[2px]'></div>
                        </div>
                    </div>
                </div>
                <div className="flex justify-between mt-[16px]">
                    <div className="flex">
                        <Avatar
                            name={"Gia Tama"}
                            width="25px"
                            color={0}
                            // image_url={
                            //     detail_teams.team.image.url
                            //         ? `https://idea.bigbox.co.id/minio/profile/team/${detail_teams.team.image.url}`
                            //         : null
                            // }
                            fontStyle="text-s-med"
                        />
                        <p className="text-m-reg text-neutral-100 mt-[2px] ml-[8px]">
                            Gia Tama
                        </p>
                    </div>
                    <p className="text-m-reg">
                        Owner
                    </p>
                </div>
                <footer>
                    <div
                        className="bi-ic-link cursor-pointer text-primary"
                    // onClick={() => handleCopyToken()}
                    >
                        <span className='text-m-med ml-[9px]'>
                            Copy Link
                        </span>
                    </div>
                    <div className="flex">
                        <button
                            onClick={() => cancel()}
                            className=" bg-surface-primary cursor-pointer"
                            style={{
                                width: "85px",
                                height: "40px",
                                boxShadow: '0px 1px 2px rgba(9, 45, 115, 0.12)',
                                borderRadius: '5px'
                            }}>
                            <p className="text-primary text-l-med">Cancel</p>
                        </button>
                        <button
                            // onClick={() => action()}
                            className=" bg-primary cursor-pointer ml-[15px]"
                            type='button'
                            style={{
                                width: "116px",
                                height: "40px",
                                boxShadow: '0px 1px 2px rgba(9, 45, 115, 0.12)',
                                borderRadius: '5px'
                            }}>
                            <p className="text-neutral-10 text-l-med">
                                Send Invite
                            </p>
                        </button>
                    </div>
                </footer>
                <div
                    className="input-select-options"
                    hidden={hideOptionsWhiteboard}
                    onClick={() => setHideOptionsWhiteboard(true)}>
                    {roles_whiteboard.map((i) => (
                        <div
                            className="flex"
                            key={i}>
                            <div className={i === "Anyone with the link" ?
                                "bi-ic-link mt-[7px] mr-[10px]" :
                                "bi-ic-lock mt-[7px] mr-[10px]"}>
                            </div>
                            <p

                                className="item text-m-reg text-neutral-100"
                                onClick={() => setRoleWhiteboard(i)}
                            >
                                {i}
                            </p>
                        </div>
                    ))}
                </div>
                <div
                    className="input-select-options-invite"
                    hidden={hideOptionsUser}
                    onClick={() => setHideOptionsUser(true)}>
                    {role_user.map((i) => (
                        <div
                            className="flex"
                            key={i}>
                            <p

                                className="item text-m-reg text-neutral-100"
                                onClick={() => setRoleUser(i)}
                            >
                                {i}
                            </p>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    )
} 