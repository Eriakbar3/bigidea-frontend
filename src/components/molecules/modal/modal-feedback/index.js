import React, { useState } from "react";
import { useDispatch } from "react-redux";
import "./style.css"
import { RatingIconOff, RatingIconOn } from '../../../atoms/icon/index';

export default function ModalFeedback(props) {
    const { cancel } = props;
    const dispatch = useDispatch();
    const [feedback, setFeedback] = useState('');
    const [rate, setRate] = useState(1);
    const [rating, setRating] = useState([1,0,0,0,0]);
    const valueRate = [1, 2, 3, 4, 5];

    const handleRating = (x) => {
        const array = [];
        if (x !== 0) {
            for (let i = 0; i < 5; i++) {
                if (i < x){
                    array.push(i + 1)
                }else[
                    array.push(0)
                ]
            }
            setRating(array)
            setRate(x)
        }else{
            setRating(array)
            setRate(array.length)
        }
    }

    const handleSendFeedback = () => {
        if (feedback !== ''){
            const data = {feedback:feedback, rate:rate};
            dispatch.feedback.addFeedback(data);
            props.setPopupFeedback()
            cancel()
        }else{
            dispatch.feedback.updateFail('feedback tidak boleh kosong')
        }
        
    }
    return (
        <div className="my-modal-feedback">
            <div className="content">
                <header>
                    <p className="head-s-med text-neutral-100">
                        Submit feedback
                    </p>
                </header>
                <div >
                    <div className="mt-[20px]">
                        <p className='text-m-reg text-neutral-100 mb-[4px]' >
                            How satisfied are you using BigIdea
                        </p>
                        <div className="flex gap-[8px]">
                            {valueRate.map((i) => (
                                <div
                                    key={i}
                                    className="cursor-pointer"
                                    onClick={() => handleRating(i)}
                                    >
                                    {i === rating[i-1] ? <RatingIconOn/> : <RatingIconOff />}
                                </div>
                            ))}
                        </div>
                        <textarea
                            className="h-[90px] mt-[11px]"
                            type="text"
                            name="feedback"
                            placeholder="Tell us more..."
                            value={feedback}
                            onChange={(e) => setFeedback(e.target.value)}
                        />
                    </div>
                </div>
                <footer>
                    <button
                        onClick={() => cancel()}
                        className=" bg-surface-primary cursor-pointer"
                        style={{
                            width: "85px",
                            height: "40px",
                            boxShadow: '0px 1px 2px rgba(9, 45, 115, 0.12)',
                            borderRadius: '5px'
                        }}>
                        <p className="text-primary text-l-med">Cancel</p>
                    </button>
                    <button
                        onClick={() => handleSendFeedback()}
                        className=" bg-primary cursor-pointer"
                        type='button'
                        style={{
                            width: "71px",
                            height: "40px",
                            boxShadow: '0px 1px 2px rgba(9, 45, 115, 0.12)',
                            borderRadius: '5px'
                        }}>
                        <p className="text-neutral-10 text-l-med">
                            Send
                        </p>
                    </button>
                </footer>
            </div>
        </div>
    )
} 