import React from 'react';
import '../../../styles/boxModal.css';

export default function BoxModal(props) {
	return (
		<div className="my-modal">
			<div
				className="content"
				style={{
					width: props.width
				}}>
				<div
					className="flex justify-start"
					style={{
						padding: '20px 0 0 25px'
					}}>
					<p className="head-s-med text-neutral-100">{props.title}</p>
				</div>
				<div
					style={{
						textAlign: props.textAlign,
						padding: '0 24px'
					}}>
					{props.content}
				</div>
				{props.footer && (
					<footer className="flex justify-center">
						<button
							onClick={() => props.cancel()}
							className="btn bg-surface-primary text-primary cursor-pointer"
							style={{
								width: props.widthButton,
								height: props.heightButton,
								boxShadow: '0px 1px 2px rgba(9, 45, 115, 0.12)',
								borderRadius: '5px'
							}}>
							Cancel
						</button>
						<button
							onClick={() => props.action()}
							className="btn bg-primary cursor-pointer"
							style={{
								width: props.widthButton,
								height: props.heightButton,
								boxShadow: '0px 1px 2px rgba(9, 45, 115, 0.12)',
								borderRadius: '5px'
							}}>
							<p className="text-neutral-10">
								{props.actionName}
							</p>
						</button>
					</footer>
				)}
			</div>
		</div>
	);
}
