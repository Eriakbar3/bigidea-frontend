import React, { useEffect } from "react";
import "./style.css"

export default function PopupFeedback(props) {
    useEffect(() => {
        setTimeout(() => {
			props.setPopupFeedback()
		}, 2500);
        
    },[])
    return (
        <div className="popup-feedback">
            <div className="content text-center">
                <div >
                    <img className="ml-[60px]" src="/image/illustration/heart.svg"/>
                    <p className="text-l-med text-neutral-100 mt-[20px] mb-[12px]">
                        Thank you for your feedback!
                    </p>
                    <p className="text-m-reg text-neutral-80">
                        Your feedback will help us make <p>BigIdea better.</p>
                    </p>
                </div>
               
            </div>
        </div>
    )
} 