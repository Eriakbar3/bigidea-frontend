import React from "react";
import './style.css';

export default function ModalJoinTeam(props) {
   
    return (
        <div className="my-modal-join">
            <div className="content">
                <header className="flex justify-between">
                    <p className="text-l-med text-neutral-100">
                        Enter Team Code
                    </p>
                </header>
                <div className="mt-[16px]">
                    <p className="text-neutral-70 text-m-reg mt-[20px] mb-[4px]">
                        The team code is 6 characters long and only contains numbers.
                    </p>

                    <div className='flex pin-input'>
                        {
                            props.pinObj.map((obj, idx) => (
                                <>
                                    <input ref={(e) => props.inputRefs.current[idx] = e}
                                        type="number"
                                        name={obj.name}
                                        value={props.pin[obj.name]}
                                        maxLength={1}
                                        onFocus={(e) => e.target.select()}
                                        onKeyDown={(e) => props.handleKeyDown(e, idx)}
                                        onChange={(e) => props.handleChangePin(e, idx)} 
                                        />
                                    {
                                        idx === 2 &&
                                        <span className='text-m-reg text-neutral-70' style={{ lineHeight: "71px" }}>
                                            -
                                        </span>
                                    }
                                </>
                            ))
                        }
                    </div>
                </div>

                <footer>
                    <button
                        onClick={() => props.cancel()}
                        className=" bg-surface-primary cursor-pointer"
                        style={{
                            padding: "6px 16px 6px 16px",
                            boxShadow: '0px 1px 2px rgba(9, 45, 115, 0.12)',
                            borderRadius: '5px'
                        }}>
                        <p className="text-primary text-m-med">Cancel</p>
                    </button>
                    <button
                        onClick={() => props.action()}
                        className=" bg-primary cursor-pointer"
                        type='button'
                        style={{
                            padding: "6px 16px 6px 16px",
                            boxShadow: '0px 1px 2px rgba(9, 45, 115, 0.12)',
                            borderRadius: '5px'
                        }}>
                        <p className="text-neutral-10 text-m-med">
                            Join
                        </p>
                    </button>
                </footer>
            </div>
        </div>
    )
} 