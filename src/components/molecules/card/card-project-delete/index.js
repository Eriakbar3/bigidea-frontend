import React, { useState, useEffect } from 'react';
// import { useDispatch } from 'react-redux';
import MenuCardProject from '../../menu-card-project';
import './style.css';

export default function CardProjectDelete(props) {
	// const dispatch = useDispatch();
	const { project, detail_teams, isSelect, checked, idx, handleChange } = props
	const [name, setName] = useState(project.name);

	const createdAt = new Date(project.createdAt).toLocaleString('en-US', {
		year: 'numeric',
		month: '2-digit',
		day: '2-digit'
	});

	useEffect(() => {
		setName(project.name);
	}, []);

	return (
		<>
			<MenuCardProject>
				<div
					className='card-project'>
					{props.select ? (
						<div
							className="card"
							style={{
								backgroundImage: `url("/image/img-project/ss-board.png")`
							}}>
							<div
								className="overlay"
								style={{
									height: '191px',
									borderRadius: '10px',
									background: '#1E1E1E1A'
								}}></div>
						</div>
					) : (
						<div
							className="card"
							style={{
								backgroundImage: `url("/image/img-project/ss-board.png")`
							}}>
						</div>
					)}
					{props.select && props.isSelected && (
						<>
							<div
								className="absolute"
								style={{
									top: '12px',
									right: '13px',
									zIndex: '500'
								}}>
								<div className="xs-circle text-primary">
									<svg
										className="absolute abs-center"
										width="16"
										height="16"
										viewBox="0 0 16 16"
										fill="none"
										xmlns="http://www.w3.org/2000/svg">
										<path
											d="M13.8047 3.52851C14.0651 3.78886 14.0651 4.21097 13.8047 4.47132L6.4714 11.8047C6.21106 12.065 5.78894 12.065 5.5286 11.8047L2.19526 8.47132C1.93491 8.21097 1.93491 7.78886 2.19526 7.52851C2.45561 7.26816 2.87772 7.26816 3.13807 7.52851L6 10.3904L12.8619 3.52851C13.1223 3.26816 13.5444 3.26816 13.8047 3.52851Z"
											fill="white"
										/>
									</svg>
								</div>
							</div>
							<div
								className="absolute"
								style={{
									top: 0,
									right: 0,
									left: 0,
									bottom: 0,
									background: '#FFFFFF80',
									zIndex: '499'
								}}></div>
						</>
					)}
					<div
						style={{
							position: 'absolute',
							display: 'flex',
							bottom: '0',
							left: '0',
							right: '0',
							height: '58px',
							background: '#FFFFFF',
							borderRadius: '0 0 10px 10px',
							paddingRight: '15px',
							zIndex: 2
						}}>
						<img
							src="/image/icons/big-board-icon.svg"
							style={{ height: '24px', margin: 'auto 15px' }}
						/>

						<div className="my-auto">
							<input
								className="rename-input text-m-med text-neutral-100"
								type="text"
								value={name}
								disabled
								style={{ textOverflow: 'ellipsis' }}
							/>
							<div className="flex justify-start">
								<div className="flex">
									<p className="text-primary text-s-reg">
										{detail_teams['team']['name']}{' '}
									</p>
								</div>
								<div className="flex mt-[7px] mr-[5px] ml-[5px]">
									<svg
										width="3"
										height="3"
										viewBox="0 0 3 3"
										fill="none"
										xmlns="http://www.w3.org/2000/svg">
										<path
											d="M1.28955 0.349854C1.61816 0.349854 1.89131 0.458351 2.10901 0.675345C2.32671 0.887818 2.43555 1.16132 2.43555 1.49585C2.43555 1.83039 2.32671 2.10615 2.10901 2.32314C1.89131 2.53562 1.61816 2.64185 1.28955 2.64185C0.956845 2.64185 0.681641 2.53336 0.463942 2.31636C0.25035 2.09937 0.143555 1.82587 0.143555 1.49585C0.143555 1.16584 0.25035 0.892339 0.463942 0.675345C0.681641 0.458351 0.956845 0.349854 1.28955 0.349854Z"
											fill="#737373"
										/>
									</svg>{' '}
								</div>
								<p className="text-neutral-80 text-s-reg">
									{createdAt}
									{/* Last active {moment(props.data.createdAt).fromNow()} */}
								</p>
							</div>
						</div>
					</div>
					{/* <div className="overlay"> */}
					{isSelect === true && (
						<div
							className="absolute flex"
							style={{ right: '10px', top: '10px', gap: '14px' }}>
							<label className="relative h-[20px] my-auto">
								<input 
									type="checkbox"
									id={idx}
									checked={checked[idx]} 
									onChange={() => handleChange(idx)}
								/>
								<div className="absolute abs-center bi-ic-check text-neutral-10"></div>
							</label>
						</div>
					)}
					{/* </div> */}
				</div>
			</MenuCardProject>
		</>
	)
}
