import React from 'react';
import Card from '../../../atoms/card/Card';
import Avatar from '../../../atoms/avatar';
import { useSelector } from 'react-redux';

export default function CardProfileLeft() {
	const { userData } = useSelector((state) => state.auth);
	return (
		<Card>
			<div className="flex pt-[50px] ">
				<div className="relative mx-auto">
					<Avatar
						name={userData.name}
						width="160px"
						color={userData.image.color}
						image_url={
							userData.image.url
								? `https://idea.bigbox.co.id/minio/profile/user/${userData.image.url}`
								: null
						}
						fontStyle="head-xl-med"
					/>
				</div>
			</div>
			<div className="text-center pt-[24px] pb-[50px]">
				<p className="head-s-med text-neutral-100">{userData.name}</p>
				<p className="text-l-reg text-neutral-80 pt-[5px] ">
					{userData.email}
				</p>
			</div>
		</Card>
	);
}
