import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Card from '../../../atoms/card/Card';
import Avatar from '../../../atoms/avatar';
import InputPassword from '../../../atoms/input/InputPassword';
import BoxModal from '../../../molecules/modal/BoxModal';
import BoxModalDeleteAccount from '../../modal/BoxModalDeleteAccount';
import BoxModalValidChange from '../../modal/BoxModalValidChange';
import Button from '../../../elements/Button';
import InputSelect from '../../../atoms/input/InputSelect';

export default function CardProfileRight() {
	const { userData } = useSelector((state) => state.auth);
	const dispatch = useDispatch();
	const [modalDeleteAccount, setModalDeleteAccount] = useState(false);
	const [oldPassword, setOldPassword] = useState('');
	const [newPassword, setNewPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');
	const [modalConfirmChange, setModalConfirmChange] = useState(false);
	const [modalChangePassword, setModalChangePassword] = useState(false);
	const [modalConfirmDelChange, setModalConfirmDelChange] = useState(false);
	const [validasiPassword, setValidasiPassword] = useState('');

	const [upProfile, setUpProfile] = useState({
		name: userData.name,
		role: userData.role,
		email: userData.email
	});

	const roles = [
		'Project Manager',
		'Product Manager',
		'Engineering',
		'Scrum Master',
		'UX Reseacher',
		'UX Designer',
		'Marketing',
		'Freelancer',
		'Support',
		'Consultant / Professional Service',
		'IT Services',
		'Student',
		'Teacher',
		'Others'
	];

	const handleChangeProfile = (e) => {
		setUpProfile({
			...upProfile,
			[e.target.name]: e.target.value
		});
	};

	const handleUpdateUser = (e) => {
		e.preventDefault();
		setModalConfirmChange(false);
		if (!Object.values(upProfile).includes('')) {
			dispatch.auth.updateUserProfile(upProfile);
		} else {
			dispatch.auth.updateFail(
				'Changes failed to save, please try again'
			);
		}
	};

	const handleUpdatePassword = () => {
		if (oldPassword.length && newPassword.length) {
			if (newPassword === confirmPassword) {
				if (oldPassword !== newPassword) {
					dispatch.auth.updatePassword({
						password: newPassword,
						currentPassword: oldPassword
					});
					setModalChangePassword(false);
					setOldPassword('');
					setNewPassword('');
					setConfirmPassword('');
				} else {
					dispatch.auth.updateFail(
						'Changes failed to save, please try again'
					);
				}
			} else {
				dispatch.auth.updateFail(
					'Changes failed to save, please try again'
				);
			}
		} else {
			dispatch.auth.updateFail(
				'Changes failed to save, please try again'
			);
		}
	};

	const handleRemoveImage = async () => {
		if (userData.image.url !== null) {
			dispatch.auth.removeImageProfile();
		} else {
			dispatch.auth.updateFail(
				'Changes failed to save, please try again'
			);
		}
	};

	const handleChangeImage = async (file) => {
		const form_data = new FormData();
		form_data.append('image', file);
		console.log('form data', form_data);
		dispatch.auth.updateUserProfile(form_data);
	};

	const handleDeleteUser = () => {
		if (validasiPassword.length) {
			dispatch.auth.deleteUser({ password: validasiPassword });
		} else {
			dispatch.auth.updateFail(
				'Account failed to deleted, please try again'
			);
		}
	};

	return (
		<Card>
			<div className="p-[24px]">
				<div className="flex justify-between">
					<div className="flex">
						<div className="mx-auto">
							<Avatar
								name={userData.name}
								width="90px"
								color={userData.image.color}
								image_url={
									userData.image.url
										? `https://idea.bigbox.co.id/minio/profile/user/${userData.image.url}`
										: null
								}
								fontStyle="head-m-med"
							/>
						</div>
						<div className="mt-[28px] ml-[24px] mr-[22px]">
							<form encType="multipart/form-data" method="PUT">
								<input
									type="file"
									name="image"
									id="image"
									accept="image/x-png,image/gif,image/jpeg"
									className="w-[0.1px] h-[0.1px] opacity-0 overflow-hidden absolute -z-1"
									onChange={(e) =>
										handleChangeImage(e.target.files[0])
									}
								/>
								<div className="btn-change-img">
									<label
										htmlFor="image"
										className="text-m-med text-neutral-80 cursor-pointer">
										Change Picture
									</label>
								</div>
							</form>
						</div>
						<div className="mt-[35px] cursor-pointer">
							<div
								className="my-auto bi-ic-trash-2 text-neutral-60 w-[20px] h-[20px]"
								onClick={() => handleRemoveImage()}></div>
						</div>
					</div>
					<div className="flex mt-[65px] cursor-pointer">
						<p
							className="text-m-med text-primary"
							onClick={() => setModalChangePassword(true)}>
							Change Password
						</p>
					</div>
					{modalChangePassword && (
						<BoxModal
							title="Change Password"
							width="638px"
							widthButton="91px"
							heightButton="40px"
							footer={true}
							actionName="Change"
							actionState={oldPassword ? '' : 'disable'}
							textAlign="left"
							cancel={() => setModalChangePassword(false)}
							action={() => handleUpdatePassword()}
							handleCancelAction={true}
							headerClose={true}
							content={
								<div>
									<div className="mt-[16px]">
										<hr className="bg-neutral-10"></hr>
										<p className="text-neutral-100 text-m-reg mt-[20px] ml-[5px] mb-[4px]">
											Old Password
										</p>
										<InputPassword
											placeholder="Input old password"
											value={oldPassword}
											onChange={(e) => {
												setOldPassword(e);
											}}
										/>
										<p className="text-neutral-100 text-m-reg mt-[20px] ml-[5px] mb-[4px]">
											New Password
										</p>
										<InputPassword
											placeholder="Input new password"
											value={newPassword}
											onChange={(e) => setNewPassword(e)}
										/>
										<p className="text-neutral-100 text-m-reg mt-[20px] ml-[5px] mb-[4px]">
											Confirm New Password
										</p>
										<InputPassword
											placeholder="Input confirm new password"
											value={confirmPassword}
											onChange={(e) =>
												setConfirmPassword(e)
											}
										/>
									</div>
								</div>
							}
						/>
					)}
				</div>
				<form onSubmit={handleUpdateUser}>
					<p className="text-neutral-100 text-m-reg pt-[24px] mb-[4px]">
						Name
					</p>
					<input
						type="text"
						name="name"
						placeholder="Your Name"
						value={upProfile.name}
						onChange={(e) => handleChangeProfile(e)}
					/>
					<p className="text-neutral-100 text-m-reg mt-[20px] mb-[4px]">
						Role
					</p>
					<InputSelect
						value={upProfile.role}
						options={roles}
						name="role"
						placeholder="What is your role in the team?"
						onChange={(e) => {
							handleChangeProfile({
								target: {
									name: 'role',
									value: e
								}
							});
						}}
					/>
					<p className="text-neutral-100 text-m-reg mt-[20px] mb-[4px]">
						Email
					</p>
					<input
						type="email"
						name="email"
						placeholder="Your Email"
						value={upProfile.email}
						onChange={(e) => handleChangeProfile(e)}
					/>
					<div className="flex justify-between">
						<div
							className="flex mt-[50px]"
							onClick={() => setModalDeleteAccount(true)}>
							<Button
								classBackgroundColor={'bg-surface-primary'}
								width="148px"
								heigth="40px"
								borderRadius="5px"
								boxShadow="0px 1px 2px rgba(9, 45, 115, 0.12)"
								classTextColor="text-primary"
								classSizeText="text-l-med"
								content={<span>Delete Account</span>}
							/>
						</div>
						{modalDeleteAccount && (
							<BoxModalDeleteAccount
								title="Delete Account"
								width="424px"
								widthButton="75px"
								heightButton="32px"
								footer={true}
								actionName="Delete"
								action={() => handleDeleteUser()}
								textAlign="center"
								cancel={() => {
									setModalDeleteAccount(false);
									setValidasiPassword('');
								}}
								handleCancelAction={true}
								headerClose={true}
								content={
									<div className="mt-[16px]">
										<p className="text-m-med text-neutral-80 mb-[16px]">
											This will permanently delete the
											account. Deleting this account will
											delete all associated data, such as
											projects, files, for all users.
											<br />
											<br />
											You have{' '}
											<span className="text-neutral-100">
												{' '}
												28 days{' '}
											</span>{' '}
											to undo this operation by following
											instructions that will be sent to
											your email.
											<br />
											<br />
											To confirm, please enter the
											password.
										</p>
										<input
											type="password"
											placeholder="Password"
											value={validasiPassword}
											onChange={(e) =>
												setValidasiPassword(
													e.target.value
												)
											}
										/>
									</div>
								}
							/>
						)}
						{modalConfirmDelChange && (
							<BoxModalValidChange
								title="Save Changes"
								footer={true}
								action={() => handleDeleteUser()}
								discard={() => {
									setModalConfirmDelChange(false);
									setModalDeleteAccount(true);
								}}
								cancel={() => {
									setValidasiPassword('');
									setModalConfirmDelChange(false);
								}}
								handleCancelAction={true}
								headerClose={true}
								content={
									<div className="mt-[16px]">
										<p className="text-m-med text-neutral-80 mb-[16px]">
											Do you want to save changes before
											leaving this
											<p>page?</p>
										</p>
									</div>
								}
							/>
						)}
						<div className="flex mt-[50px]">
							<div
								className="btn btn-primary bg-primary text-neutral-10 block w-full"
								onClick={() => setModalConfirmChange(true)}>
								Save Changes
							</div>
							{modalConfirmChange && (
								<BoxModalValidChange
									title="Save Changes"
									footer={true}
									// action={() =>
									//     setModalValidasiChange(false)
									// }
									discard={() => setModalConfirmChange(false)}
									cancel={() => setModalConfirmChange(false)}
									handleCancelAction={true}
									headerClose={true}
									content={
										<div className="mt-[16px]">
											<p className="text-m-med text-neutral-80 mb-[16px]">
												Do you want to save changes
												before leaving this
												<p>page?</p>
											</p>
										</div>
									}
								/>
							)}
						</div>
					</div>
				</form>
			</div>
		</Card>
	);
}
