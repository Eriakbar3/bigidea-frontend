import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import MenuCardProject from '../../menu-card-project';
import ModalDelete from '../../modal/modal-delete';
import './style.css';

export default function CardProjectAll(props) {
    const dispatch = useDispatch();
    const { project, detail_teams } = props
    const { userData } = useSelector((state) => state.auth);
    const { isUpdateSuccess } = useSelector((state) => state.project);
    const { dataBoardMany } = useSelector((state) => state.canvas);
    const [name, setName] = useState(project.name);
    const [onRename, setOnRename] = useState(false);
    const [modalDelete, setModalDelete] = useState(false);
    const [pin, setPin] = useState(false);
    const [detailTeam, setDetailTeam] = useState(null)
    const getLink = '/whiteboard/project/' + project._id;
    const [updateProject, setUpdateProject] = useState(new Date(project.updatedAt).toLocaleString('en-US', {
		year: 'numeric',
		month: '2-digit',
		day: '2-digit'
	}));

    useEffect(() => {
        setName(project.name);
        if (project.pinned_by.length > 0) {
            for (let i = 0; i < project.pinned_by.length; i++) {
                if (project.pinned_by[i] === userData._id) {
                    setPin(true);
                    break;
                }
                else {
                    setPin(false)
                }
            }
        } else {
            setPin(false);
        }
    }, [isUpdateSuccess, project]);

    useEffect(() => {
		if (dataBoardMany.length > 0){
			for (let i = 0; i < dataBoardMany.length; i++) {
				if (project._id === dataBoardMany[i]['project']) {
					setUpdateProject(new Date(dataBoardMany[i]['updatedAt']).toLocaleString('en-US', {
						year: 'numeric',
						month: '2-digit',
						day: '2-digit'
					}))
				}
			}
		}
	},[isUpdateSuccess, dataBoardMany])

    useEffect(() => {
        const array = []
        for (let i = 0; i < detail_teams.length; i++) {
            if (project.team === detail_teams[i]['_id']) {
                array.push(detail_teams[i])
            }
        }
        if (array.length > 0){
            setDetailTeam(array)
        }
    }, [project, detail_teams, userData])

    const handleRename = () => {
        setOnRename(true);
    };
    const handleChange = (val) => {
        if (onRename) setName(val);
    };

    const handleDeleteProject = () => {
        dispatch.project.deleteProjectTeam(project._id);
        setModalDelete(false)
    }

    const handleEnter = async (e) => {
	    if (e.key === "Enter") {
	        setOnRename(false)
			dispatch.project.updateProject({ id:project._id, project_name: name });
	    }
	}

    const handleDuplicateProject = () => {
		const dataDuplicate = {
			id:project._id,
			name:project.name,
			project_desc:project.project_desc,
			team:project.team,
			type:project.type,
			created_by:project.created_by,
			createdAt:project.createdAt,
			updatedAt:project.updatedAt
		}
		dispatch.project.duplicateProject(dataDuplicate)
	}

    const handlePin = async () => {
        dispatch.project.pinProjectTeam(project._id)
        setPin(true)
    }

    const handleUnpin = async () => {
        dispatch.project.unpinProjectTeam(project._id)
        setPin(false)
    }
    return (
        <>
            <MenuCardProject
                handleDeleteProject={() => setModalDelete(true)}
                handleDuplicateProject={() => handleDuplicateProject()}
                handleRenameProject={() => handleRename()}>
                <div
                    className={`${props.select ? 'no-hover-card-project' : 'card-project'
                        }`}>
                    {props.select ? (
                        <div to={getLink}>
                            <div
                                className="card"
                                style={{
                                    backgroundImage: `url("/image/img-project/ss-board.png")`
                                }}>
                                <div
                                    className="overlay"
                                    style={{
                                        height: '191px',
                                        borderRadius: '10px',
                                        background: '#1E1E1E1A'
                                    }}></div>
                            </div>
                        </div>
                    ) : (
                        <Link to={getLink}>
                            <div
                                className="card"
                                style={{
                                    backgroundImage: `url("/image/img-project/ss-board.png")`
                                }}>
                                <div
                                    className="overlay"
                                    style={{
                                        height: '191px',
                                        borderRadius: '10px',
                                        background: '#1E1E1E1A'
                                    }}></div>
                            </div>
                        </Link>
                    )}
                    {props.select && props.isSelected && (
                        <>
                            <div
                                className="absolute"
                                style={{
                                    top: '12px',
                                    right: '13px',
                                    zIndex: '500'
                                }}>
                                <div className="xs-circle text-primary">
                                    <svg
                                        className="absolute abs-center"
                                        width="16"
                                        height="16"
                                        viewBox="0 0 16 16"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M13.8047 3.52851C14.0651 3.78886 14.0651 4.21097 13.8047 4.47132L6.4714 11.8047C6.21106 12.065 5.78894 12.065 5.5286 11.8047L2.19526 8.47132C1.93491 8.21097 1.93491 7.78886 2.19526 7.52851C2.45561 7.26816 2.87772 7.26816 3.13807 7.52851L6 10.3904L12.8619 3.52851C13.1223 3.26816 13.5444 3.26816 13.8047 3.52851Z"
                                            fill="white"
                                        />
                                    </svg>
                                </div>
                            </div>
                            <div
                                className="absolute"
                                style={{
                                    top: 0,
                                    right: 0,
                                    left: 0,
                                    bottom: 0,
                                    background: '#FFFFFF80',
                                    zIndex: '499'
                                }}></div>
                        </>
                    )}
                    <div
                        style={{
                            position: 'absolute',
                            display: 'flex',
                            bottom: '0',
                            left: '0',
                            right: '0',
                            height: '58px',
                            background: '#FFFFFF',
                            borderRadius: '0 0 10px 10px',
                            paddingRight: '15px',
                            zIndex: 2
                        }}>
                        <img
                            src="/image/icons/big-board-icon.svg"
                            style={{ height: '24px', margin: 'auto 15px' }}
                        />

                        <div className="my-auto">
                            <input
                                className="rename-input text-m-med text-neutral-100"
                                type="text"
                                value={name}
                                disabled={!onRename}
                                style={{ textOverflow: 'ellipsis' }}
                                onChange={(e) => handleChange(e.target.value)}
                                onKeyDown={e => handleEnter(e)}
                            />
                            <div className="flex justify-start">
                                <div className="flex">
                                    <p className="text-primary text-s-reg">
                                        {detailTeam !== null ? detailTeam[0]['name'] : ' '}{' '}
                                    </p>
                                </div>
                                <div className="flex mt-[7px] mr-[5px] ml-[5px]">
                                    <svg
                                        width="3"
                                        height="3"
                                        viewBox="0 0 3 3"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M1.28955 0.349854C1.61816 0.349854 1.89131 0.458351 2.10901 0.675345C2.32671 0.887818 2.43555 1.16132 2.43555 1.49585C2.43555 1.83039 2.32671 2.10615 2.10901 2.32314C1.89131 2.53562 1.61816 2.64185 1.28955 2.64185C0.956845 2.64185 0.681641 2.53336 0.463942 2.31636C0.25035 2.09937 0.143555 1.82587 0.143555 1.49585C0.143555 1.16584 0.25035 0.892339 0.463942 0.675345C0.681641 0.458351 0.956845 0.349854 1.28955 0.349854Z"
                                            fill="#737373"
                                        />
                                    </svg>{' '}
                                </div>
                                <p className="text-neutral-80 text-s-reg">
                                    {updateProject}
                                    {/* Last active {moment(props.data.createdAt).fromNow()} */}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="overlay">
                        <div
                            className="absolute flex"
                            style={{ right: '10px', top: '10px', gap: '14px' }}>
                            <img
                                src="/image/icons/rename-button.svg"
                                style={{ zIndex: 200, cursor: "pointer" }}
                                onClick={() => handleRename()}
                            />
                            {pin ? (
                                <img
                                    src="/image/icons/unpin-button.svg"
                                    style={{ zIndex: 200, cursor: "pointer" }}
                                    onClick={() => handleUnpin()}
                                />
                            ) : (
                                <img
                                    src="/image/icons/pin-button.svg"
                                    style={{ zIndex: 200, cursor: "pointer" }}
                                    onClick={() => handlePin()}
                                />
                            )}
                            <img
                                src="/image/icons/delete-button.svg"
                                style={{ zIndex: 200, cursor: "pointer" }}
                                onClick={() => setModalDelete(true)}
                            />
                        </div>
                    </div>
                </div>
            </MenuCardProject>
            {modalDelete && (
                <ModalDelete
                    title="Delete Project"
                    footer={true}
                    action={() => handleDeleteProject()}
                    cancel={() => setModalDelete(false)}
                    handleCancelAction={true}
                    headerClose={true}
                    content={
                        <div className="mt-[16px]">
                            <p className="text-m-med text-neutral-100 mb-[16px]">
                                Are you sure you want to delete?
                            </p>
                            <p className="text-m-med text-neutral-80 mb-[16px]">
                                This will be in the recently deleted in 28 days.
                                After 28 days will be permanently deleted.
                            </p>
                        </div>
                    }
                />
            )}
        </>
    )
}
