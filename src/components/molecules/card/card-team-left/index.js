import React from 'react';
import Card from '../../../atoms/card/Card';
import Avatar from '../../../atoms/avatar';

export default function CardTeamLeft(props) {
	const { data } = props;
	return (
		<Card>
			<div className="flex pt-[50px] ">
				<div className="relative mx-auto">
					<Avatar
						name={data.team.name}
						width="160px"
						color={data.team.image.color}
						image_url={
							data.team.image.url
								? `https://idea.bigbox.co.id/minio/profile/team/${data.team.image.url}`
								: null
						}
						fontStyle="head-xl-med"
					/>
				</div>
			</div>
			<div className="text-center pt-[24px] pb-[50px]">
				<p className="head-s-med text-neutral-100">{data.team.name}</p>
			</div>
		</Card>
	);
}
