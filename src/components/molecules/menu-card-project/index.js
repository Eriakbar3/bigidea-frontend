import React, { useEffect, useRef } from "react";
import './style.css'

export default function MenuCardProject(props) {
    const ref = useRef()
    const menuRef = useRef()

    useEffect(() => {
        if (ref.current) {
            ref.current.addEventListener('contextmenu', function (e) {
                e.preventDefault()
                if (menuRef.current) {
                    menuRef.current.style.position = 'fixed'
                    menuRef.current.style.zIndex = 1000
                    menuRef.current.style.top = e.clientY + "px"
                    menuRef.current.style.left = e.clientX + "px"
                    menuRef.current.style.display = 'block'
                }
            })
        }
        document.addEventListener('click', function (e) {
            if (menuRef.current) {
                if (e.target !== menuRef.current)
                    menuRef.current.style.display = 'none'
            }
        })
    }, [])

    return (
        <>
            <div 
                className="dropdown"
                ref={ref} >
                {props.children}
                <div 
                    className="dropdown-content text-neutral-70 text-m-reg"
                    style={{ display: 'none' }}
                    ref={menuRef}>
                        <ul className="list-none">
                            <li onClick={() => props.handleShareProject()}>
                                Share
                            </li>
                            <li onClick={() => props.handleRenameProject()}>
                                Rename
                            </li>
                            <li style={{borderTop: "1px solid #E4E4E4", borderBottom:"1px solid #E4E4E4"}}>
                                Change cover image
                            </li>
                            <li onClick={() => props.handleDuplicateProject()}>
                                Duplicate
                            </li>
                            <li onClick={() => props.handleDeleteProject()}> 
                                Delete
                            </li>
                        </ul>
                </div>
            </div>
        </>
    );
}