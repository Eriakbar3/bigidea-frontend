import React from 'react';
import PhoneCall from '../../../assets/image/icon/phone-call-2 2.svg';
import Comment from '../../../assets/image/icon/ic-comment.svg';
import ThumbsUp from '../../../assets/image/icon/ic-thumbs-up.svg';
import Timer from '../../../assets/image/icon/ic-timer.svg';
import ChatGPT from '../../../assets/image/icon/ic-chatgpt.svg';

export default function InteractiveToolbar() {
	return (
		<div className="tools-custom">
			<div className="tools-icon">
				<img src={PhoneCall} alt="" />
			</div>
			<div className="tools-icon">
				<img src={Comment} alt="" />
			</div>
			<div className="tools-icon">
				<img src={ThumbsUp} alt="" />
			</div>
			<div className="tools-icon">
				<img src={Timer} alt="" />
			</div>
			<div className="tools-icon">
				<img src={ChatGPT} alt="" />
			</div>
		</div>
	);
}
