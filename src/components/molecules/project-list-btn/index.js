import React from "react"
import './style.css'

export default function ProjectListButton(props) {
    return (
        <div className="flex justify-between project-list-button">
            <img src={props.icon} className="icon"/>
            <div>
                <p className="text-m-med text-primary mb-[5px]" >
                    {props.title}
                </p>
                <p className="text-s-reg text-neutral-60">
                    {props.subtitle}
                </p>
            </div>
            <svg 
                style={{margin:"auto 0 auto auto"}} 
                width="14" 
                height="20" 
                viewBox="0 0 14 20" 
                fill="none" 
                xmlns="http://www.w3.org/2000/svg">
                <path 
                    d="M6.91107 4.41076C7.23651 4.08533 7.76414 4.08533 8.08958 4.41076L13.0896 9.41077C13.415 9.7362 13.415 10.2638 13.0896 10.5893L8.08958 15.5893C7.76414 15.9147 7.23651 15.9147 6.91107 15.5893C6.58563 15.2638 6.58563 14.7362 6.91107 14.4108L11.3218 10L6.91107 5.58928C6.58563 5.26384 6.58563 4.7362 6.91107 4.41076Z" 
                    fill="#C9C9C9" />
            </svg>
        </div>
    )
}