import React from 'react';
import './style.css';
import DropdownV2 from '../../atoms/dropdown/DropdownV2';
import ColorPicker from '../../atoms/color-picker';
import ListDropdownV2 from '../../atoms/dropdown/ListDropdownV2';

export default function LineToolbox({ x, y, transform, style, onChange }) {
	const colorList = [
		'#F2AFA6',
		'#FC9A53',
		'#F3E274',
		'#FFE199',
		'#FFF5D1',
		'#DDCCBB',
		'#AAAAAA',
		'#4BC0C0',
		'#BFF2D6',
		'#8CD0F1',
		'#C2DCF8',
		'#C2B8F5',
		'#C3B4CF',
		'#1E1E1E'
	];

	const lineWidthList = [
		{
			name: <hr style={{ borderTop: '1px solid #737373' }} />,
			value: 1
		},
		{
			name: <hr style={{ borderTop: '2px solid #737373' }} />,
			value: 2
		},
		{
			name: <hr style={{ borderTop: '3px solid #737373' }} />,
			value: 3
		},
		{
			name: <hr style={{ borderTop: '5px solid #737373' }} />,
			value: 5
		},
		{
			name: <hr style={{ borderTop: '8px solid #737373' }} />,
			value: 8
		}
	];

	return (
		<div
			className="toolbox line-toolbox"
			style={{
				top: y - 100 + 'px',
				left: x + 'px',
				transform
			}}>
			<DropdownV2
				content={
					<ColorPicker
						colors={colorList}
						value={style.stroke}
						onChange={(e) =>
							onChange({
								...style,
								stroke: e
							})
						}
					/>
				}>
				<div className="toolbox-item">
					<svg
						width="24"
						height="24"
						viewBox="0 0 24 24"
						fill="none"
						xmlns="http://www.w3.org/2000/svg">
						<circle
							cx="12"
							cy="12"
							r="11"
							stroke={style.stroke}
							strokeWidth="2"
						/>
					</svg>
					<div className="bi-ic-chevron-down py-[5px] px-[3px]"></div>
				</div>
			</DropdownV2>
			<ListDropdownV2
				items={lineWidthList}
				value={style.lineWidth}
				onChange={(e) =>
					onChange({
						...style,
						lineWidth: e
					})
				}>
				<div className="toolbox-item">
					<hr
						style={{
							borderTop: `${style.lineWidth}px solid #737373`,
							width: '24px'
						}}
					/>
					<div className="bi-ic-chevron-down"></div>
				</div>
			</ListDropdownV2>
		</div>
	);
}
