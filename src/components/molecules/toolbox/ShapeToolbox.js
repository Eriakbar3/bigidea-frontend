import React from 'react';
import './style.css';
import DropdownV2 from '../../atoms/dropdown/DropdownV2';
import ColorPicker from '../../atoms/color-picker';

export default function ShapeToolbox({ x, y, transform, style, onChange }) {
	const colorList = [
		'#F2AFA6',
		'#FC9A53',
		'#F3E274',
		'#FFE199',
		'#FFF5D1',
		'#DDCCBB',
		'#AAAAAA',
		'#4BC0C0',
		'#BFF2D6',
		'#8CD0F1',
		'#C2DCF8',
		'#C2B8F5',
		'#C3B4CF',
		'#1E1E1E'
	];

	return (
		<div
			className="toolbox shape-toolbox"
			style={{
				top: y - 100 + 'px',
				left: x + 'px',
				transform
			}}>
			<DropdownV2
				content={
					<ColorPicker
						colors={colorList}
						value={style?.fill}
						onChange={(e) =>
							onChange({
								...style,
								fill: e
							})
						}
					/>
				}>
				<div className="toolbox-item">
					<svg
						width="24"
						height="24"
						viewBox="0 0 24 24"
						fill="none"
						xmlns="http://www.w3.org/2000/svg">
						<circle cx="12" cy="12" r="12" fill={style?.fill} />
					</svg>
					<div className="bi-ic-chevron-down py-[5px] px-[3px]"></div>
				</div>
			</DropdownV2>
			<DropdownV2
				content={
					<ColorPicker
						colors={colorList}
						value={style?.stroke}
						onChange={(e) =>
							onChange({
								...style,
								stroke: e
							})
						}
					/>
				}
				placement="bottom-right">
				<div className="toolbox-item">
					<svg
						width="24"
						height="24"
						viewBox="0 0 24 24"
						fill="none"
						xmlns="http://www.w3.org/2000/svg">
						<circle
							cx="12"
							cy="12"
							r="11"
							stroke={style?.stroke}
							strokeWidth="2"
						/>
					</svg>
					<div className="bi-ic-chevron-down py-[5px] px-[3px]"></div>
				</div>
			</DropdownV2>
		</div>
	);
}
