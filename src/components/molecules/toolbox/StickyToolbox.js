import React from 'react';
import './style.css';
import ColorPicker from '../../atoms/color-picker';
import DropdownV2 from '../../atoms/dropdown/DropdownV2';
import ListDropdownV2 from '../../atoms/dropdown/ListDropdownV2';
import AlignPicker from '../../atoms/align-picker';
import { alignIcons } from '../../atoms/icon';

export default function StickyToolbox({ x, y, transform, style, onChange }) {
	const colorList = [
		'#F2AFA6',
		'#FC9A53',
		'#F3E274',
		'#FFE199',
		'#FFF5D1',
		'#DDCCBB',
		'#AAAAAA',
		'#4BC0C0',
		'#BFF2D6',
		'#8CD0F1',
		'#C2DCF8',
		'#C2B8F5',
		'#C3B4CF',
		'#1E1E1E'
	];

	const fontList = [
		{
			name: 'Arial',
			value: 'Arial'
		},
		{
			name: 'Helvetica',
			value: 'Helvetica'
		},
		{
			name: 'SF Compact Display',
			value: 'SF Compact Display'
		},
		{
			name: 'SF Pro Display',
			value: 'SF Pro Display'
		},
		{
			name: 'SF Pro Rounded',
			value: 'SF Pro Rounded'
		}
	];

	const fontSizeList = [
		{
			name: '10',
			value: 10
		},
		{
			name: '11',
			value: 11
		},
		{
			name: '12',
			value: 12
		},
		{
			name: '14',
			value: 14
		},
		{
			name: '16',
			value: 16
		},
		{
			name: '18',
			value: 18
		},
		{
			name: '20',
			value: 20
		},
		{
			name: '20',
			value: 20
		},
		{
			name: '22',
			value: 22
		},
		{
			name: '26',
			value: 26
		},
		{
			name: '28',
			value: 28
		},
		{
			name: '36',
			value: 36
		},
		{
			name: '48',
			value: 48
		}
	];

	return (
		<div
			className="toolbox text-toolbox"
			style={{
				top: y - 100 + 'px',
				left: x + 'px',
				transform
			}}>
			<DropdownV2
				content={
					<ColorPicker
						colors={colorList}
						value={style.fill}
						onChange={(e) =>
							onChange({
								...style,
								fill: e
							})
						}
					/>
				}>
				<div className="toolbox-item px-[9px]">
					<svg
						width="24"
						height="24"
						viewBox="0 0 24 24"
						fill="none"
						xmlns="http://www.w3.org/2000/svg">
						<circle cx="12" cy="12" r="12" fill={style.fill} />
					</svg>
					<div className="bi-ic-chevron-down"></div>
				</div>
			</DropdownV2>
			<div
				className={`toolbox-item ${
					style.fontWeight === 500 ? 'bg-neutral-40' : ''
				}`}
				onClick={() => {
					console.log('fontWeight', style.fontWeight);
					onChange({
						...style,
						fontWeight: style.fontWeight === 400 ? 500 : 400
					});
				}}>
				<div className="bi-ic-bold text-neutral-80 px-[17px] text-[20px]"></div>
			</div>
			<ListDropdownV2
				items={fontList}
				value={style.font}
				onChange={(e) =>
					onChange({
						...style,
						font: e
					})
				}
				align="left">
				<div className="toolbox-item font">
					<p>{style.font}</p>
					<div className="bi-ic-chevron-down"></div>
				</div>
			</ListDropdownV2>
			<ListDropdownV2
				items={fontSizeList}
				value={style.fontSize}
				onChange={(e) =>
					onChange({
						...style,
						fontSize: e
					})
				}
				align="left">
				<div className="toolbox-item px-[10px]">
					<p>{style.fontSize}</p>
					<div className="bi-ic-chevron-down"></div>
				</div>
			</ListDropdownV2>
			<DropdownV2
				content={
					<AlignPicker
						value={style.textAlign}
						onChange={(e) =>
							onChange({
								...style,
								textAlign: e
							})
						}
					/>
				}>
				<div className="toolbox-item px-[9px]">
					{alignIcons[style?.textAlign ? style.textAlign : 'left']}
					<div className="bi-ic-chevron-down"></div>
				</div>
			</DropdownV2>
		</div>
	);
}
