import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';
import Avatar from '../../../atoms/avatar';
import ModalTeam from '../../modal/modal-team';
import ModalCreateTeam from '../../modal/create-team';
import ModalJoinTeam from '../../modal/join-team';
import Process from '../../../atoms/process';
import Alert from '../../../atoms/alert';
import './style.css';

export default function SideNav() {
	const { pathname } = useLocation();
	const dispatch = useDispatch();
	const { 
		list_teams, 
		isUpdateSuccess, 
		errorMessage, 
		successMessage,
		isDeleteTeam 
	} = useSelector((state) => state.team);
	const isLoading = useSelector((state) => state.loading.global);
	const [modalTeam, setModalTeam] = useState(false)
	const [modalCreateTeam, setModalCreateTeam] = useState(false)
	const [modalJoinTeam, setModalJoinTeam] = useState(false)
	const [teamName, setTeamName] = useState('')
    const [teamDesc, setTeamDesc] = useState('')
	const inputRefs = useRef(new Array(6).fill(null))
	const [pin, setPin] = useState({ num1: '', num2: '', num3: '', num4: '', num5: '', num6: '' })
	const pinObj = [{ name: 'num1' }, { name: 'num2' }, { name: 'num3' }, { name: 'num4' }, { name: 'num5' }, { name: 'num6' }]
	const finalPin = pin.num1 + pin.num2 + pin.num3 + pin.num4 + pin.num5 + pin.num6
	
    const handleKeyDown = (e, idx) => {
		if(e.key === "Backspace" && idx>0 && pin[e.target.name].length===0){
			inputRefs.current[idx-1].focus()
		}
	}
	const handleChangePin = (e, idx) => {
		let val = e.target.value
		if (val.length===1) {
			setPin({ ...pin, [e.target.name]: val })
			if(idx !== 5)inputRefs.current[idx + 1].focus()
		}else if(val.length===0){
			setPin({ ...pin, [e.target.name]: val })
		}
	}

	useEffect(() => {
		dispatch.team.listTeams();
		dispatch.team.resetState();
	}, [isUpdateSuccess, isDeleteTeam]);

    const handleSubmit = (e) => {
		e.preventDefault();
		if (teamName.length) {
			dispatch.team.addTeam({ name: teamName, team_desc:teamDesc });
			setModalCreateTeam(false)
		} else {
			dispatch.team.updateFail('Your team failed to create, please try again');
		}
	};

	const handleJoinTeam = () => {
		dispatch.team.joinTeam({invite_token:finalPin})
		setModalJoinTeam(false)
	}

	useEffect(() => {
		if (errorMessage?.length) {
			const alert = new Alert();
			alert.error(errorMessage);
		}
		if (successMessage?.length) {
			const alert = new Alert();
			alert.success(successMessage);
		}
	}, [errorMessage, successMessage]);
	return (
		<>
			{isLoading ? <Process /> : <></>}
			<nav className="dashboard-sidenav overflow-auto">
				<Link
					to="/dashboard/pinned"
					className={`${pathname === '/dashboard/pinned' ? 'active' : ''
						}`}>
					<svg
						width="24"
						height="24"
						viewBox="0 0 24 24"
						fill="none"
						xmlns="http://www.w3.org/2000/svg">
						<path
							className="orange"
							d="M2.89939 19.5429L7.65607 14.8386L9.24163 16.4067L4.48495 21.111C4.48495 21.111 2.58067 22.3637 2.10628 21.8951C1.63188 21.4264 2.89939 19.5429 2.89939 19.5429Z"
							fill="#AAAAAA"
						/>
						<path
							className="primary"
							d="M2.96954 10.3006C2.67914 10.5878 2.53838 10.9119 2.54726 11.2727C2.55613 11.6335 2.71628 11.9679 3.02772 12.2759L6.7487 15.9559C6.84161 16.0478 6.78293 15.9898 6.93102 16.1362C7.02762 16.2318 6.91046 16.1159 6.99157 16.1961C7.11849 16.3216 6.93102 16.1362 7.05959 16.2634C7.12827 16.3313 7.26961 16.4711 7.31752 16.5185C7.36064 16.5611 7.52275 16.7214 7.5616 16.7599C7.71999 16.9165 7.52274 16.7214 7.7136 16.9102C7.91722 17.1116 7.68301 16.8799 7.81391 17.0094C7.91722 17.1116 7.71998 16.9165 7.96514 17.159L11.6233 20.7768C11.9347 21.0848 12.2729 21.2432 12.6377 21.252C13.0026 21.2607 13.3302 21.1215 13.6206 20.8343C14.0936 20.3665 14.4375 19.7847 14.6522 19.0891C14.8715 18.3889 14.9409 17.6237 14.8603 16.7936C14.7798 15.9635 14.5379 15.1175 14.1349 14.2557L16.9823 10.9989C17.757 11.2184 18.4812 11.3741 19.1549 11.466C19.8286 11.5579 20.3641 11.5779 20.7614 11.5262C21.1683 11.4839 21.4841 11.3516 21.709 11.1292C21.915 10.9254 22.0117 10.6782 21.9989 10.3876C21.9909 10.1017 21.8623 9.8356 21.6131 9.5892L14.3328 2.38905C14.0836 2.14264 13.8121 2.01311 13.5183 2.00045C13.2292 1.99252 12.9817 2.09046 12.7756 2.29428C12.5508 2.51663 12.417 2.82901 12.3742 3.23142C12.3267 3.62909 12.347 4.15869 12.4351 4.82023C12.528 5.48651 12.6902 6.20744 12.9217 6.98303L9.62868 9.79911C8.75246 9.39576 7.89467 9.15425 7.05531 9.07456C6.21596 8.99487 5.44225 9.0635 4.7342 9.28042C4.03083 9.49272 3.44261 9.83279 2.96954 10.3006Z"
							fill="#AAAAAA"
						/>
					</svg>
					<p>Pinned</p>
				</Link>
				<Link
					to="/dashboard/my-projects"
					className={`${pathname === '/dashboard/my-projects' ? 'active' : ''
						}`}>
					<svg
						width="24"
						height="24"
						viewBox="0 0 24 24"
						fill="none"
						xmlns="http://www.w3.org/2000/svg">
						<path
							className="orange"
							d="M6.28669 8.91594C6.36246 8.62128 6.62811 8.4153 6.93235 8.4153H22.1162C22.5591 8.4153 22.8789 8.83924 22.7572 9.26512L19.7536 19.7779C19.6718 20.0641 19.4102 20.2615 19.1126 20.2615H4.22906C3.79349 20.2615 3.47492 19.8506 3.58339 19.4288L6.28669 8.91594Z"
							fill="#AAAAAA"
						/>
						<path
							className="primary"
							d="M1.66667 3C1.29848 3 1 3.29848 1 3.66667V18.418C1 18.6206 1.09206 18.8121 1.2502 18.9386L1.42969 19.0822C1.67317 19.277 2.01914 19.277 2.26262 19.0822L2.51255 18.8823C2.62902 18.7891 2.71087 18.6596 2.74503 18.5144L4.04615 12.9846L5.27376 8.07421C5.34795 7.77743 5.61461 7.56923 5.92052 7.56923H18.7795C19.1477 7.56923 19.4462 7.27075 19.4462 6.90256V5.69744C19.4462 5.32925 19.1477 5.03077 18.7795 5.03077H11.1358C10.9333 5.03077 10.7417 4.93871 10.6152 4.78057L10.1385 4.18462L9.32263 3.23281C9.19597 3.08504 9.01107 3 8.81645 3H1.66667Z"
							fill="#AAAAAA"
						/>
					</svg>
					<p>My Projects</p>
				</Link>
				<div className="flex gap-[24px] text-primary mb-[40px] cursor-pointer"
					onClick={() => setModalTeam(true)}>
					<svg
						width="24"
						height="24"
						viewBox="0 0 24 24"
						fill="none"
						xmlns="http://www.w3.org/2000/svg">
						<path
							d="M12 3C7.02944 3 3 7.02944 3 12C3 16.9706 7.02944 21 12 21C16.9706 21 21 16.9706 21 12C21 7.02944 16.9706 3 12 3ZM1 12C1 5.92487 5.92487 1 12 1C18.0751 1 23 5.92487 23 12C23 18.0751 18.0751 23 12 23C5.92487 23 1 18.0751 1 12Z"
							fill="#0549CF"
						/>
						<path
							d="M12 7C12.5523 7 13 7.44772 13 8V16C13 16.5523 12.5523 17 12 17C11.4477 17 11 16.5523 11 16V8C11 7.44772 11.4477 7 12 7Z"
							fill="#0549CF"
						/>
						<path
							d="M7 12C7 11.4477 7.44772 11 8 11H16C16.5523 11 17 11.4477 17 12C17 12.5523 16.5523 13 16 13H8C7.44772 13 7 12.5523 7 12Z"
							fill="#0549CF"
						/>
					</svg>
					<p>Create / Join Team</p>
				</div>
				{modalTeam && (
					<ModalTeam
						cancel={() => setModalTeam(false)}
						setCreateModal={() => {
							setModalTeam(false)
							setModalCreateTeam(true)
						}}
						setJoinModal={() => {
							setModalTeam(false)
							setModalJoinTeam(true)
						}}
					/>
				)}
				{modalCreateTeam && (
					<ModalCreateTeam
						cancel={() => {
							setModalTeam(true)
							setModalCreateTeam(false)
						}}
						teamName={teamName}
						teamDesc={teamDesc}
						setTeamName={(e) => setTeamName(e)}
						setTeamDesc={(e) => setTeamDesc(e)}
						handleSubmit={handleSubmit}
					/>
				)}
				{modalJoinTeam && (
					<ModalJoinTeam
						cancel={() => {
							setModalTeam(true)
							setModalJoinTeam(false)
						}}
						pin={pin}
						pinObj={pinObj}
						inputRefs={inputRefs}
						handleKeyDown={(e,idx)=>handleKeyDown(e, idx)}
						handleChangePin={(e,idx)=>handleChangePin(e,idx)}
						action={()=>handleJoinTeam()}
					/>
				)}
				{list_teams.length ? (
					list_teams.map((i, idx) => (
						<Link
							to={`/dashboard/team/${i._id}`}
							className={`${pathname === `/dashboard/team/${i._id}`
								? 'active'
								: ''
								} truncate`}
							key={idx}>
							<Avatar
								name={i.name}
								width="24px"
								color={i.image.color}
								image_url={
									i.image.url
										? `https://idea.bigbox.co.id/minio/profile/team/${i.image.url}`
										: null
								}
								fontStyle="text-s-med"
							/>
							<p>{i.name}</p>
						</Link>
					))
				) : (
					<></>
				)}
			</nav>
		</>
	);
}
