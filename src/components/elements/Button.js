import React from 'react';

export default function Button(props) {
	const {
		classBackgroundColor,
		width,
		height,
		borderRadius,
		boxShadow,
		classTextColor,
		classSizeText,
		type,
		content
	} = props;
	return (
		<>
			<div
				className={`cursor-pointer btn ${classBackgroundColor}`}
				type={type}
				style={{
					width: width,
					height: height,
					borderRadius: borderRadius,
					boxShadow: boxShadow
				}}>
				<p className={`${classSizeText} ${classTextColor}`}>
					{content}
				</p>
			</div>
		</>
	);
}
