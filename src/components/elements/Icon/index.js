import React from 'react'

import { ReactComponent as IconNavigation } from '../../../assets/image/icon/ic-navigation.svg';
import { ReactComponent as IconText } from '../../../assets/image/icon/ic-text.svg';
import { ReactComponent as IconStickyNote } from '../../../assets/image/icon/ic-stickynote.svg';
import { ReactComponent as IconSquere } from '../../../assets/image/icon/ic-square.svg';
import { ReactComponent as IconTemplate } from '../../../assets/image/icon/ic-template.svg';
import { ReactComponent as IconPencil } from '../../../assets/image/icon/ic-pencil.svg';
import { ReactComponent as IconFlowchart } from '../../../assets/image/icon/ic-flowchart.svg';
import { ReactComponent as IconSticker } from '../../../assets/image/icon/ic-sticker.svg';
import { ReactComponent as IconFrame } from '../../../assets/image/icon/ic-frame-whiteboard.svg';
import { ReactComponent as IconShare } from '../../../assets/image/icon/ic-share.svg';