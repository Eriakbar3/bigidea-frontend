import './index.css';

export default class Alert {
	constructor() {
		this.body = document.getElementById('root');
		this.alertEl = document.createElement('div');
		this.alertEl.id = 'alert';
		this.text = document.createElement('p');
		this.errorIcon = document.createElement('span');
		this.errorIcon.className = 'bi-ic-x';
		this.successIcon = document.createElement('span');
		this.successIcon.className = 'bi-ic-check';
	}
	success(message) {
		this.alertEl.append(this.successIcon);
		this.text.innerText = message;
		this.alertEl.append(this.text);
		this.body.append(this.alertEl);
		setTimeout(() => {
			this.alertEl.remove();
		}, 4000);
	}
	error(message) {
		this.alertEl.append(this.errorIcon);
		this.text.innerText = message;
		this.alertEl.append(this.text);
		this.body.append(this.alertEl);
		setTimeout(() => {
			this.alertEl.remove();
		}, 4000);
	}
}

// const alert = new Alert()

// export default alert
