import React from 'react';
import './style.css';

export default function ColorPicker({ colors, value, onChange }) {
	return (
		<div className="color-picker">
			{colors.map((i) => (
				<div
					key={i}
					className={`color ${i === value ? 'active' : ''}`}
					style={{ backgroundColor: i }}
					onClick={() =>
						typeof onChange === 'function'
							? onChange(i)
							: void undefined
					}></div>
			))}
		</div>
	);
}
