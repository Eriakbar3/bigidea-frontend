import React, {useState} from 'react';
import './style.css'

export default function InputInvitation(props) {
    const { value, onChangeEmail, onChangeRole, options } = props;
    const [hideOptions, setHideOptions] = useState(true);
    return (
        <div className="relative">
            <input
                type="email"
                placeholder="Email"
                onChange={(e) => onChangeEmail(e.target.value)}
            />
            <div 
                className='absolute cursor-pointer text-neutral-80 top-1/2 right-[12px] -translate-y-2/4'
                onClick={() => setHideOptions(false)}
                onBlur={() => {
                    setTimeout(function () {
                        setHideOptions(false);
                    }, 500);
                }}>
                <div className='flex' >
                    <p className='text-m-reg text-neutral-80 mr-[12px]'>
                        {value}
                    </p>
                    <div className='bi-ic-chevron-down mt-[2px]'></div>
                </div>
            </div>
            <div 
                className="input-select-options-invite" 
                hidden={hideOptions}
                onClick={() => setHideOptions(true)}>
				{options.map((i) => (
					<p
						key={i}
						className="item text-m-reg text-neutral-100"
						onClick={() => onChangeRole(i)}
                        >
						{i}
					</p>
				))}
			</div>
        </div>
    );
}
