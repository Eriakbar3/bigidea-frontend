import React from 'react';

export default function InputCheckbox() {
	return (
		<label className="relative h-[20px] my-auto">
			<input type="checkbox" />
			<div className="absolute abs-center bi-ic-check text-neutral-10"></div>
		</label>
	);
}
