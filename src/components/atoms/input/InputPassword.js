import React from 'react';
import { useState } from 'react';

export default function InputPassword(props) {
	const { value, placeholder, onChange } = props;

	const [showPassword, setShowPassword] = useState(false);

	return (
		<div className="relative">
			<input
				type={showPassword ? 'text' : 'password'}
				placeholder={placeholder}
				value={value}
				onChange={(e) => onChange(e.target.value)}
			/>
			<div
				className={`absolute text-neutral-80 top-1/2 right-[12px] -translate-y-2/4 ${
					showPassword ? 'bi-ic-eye' : 'bi-ic-eye-off'
				}`}
				onClick={() => setShowPassword((e) => !e)}></div>
		</div>
	);
}
