import React from 'react';
import { useState } from 'react';

export default function InputSelect(props) {
	const { options, value, placeholder, onChange } = props;

	const [hideOptions, setHideOptions] = useState(true);

	return (
		<div className="relative">
			<div className="relative">
				<input
					className="cursor-default caret-transparent"
					type="text"
					placeholder={placeholder}
					onFocus={() => setHideOptions(false)}
					onBlur={() => {
						setTimeout(function () {
							setHideOptions(true);
						}, 500);
					}}
					value={value}
				/>
				<div className="bi-ic-chevron-down absolute top-1/2 right-[12px] -translate-y-2/4 text-neutral-80 "></div>
			</div>
			<div className="input-select-options" hidden={hideOptions}>
				{options.map((i) => (
					<p
						key={i}
						className="item text-m-reg text-neutral-100"
						onClick={() => onChange(i)}>
						{i}
					</p>
				))}
			</div>
		</div>
	);
}
