import React from 'react';

export default function InputSearch(props) {
	const { value, placeholder, onChange } = props;

	return (
		<div className="relative my-auto">
			<input
				type="text"
				placeholder={placeholder}
				value={value}
				onChange={(e) => onChange(e.target.value)}
				style={{ marginBottom: 0, paddingLeft: '36px' }}
			/>
			<div className="absolute top-1/2 left-[12px] -translate-y-2/4 bi-ic-search text-neutral-80"></div>
		</div>
	);
}
