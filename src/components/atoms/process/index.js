import React from 'react';
import Spinner from '@atlaskit/spinner';
import './index.css';

export default function Process() {
	return (
		<div className="process">
			<Spinner size="small" appearance="invert" />
			<p>Please wait...</p>
		</div>
	);
}
