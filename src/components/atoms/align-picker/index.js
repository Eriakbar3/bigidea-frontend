import React from 'react';
import './style.css';

export default function AlignPicker({ value, onChange }) {
	const alignList = [
		{
			icon: (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					fill="none"
					xmlns="http://www.w3.org/2000/svg">
					<path
						fillRule="evenodd"
						clipRule="evenodd"
						d="M2 10C2 9.44772 2.44772 9 3 9H17C17.5523 9 18 9.44772 18 10C18 10.5523 17.5523 11 17 11H3C2.44772 11 2 10.5523 2 10Z"
						fill="#737373"
					/>
					<path
						fillRule="evenodd"
						clipRule="evenodd"
						d="M2 6C2 5.44772 2.44772 5 3 5H21C21.5523 5 22 5.44772 22 6C22 6.55228 21.5523 7 21 7H3C2.44772 7 2 6.55228 2 6Z"
						fill="#737373"
					/>
					<path
						fillRule="evenodd"
						clipRule="evenodd"
						d="M2 14C2 13.4477 2.44772 13 3 13H21C21.5523 13 22 13.4477 22 14C22 14.5523 21.5523 15 21 15H3C2.44772 15 2 14.5523 2 14Z"
						fill="#737373"
					/>
					<path
						fillRule="evenodd"
						clipRule="evenodd"
						d="M2 18C2 17.4477 2.44772 17 3 17H17C17.5523 17 18 17.4477 18 18C18 18.5523 17.5523 19 17 19H3C2.44772 19 2 18.5523 2 18Z"
						fill="#737373"
					/>
				</svg>
			),
			value: 'left'
		},
		{
			icon: (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					fill="none"
					xmlns="http://www.w3.org/2000/svg">
					<path
						fillRule="evenodd"
						clipRule="evenodd"
						d="M5 10C5 9.44772 5.44772 9 6 9H18C18.5523 9 19 9.44772 19 10C19 10.5523 18.5523 11 18 11H6C5.44772 11 5 10.5523 5 10Z"
						fill="#737373"
					/>
					<path
						fillRule="evenodd"
						clipRule="evenodd"
						d="M2 6C2 5.44772 2.44772 5 3 5H21C21.5523 5 22 5.44772 22 6C22 6.55228 21.5523 7 21 7H3C2.44772 7 2 6.55228 2 6Z"
						fill="#737373"
					/>
					<path
						fillRule="evenodd"
						clipRule="evenodd"
						d="M2 14C2 13.4477 2.44772 13 3 13H21C21.5523 13 22 13.4477 22 14C22 14.5523 21.5523 15 21 15H3C2.44772 15 2 14.5523 2 14Z"
						fill="#737373"
					/>
					<path
						fillRule="evenodd"
						clipRule="evenodd"
						d="M5 18C5 17.4477 5.44772 17 6 17H18C18.5523 17 19 17.4477 19 18C19 18.5523 18.5523 19 18 19H6C5.44772 19 5 18.5523 5 18Z"
						fill="#737373"
					/>
				</svg>
			),
			value: 'center'
		},
		{
			icon: (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					fill="none"
					xmlns="http://www.w3.org/2000/svg">
					<path
						fillRule="evenodd"
						clipRule="evenodd"
						d="M6 10C6 9.44772 6.44772 9 7 9H21C21.5523 9 22 9.44772 22 10C22 10.5523 21.5523 11 21 11H7C6.44772 11 6 10.5523 6 10Z"
						fill="#737373"
					/>
					<path
						fillRule="evenodd"
						clipRule="evenodd"
						d="M2 6C2 5.44772 2.44772 5 3 5H21C21.5523 5 22 5.44772 22 6C22 6.55228 21.5523 7 21 7H3C2.44772 7 2 6.55228 2 6Z"
						fill="#737373"
					/>
					<path
						fillRule="evenodd"
						clipRule="evenodd"
						d="M2 14C2 13.4477 2.44772 13 3 13H21C21.5523 13 22 13.4477 22 14C22 14.5523 21.5523 15 21 15H3C2.44772 15 2 14.5523 2 14Z"
						fill="#737373"
					/>
					<path
						fillRule="evenodd"
						clipRule="evenodd"
						d="M6 18C6 17.4477 6.44772 17 7 17H21C21.5523 17 22 17.4477 22 18C22 18.5523 21.5523 19 21 19H7C6.44772 19 6 18.5523 6 18Z"
						fill="#737373"
					/>
				</svg>
			),
			value: 'right'
		},
		{
			icon: (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					fill="none"
					xmlns="http://www.w3.org/2000/svg">
					<path
						fillRule="evenodd"
						clipRule="evenodd"
						d="M2 10C2 9.44772 2.44772 9 3 9H21C21.5523 9 22 9.44772 22 10C22 10.5523 21.5523 11 21 11H3C2.44772 11 2 10.5523 2 10Z"
						fill="#737373"
					/>
					<path
						fillRule="evenodd"
						clipRule="evenodd"
						d="M2 6C2 5.44772 2.44772 5 3 5H21C21.5523 5 22 5.44772 22 6C22 6.55228 21.5523 7 21 7H3C2.44772 7 2 6.55228 2 6Z"
						fill="#737373"
					/>
					<path
						fillRule="evenodd"
						clipRule="evenodd"
						d="M2 14C2 13.4477 2.44772 13 3 13H21C21.5523 13 22 13.4477 22 14C22 14.5523 21.5523 15 21 15H3C2.44772 15 2 14.5523 2 14Z"
						fill="#737373"
					/>
					<path
						fillRule="evenodd"
						clipRule="evenodd"
						d="M2 18C2 17.4477 2.44772 17 3 17H21C21.5523 17 22 17.4477 22 18C22 18.5523 21.5523 19 21 19H3C2.44772 19 2 18.5523 2 18Z"
						fill="#737373"
					/>
				</svg>
			),
			value: 'justify'
		}
	];

	return (
		<div className="align-picker">
			{alignList.map((i, idx) => (
				<div
					key={idx}
					className={`item ${i.value === value ? 'active' : ''}`}
					onClick={() =>
						typeof onChange === 'function'
							? onChange(i.value)
							: void undefined
					}>
					{i.icon}
				</div>
			))}
		</div>
	);
}
