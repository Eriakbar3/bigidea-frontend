import React from 'react';
import './style.css';

export default function Tooltip({ children, placement = 'bottom', text = '' }) {
	return (
		<div className="tooltip">
			{children}
			<p className={`tooltip-text tooltip-${placement}`}>{text}</p>
		</div>
	);
}
