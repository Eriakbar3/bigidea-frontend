import React from "react";
import './style.css';

export default function DeleteRestore(props) {
    return (
        <>
            <div className={props.select !== 0 ? "btn-recently bottom-[100px]" : "btn-recently bottom-[70px]" }>
                <button
                    onClick={() => props.delete()}
                    className="btn-recently-delete bg-surface-primary text-primary">
                    Delete
                </button>
                <button
                    onClick={() => props.restore()}
                    className="btn-recently-restore bg-primary">
                    <p className="text-neutral-10">
                        Restore
                    </p>
                </button>
            </div>

        </>
    )
}