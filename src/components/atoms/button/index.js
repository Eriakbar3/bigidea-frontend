import React from 'react';
import Spinner from '@atlaskit/spinner';
import './style.css';

export default function Button({
	type,
	fluid,
	isLoading,
	spinner = 'invert',
	children,
	htmlType = 'button',
	onClick
}) {
	return (
		<button
			type={htmlType}
			className={`btn ${type}`}
			style={{
				width: fluid ? '100%' : 'unset'
			}}
			onClick={() => {
				typeof onClick === 'function' ? onClick() : void undefined;
			}}>
			{isLoading && (
				<span className="spinner">
					<Spinner size="small" appearance={spinner} />
				</span>
			)}
			{children}
		</button>
	);
}
