import React from 'react';
import { getInitialName } from '../../../utils/index.js';
import './style.css';

export default function Avatar(props) {
	const { name, width, color, image_url, fontStyle } = props;

	const colorComb = [
		{ color: '#0549CF', bg: '#F0F5FF' },
		{ color: '#FF931E', bg: '#FFF8F0' },
		{ color: '#5F32BC', bg: '#EFEAF9' },
		{ color: '#FFD445', bg: '#FFFBEC' },
		{ color: '#03A08B', bg: '#E6F6F4' },
		{ color: '#FF483D', bg: '#FFECEB' }
	];

	return (
		<>
			{!image_url ? (
				<div
					className="rounded-full relative avatar"
					style={{
						width,
						height: width,
						backgroundColor: colorComb[color].bg
					}}>
					<p
						className={`font-medium absolute abs-center ${fontStyle}`}
						style={{
							color: colorComb[color].color
						}}>
						{getInitialName(name)}
					</p>
				</div>
			) : (
				<div
					className={`rounded-full min-w-${width}`}
					style={{
						width,
						height: width
					}}>
					<img
						src={image_url ? image_url : 'image/icons/Avatar.svg'}
						onError={(e) =>
							(e.target.src = 'image/icons/Avatar.svg')
						}
						className={`object-cover h-full w-full rounded-full`}
					/>
				</div>
			)}
		</>
	);
}
