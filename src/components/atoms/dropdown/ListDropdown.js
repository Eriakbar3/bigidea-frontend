import React from 'react';
import Dropdown from './Dropdown';

export default function ListDropdown({ menu, children }) {
	const content = (
		<ul className="text-m-reg">
			{menu.map((i, idx) => (
				<li key={idx}>{i}</li>
			))}
		</ul>
	);
	return <Dropdown content={content}>{children}</Dropdown>;
}
