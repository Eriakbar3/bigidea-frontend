import React, { useEffect, useRef, useState } from 'react';
import './style.css';

export default function Dropdown(props) {
	const { children, content } = props;
	const [hide, setHide] = useState(true);

	const ref = useRef(null);

	useEffect(() => {
		function handleClickOutside(event) {
			if (ref.current && !ref.current.contains(event.target)) {
				setHide(true);
			}
		}
		document.addEventListener('mousedown', handleClickOutside);
		return () => {
			document.removeEventListener('mousedown', handleClickOutside);
		};
	}, [ref]);

	return (
		<div ref={ref} className="h-full">
			<div
				className="relative dropdown cursor-default"
				onClick={() => setHide((e) => !e)}>
				{children}
				<div
					className={`absolute content right-0`}
					style={{
						top: 'calc(100% + 10px)',
						zIndex: 1000
					}}
					hidden={hide}>
					{content}
				</div>
			</div>
		</div>
	);
}
