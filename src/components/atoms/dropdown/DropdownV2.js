import React, { useEffect, useRef, useState } from 'react';
import './style.css';

export default function DropdownV2({
	children,
	placement = 'bottom-left',
	content
}) {
	const [open, setOpen] = useState(false);

	const ref = useRef(null);

	useEffect(() => {
		function handleClickOutside(event) {
			if (ref.current && !ref.current.contains(event.target)) {
				setOpen(false);
			}
		}
		document.addEventListener('mousedown', handleClickOutside);
		return () => {
			document.removeEventListener('mousedown', handleClickOutside);
		};
	}, [ref]);

	return (
		<div className="dropdown-v2" ref={ref}>
			<div onClick={() => setOpen((prev) => !prev)}>{children}</div>
			{open ? (
				<div
					className={`item ${placement}`}
					onClick={() => setOpen(false)}>
					{content}
				</div>
			) : (
				<></>
			)}
		</div>
	);
}
