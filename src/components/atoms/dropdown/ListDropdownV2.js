import React, { useEffect, useRef, useState } from 'react';
import './style.css';

export default function ListDropdownV2({
	children,
	items,
	value,
	onChange,
	align = 'center'
}) {
	const [open, setOpen] = useState(false);

	const ref = useRef(null);

	useEffect(() => {
		function handleClickOutside(event) {
			if (ref.current && !ref.current.contains(event.target)) {
				setOpen(false);
			}
		}
		document.addEventListener('mousedown', handleClickOutside);
		return () => {
			document.removeEventListener('mousedown', handleClickOutside);
		};
	}, [ref]);

	return (
		<div
			className="list-dropdown-v2"
			onClick={() => setOpen((prev) => !prev)}
			ref={ref}>
			{children}
			{open ? (
				<div className="content">
					<ul style={{ textAlign: align }}>
						{items.map((i, idx) => (
							<li
								className={i.value === value ? 'selected' : ''}
								key={idx}
								onClick={() => onChange(i.value)}>
								<div>{i.name}</div>
							</li>
						))}
					</ul>
				</div>
			) : (
				<></>
			)}
		</div>
	);
}
