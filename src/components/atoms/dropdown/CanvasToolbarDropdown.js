import React, { useEffect, useRef, useState } from 'react';
import './style.css';

export default function CanvasToolbarDropdown({ children, content }) {
	const [hide, setHide] = useState(true);

	const ref = useRef(null);

	useEffect(() => {
		function handleClickOutside(event) {
			if (ref.current && !ref.current.contains(event.target)) {
				setHide(true);
			}
		}
		document.addEventListener('mousedown', handleClickOutside);
		return () => {
			document.removeEventListener('mousedown', handleClickOutside);
		};
	}, [ref]);

	return (
		<div ref={ref} className="h-full">
			<div className="relative" onClick={() => setHide((e) => !e)}>
				{children}
				<div className="canvas-toolbar-dropdown" hidden={hide}>
					{content}
				</div>
			</div>
		</div>
	);
}
