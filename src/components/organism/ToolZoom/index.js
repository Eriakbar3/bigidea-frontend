import React from 'react';
import IconPlus from '../../../assets/image/icon/ic-plus.svg';
import IconMinus from '../../../assets/image/icon/ic-minus.svg';
import IconGridLayout from '../../../assets/image/icon/ic-grid-layout.svg';
export default function ToolZoom() {
	return (
		<div className="tools-canvas">
			<div className="tools-bg" id="tl">
				<div className="tools-icon">
					<img src={IconGridLayout} alt="" />
				</div>
				<div className="bi-ic-chevron-down chevron-down text-neutral-80 "></div>
			</div>
			<div className="tools-zoom" id="tr">
				<div className="tools-icon">
					<img src={IconMinus} alt="" />
				</div>
				<div className="tools-icon">
					<img src={IconPlus} alt="" />
				</div>
				100%
			</div>
		</div>
	);
}
