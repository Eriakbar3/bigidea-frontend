import React from 'react';
import DashboardHeader from '../../molecules/header/dashboard-header/DashboardHeader';
import SideNav from '../../molecules/sidenav/dashboard-sidenav/SideNav';
import './style.css';

export default function DashboardLayout(props) {
	const { children } = props;

	return (
		<div className="dashboard-layout">
			<DashboardHeader />
			<div className="main">
				<SideNav />
				<div className="page">{children}</div>
			</div>
		</div>
	);
}
