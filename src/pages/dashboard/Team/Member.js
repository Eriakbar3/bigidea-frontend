import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Avatar from '../../../components/atoms/avatar/index';
import EditRole from '../../../components/molecules/dropdown/edit-role';
import ModalDelete from '../../../components/molecules/modal/modal-delete';
import moment from 'moment';
import voca from 'voca';

export default function Member(props) {
	const dispatch = useDispatch();
	const { id } = useParams();
	const { team_members, detail_teams, isUpdateSuccess } = useSelector(
		(state) => state.team
	);
	const [editRoleDropdown, setEditRoleDropdown] = useState(false);
	const [modalRemove, setModalRemove] = useState(false);
	const [idMemberModal, setIdMemberModal] = useState('');

	useEffect(() => {
		dispatch.team.getMembers(id);
	}, [id, detail_teams, isUpdateSuccess]);

	const handleRemoveMember = (x) => {
		dispatch.team.removeMember({ id: x });
		setModalRemove(false);
	};

	return team_members.length ? (
		<>
			<div className="flex justify-between">
				<div className="flex">
					<p className="text-neutral-100 head-s-med mt-[26px] mb-[26px]">
						Members
					</p>
				</div>
				<div className="flex items-center">
					<p className="text-neutral-80 text-m-reg mt-[26px] mb-[26px]">
						<span>Team Code : </span>
					</p>
					<p className="text-neutral-100 head-s-med mt-[26px] mb-[26px] ml-[10px]">
						<span>{detail_teams.team.invite_token}</span>
					</p>
				</div>
			</div>
			<div className="flex-wrap gap-[40px]">
				<div className="flex gap-[40px]">
					{team_members.map((member) => (
						<div className="member" key={member._id}>
							<div className="mt-[7px]">
								<Avatar
									name={member.user.name}
									width="48px"
									color={member.user.image.color}
									image_url={
										member.user.image.url
											? `https://idea.bigbox.co.id/minio/profile/user/${member.user.image.url}`
											: null
									}
								/>
							</div>
							<div className="ml-[16px]">
								<div className="flex justify-between">
									<p className="text-s-reg text-neutral-80">
										{voca.capitalize(member.role)}
									</p>
									{props.role === 'owner' &&
									member.role !== 'owner' ? (
										<EditRole
											editRoleDropdown={editRoleDropdown}
											role={
												member.role === 'member'
													? 'Admin'
													: 'Member'
											}
											role_user={props.role}
											id={member._id}
											setModalRemove={() => {
												setModalRemove(true);
												setIdMemberModal(member._id);
											}}
											onClick={() =>
												setEditRoleDropdown(
													editRoleDropdown === false
														? true
														: false
												)
											}
										/>
									) : props.role === 'admin' &&
									  member.role !== 'owner' ? (
										<EditRole
											editRoleDropdown={editRoleDropdown}
											role={
												member.role === 'member'
													? 'Admin'
													: 'Member'
											}
											role_user={props.role}
											id={member._id}
											setModalRemove={() => {
												setModalRemove(true);
												setIdMemberModal(member._id);
											}}
											onClick={() =>
												setEditRoleDropdown(
													editRoleDropdown === false
														? true
														: false
												)
											}
										/>
									) : null}
								</div>
								<p className="text-l-reg text-neutral-100 mb-[2px]">
									{member.user.name}
								</p>
								<p className="text-s-reg text-neutral-80">
									Last active{' '}
									{moment(member.user.updatedAt).fromNow()}
								</p>
							</div>
							{modalRemove && (
								<ModalDelete
									title="Remove from Team"
									footer={true}
									action={() =>
										handleRemoveMember(idMemberModal)
									}
									cancel={() => setModalRemove(false)}
									handleCancelAction={true}
									headerClose={true}
									content={
										<div className="mt-[16px]">
											<p className="text-m-med text-neutral-80 mb-[16px]">
												Are you sure you want to remove
												<span className="text-neutral-100">
													{' '}
													{member.user.name}
												</span>{' '}
												from{' '}
												<p className="text-neutral-100">
													{
														detail_teams['team'][
															'name'
														]
													}
													<span className="text-neutral-80">
														{' '}
														?
													</span>
												</p>
											</p>
										</div>
									}
								/>
							)}
						</div>
					))}
				</div>
			</div>
		</>
	) : (
		<></>
	);
}
