import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import CardProjectDelete from '../../../components/molecules/card/card-project-delete';
import DeleteRestore from '../../../components/atoms/delete-restore';

export default function DeletedRecently(props) {
	const { isSelect } = props;
	const { id } = useParams();
	const dispatch = useDispatch();
	const { project_delete, isRestoreProject, isDeleteProject } = useSelector((state) => state.project);
	const { detail_teams } = useSelector((state) => state.team);
	const [totalSelect, setTotalSelect] = useState(0)
	const [checkedState, setCheckedState] = useState(
		''
	);

	useEffect(() => {
		setCheckedState(new Array(project_delete.length).fill(false))
	}, [isRestoreProject, isDeleteProject, project_delete])

	const handleChangeCheck = (position) => {
		let sum = 0
		const updatedCheckedState = checkedState.map((item, index) =>
			index === position ? !item : item
		);
		setCheckedState(updatedCheckedState);
		for (let i = 0; i < updatedCheckedState.length; i++) {
			if (updatedCheckedState[i] == true) {
				sum = sum + 1
			}
		}
		setTotalSelect(sum)
	}

	const handleSelectAll = () => {
		const array = []
		for (let i = 0; i < checkedState.length; i++) {
			if (checkedState[i] === false) {
				array.push(true)
			} else {
				array.push(checkedState[i])
			}
		}
		setCheckedState(array);
	}

	const handleDeletedProject = (checkedState) => {
		let count = 0;
		let arrayPayload = []
		let lengthStateFirst = checkedState.length
		for (let i = 0; i < checkedState.length; i++) {
			if (checkedState[i] === true) {
				arrayPayload.push(project_delete[i]['_id'])
			} else {
				count = count + 1
			}
		}
		if (arrayPayload.length > 0) {
			dispatch.project.deletedRecentlyProject({ "id": arrayPayload })
		}
		if (count === lengthStateFirst) {
			dispatch.project.updateFail("Please select your project")
		} else {
			for (let i = 0; i < checkedState.length; i++) {
				if (checkedState[i] === true) {
					checkedState.splice(i, 1)
					setCheckedState(checkedState)
					i = i - 1
				}
			}
		}
	}

	const handleRestoredProject = (checkedState) => {
		let count = 0;
		let arrayPayload = []
		let lengthStateFirst = checkedState.length
		for (let i = 0; i < checkedState.length; i++) {
			if (checkedState[i] === true) {
				arrayPayload.push(project_delete[i]['_id'])
			} else {
				count = count + 1
			}
		}
		if (arrayPayload.length > 0) {
			dispatch.project.restoreProjectTeam({ "id": arrayPayload })
		}
		if (count === lengthStateFirst) {
			dispatch.project.updateFail("Please select your project")
		} else {
			for (let i = 0; i < checkedState.length; i++) {
				if (checkedState[i] === true) {
					checkedState.splice(i, 1)
					setCheckedState(checkedState)
					i = i - 1
				}
			}
		}
	}
	return detail_teams !== '' ? (
		<>
			{project_delete.length > 0 ? (
				<div>
					<div className="flex flex-wrap gap-[26px] mt-[26px] mb-[26px]">
						{project_delete.map((project, index) => (
							<CardProjectDelete
								id={id}
								isSelect={isSelect}
								project={project}
								key={project._id}
								checked={checkedState}
								idx={index}
								detail_teams={detail_teams}
								handleChange={(e) => handleChangeCheck(e)}
							/>
						))}
					</div>
					{isSelect && (
						<>
							{totalSelect != 0 && (
								<div className='total-select'>
									<p className='text-s-med text-neutral-100'>
										{totalSelect} project selected
									</p>
								</div>
							)}
							<DeleteRestore
								delete={() => handleDeletedProject(checkedState)}
								restore={() => handleRestoredProject(checkedState)} 
								select={totalSelect}/>
								
							{totalSelect != 0 && (
								<div className='select-all' onClick={() => handleSelectAll()}>
									<p className='text-m-med text-primary'>
										Select All
									</p>
								</div>
							)}
						</>
					)}
					<div className='announcement-deleted'>
						<p className='text-s-reg text-neutral-70'>
							Projects can be recover in 28 days. After that time,
							projects will be permanently deleted
						</p>
					</div>
				</div>
			) : (
				<div className="grid grid-cols-11 pt-[150px] text-center">
					<div className="justify-self-center col-start-5 col-span-3 ">
						<div className="h-[238px]">image</div>
						<p className="text-l-reg text-neutral-70">
							You don't have any board yet
						</p>
					</div>
				</div>
			)}
		</>
	) : (
		<div className="h-screen relative">
			<img
				src={`${window.location.origin}/image/illustration/Loader.gif`}
				className="h-[100px] absolute abs-center"
			/>
		</div>
	);
}
