import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import TeamProject from './TeamProject';
import Member from './Member';
import Setting from './Setting';
import DeletedRecently from './DeletedRecently';
import ModalInvitation from '../../../components/molecules/modal/modal-invite';
import Alert from '../../../components/atoms/alert';
import Process from '../../../components/atoms/process';

export default function Team() {
	const { id } = useParams();
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const { errorMessage, successMessage, detail_teams } = useSelector(
		(state) => state.team
	);
	const {
		project_delete,
		errorMessageProject,
		successMessageProject,
		isDeleteProject,
		isRestoreProject,
		project_create
	} = useSelector((state) => state.project);
	const isLoading = useSelector((state) => state.loading.global);
	const [IsVisible, setIsVisible] = useState('team');
	const [settingVisible, setSettingVisible] = useState('');
	const [modalInvite, setModalInvite] = useState(false);
	const [select, setSelect] = useState(false);
	const [role, setRole] = useState('Member');
	const [email, setEmail] = useState('');
	const [copyTokenMessage, setCopyTokenMessage] = useState('');
	const roles = ['Admin', 'Member'];

	const subPage = {
		team: <TeamProject />,
		members: <Member role={settingVisible} />,
		settings: <Setting role={settingVisible} />,
		delete: <DeletedRecently isSelect={select} />
	};

	const handleCreateProject = () => {
		dispatch.project.addProject({ id: id });
	};

	useEffect(() => {
		if (project_create.length > 0) {
			navigate('/whiteboard/project/' + project_create[0]['_id']);
			dispatch.project.updateProjectCreate([]);
		}
	}, [project_create]);

	useEffect(() => {
		if (detail_teams.length != 0) {
			setSettingVisible(detail_teams['your_role']);
		}
	}, [id, detail_teams]);

	useEffect(() => {
		if (project_delete.length === 0) {
			setIsVisible('team');
		}
	}, [project_delete]);

	useEffect(() => {
		dispatch.project.getProjectDeleteByTeam(id);
		dispatch.team.getDetailTeams(id);
	}, [isDeleteProject, id, isRestoreProject]);

	useEffect(() => {
		if (errorMessage?.length) {
			const alert = new Alert();
			alert.error(errorMessage);
		}
		if (successMessage?.length) {
			const alert = new Alert();
			alert.success(successMessage);
		}
		if (copyTokenMessage?.length) {
			const alert = new Alert();
			alert.copyToken(copyTokenMessage);
		}
		dispatch.project.resetState();
		setCopyTokenMessage('');
	}, [errorMessage, successMessage, copyTokenMessage]);

	useEffect(() => {
		if (errorMessageProject?.length) {
			const alert = new Alert();
			alert.error(errorMessageProject);
		}
		if (successMessageProject?.length) {
			const alert = new Alert();
			alert.success(successMessageProject);
		}
		dispatch.project.resetState();
	}, [errorMessageProject, successMessageProject]);

	const handleInviteMember = (email, id, role) => {
		const errors = {};
		if (email === '') {
			errors.email = 'Required';
			dispatch.team.updateFail('Email Required');
		} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
			errors.email = 'Invalid email address';
			dispatch.team.updateFail('Invalid email address');
		} else {
			dispatch.team.inviteMember({
				email: email,
				team_id: id,
				role: role.toLowerCase()
			});
			setModalInvite(false);
			setEmail('');
			setRole('Member');
		}
	};

	const handleCopyToken = () => {
		navigator.clipboard.writeText(detail_teams.team.invite_token);
		setCopyTokenMessage('Team Code Copied');
	};
	return (
		<>
			{isLoading ? <Process /> : <></>}
			<div
				className="flex justify-between"
				style={{
					borderBottom: '1px solid #E4E4E4'
				}}>
				{
					<div
						className="flex"
						style={{ gap: '28px', padding: '20px 32px 0 32px' }}>
						<p
							className={` text-l-reg  ${
								IsVisible === 'team'
									? 'text-neutral-80'
									: 'text-neutral-60'
							}`}
							style={{
								padding: '12px 0',
								cursor: 'pointer',
								borderBottom: `${
									IsVisible === 'team'
										? '2px solid #0549CF'
										: 'none'
								}`
							}}
							onClick={() => setIsVisible('team')}>
							Team Name
						</p>
						<p
							className={`text-l-reg ${
								IsVisible === 'members'
									? 'text-neutral-80'
									: 'text-neutral-60'
							}`}
							style={{
								padding: '12px 0',
								cursor: 'pointer',
								borderBottom: `${
									IsVisible === 'members'
										? '2px solid #0549CF'
										: 'none'
								}`
							}}
							onClick={() => setIsVisible('members')}>
							Members
						</p>
						{(settingVisible === 'owner' ||
							settingVisible === 'admin') && (
							<p
								className={`text-center text-l-reg ${
									IsVisible === 'settings'
										? 'text-neutral-80'
										: 'text-neutral-60'
								}`}
								style={{
									padding: '12px 0',
									cursor: 'pointer',
									borderBottom: `${
										IsVisible === 'settings'
											? '2px solid #0549CF'
											: 'none'
									}`
								}}
								onClick={() => setIsVisible('settings')}>
								Settings
							</p>
						)}
						{(settingVisible === 'owner' ||
							settingVisible === 'admin') &&
							project_delete.length > 0 && (
								<p
									className={`text-l-reg  ${
										IsVisible === 'delete'
											? 'text-neutral-80'
											: 'text-neutral-60'
									}`}
									style={{
										padding: '12px 0',
										cursor: 'pointer',
										borderBottom: `${
											IsVisible === 'delete'
												? '2px solid #0549CF'
												: 'none'
										}`
									}}
									onClick={() => setIsVisible('delete')}>
									Deleted (
									{project_delete.length
										? project_delete.length
										: 0}
									)
								</p>
							)}
					</div>
				}
				{IsVisible === 'team' && (
					<div
						className="flex cursor-pointer pt-[25px] mr-[32px]"
						onClick={() => handleCreateProject()}>
						<svg
							style={{ margin: '3px 8px -2px 8px' }}
							width="16"
							height="16"
							viewBox="0 0 16 16"
							fill="none"
							xmlns="http://www.w3.org/2000/svg">
							<path
								d="M7.99967 1.66675C8.36786 1.66675 8.66634 1.96522 8.66634 2.33341V13.6667C8.66634 14.0349 8.36786 14.3334 7.99967 14.3334C7.63148 14.3334 7.33301 14.0349 7.33301 13.6667V2.33341C7.33301 1.96522 7.63148 1.66675 7.99967 1.66675Z"
								fill="#0549CF"
							/>
							<path
								d="M1.66699 7.99992C1.66699 7.63173 1.96547 7.33325 2.33366 7.33325H13.667C14.0352 7.33325 14.3337 7.63173 14.3337 7.99992C14.3337 8.36811 14.0352 8.66659 13.667 8.66659H2.33366C1.96547 8.66659 1.66699 8.36811 1.66699 7.99992Z"
								fill="#0549CF"
							/>
						</svg>
						<p className="text-primary text-l-med">New Project</p>
					</div>
				)}
				{IsVisible === 'members' &&
					settingVisible == ('owner' || 'admin') && (
						<div
							className="flex cursor-pointer pt-[25px] mr-[32px]"
							onClick={() => setModalInvite(true)}>
							<svg
								style={{ margin: '3px 8px -2px 8px' }}
								width="16"
								height="16"
								viewBox="0 0 16 16"
								fill="none"
								xmlns="http://www.w3.org/2000/svg">
								<path
									d="M7.99967 1.66675C8.36786 1.66675 8.66634 1.96522 8.66634 2.33341V13.6667C8.66634 14.0349 8.36786 14.3334 7.99967 14.3334C7.63148 14.3334 7.33301 14.0349 7.33301 13.6667V2.33341C7.33301 1.96522 7.63148 1.66675 7.99967 1.66675Z"
									fill="#0549CF"
								/>
								<path
									d="M1.66699 7.99992C1.66699 7.63173 1.96547 7.33325 2.33366 7.33325H13.667C14.0352 7.33325 14.3337 7.63173 14.3337 7.99992C14.3337 8.36811 14.0352 8.66659 13.667 8.66659H2.33366C1.96547 8.66659 1.66699 8.36811 1.66699 7.99992Z"
									fill="#0549CF"
								/>
							</svg>
							<p className="text-primary text-l-med">Invite</p>
						</div>
					)}
				{IsVisible === 'delete' &&
					settingVisible == ('owner' || 'admin') &&
					select === false && (
						<div className="flex cursor-pointer pt-[25px] mr-[32px]">
							<p
								className="text-primary text-l-med"
								onClick={() => setSelect(true)}>
								Select
							</p>
						</div>
					)}
				{IsVisible === 'delete' &&
					settingVisible == ('owner' || 'admin') &&
					select === true && (
						<div className="flex cursor-pointer pt-[25px] mr-[32px]">
							<p
								className="text-primary text-l-med"
								onClick={() => setSelect(false)}>
								Cancel
							</p>
						</div>
					)}

				{modalInvite && (
					<ModalInvitation
						role={role}
						roles={roles}
						onChangeRole={(e) => setRole(e)}
						onChangeEmail={(e) => setEmail(e)}
						cancel={() => setModalInvite(false)}
						action={() => handleInviteMember(email, id, role)}
						handleCopyToken={() => handleCopyToken()}
					/>
				)}
			</div>
			<div
				className="px-[32px] pt-[26px] pb-[30px]"
				style={{
					height: 'calc(100vh - 141px)',
					overflow: 'auto'
				}}>
				{subPage[IsVisible]}
			</div>
		</>
	);
}
