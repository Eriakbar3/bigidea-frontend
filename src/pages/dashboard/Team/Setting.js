import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router';
import Card from '../../../components/atoms/card/Card';
import Avatar from '../../../components/atoms/avatar/index';
import Button from '../../../components/elements/Button';
import BoxModalDeleteAccount from '../../../components/molecules/modal/BoxModalDeleteAccount';
import BoxModalValidChange from '../../../components/molecules/modal/BoxModalValidChange';
import CardTeamLeft from '../../../components/molecules/card/card-team-left';
import Tooltip from '../../../components/atoms/tooltip/Tooltip';

export default function Setting(props) {
	const { id } = useParams();
	const dispatch = useDispatch();
	const navigate = useNavigate();
	const { detail_teams, isDeleteTeam } = useSelector((state) => state.team);
	const [modalDeleteTeam, setModalDeleteTeam] = useState(false);
	const [modalValidasiChange, setModalValidasiChange] = useState(false);
	const [modalValidasiDelTeam, setModalValidasiDelTeam] = useState(false);
	const [namaTeamValidasi, setNamaTeamValidasi] = useState('');

	const [teamProfile, setTeamProfile] = useState({
		_id: detail_teams['team']['_id'],
		name: detail_teams['team']['name'],
		team_desc: detail_teams['team']['team_desc']
	});

	useEffect(() => {
		setTeamProfile({
			_id: detail_teams['team']['_id'],
			name: detail_teams['team']['name'],
			team_desc: detail_teams['team']['team_desc']
		});
	}, [id, detail_teams]);

	useEffect(() => {
		if (isDeleteTeam === true) {
			dispatch.team.listTeams();
			navigate('/dashboard');
		}
	}, [isDeleteTeam]);

	const handleChange = (e) => {
		setTeamProfile({
			...teamProfile,
			[e.target.name]: e.target.value
		});
	};
	const handleChangeDesc = (e) => {
		setTeamProfile({
			...teamProfile,
			[e.target.name]: e.target.value
		});
	};
	const handleUpdateTeam = () => {
		setModalValidasiChange(false);
		dispatch.team.updateTeamProfile(teamProfile);
	};
	const handleChangeImage = (file) => {
		const form_data = new FormData();
		form_data.append('image', file);
		form_data.append('_id', teamProfile._id);
		dispatch.team.updateTeamProfile(form_data);
	};

	const handleRemoveImage = () => {
		dispatch.team.resetState();
		if (detail_teams['team']['image']['url'] !== null) {
			dispatch.team.removeImageTeam(detail_teams['team']);
		} else {
			dispatch.team.updateFail(
				'Changes failed to save, please try again'
			);
		}
	};
	const handleDeleteTeam = () => {
		if (namaTeamValidasi.length) {
			dispatch.team.deleteTeam({
				_id: teamProfile._id,
				namaTeam: namaTeamValidasi
			});
			setModalValidasiChange(false);
			setModalDeleteTeam(false);
			setModalValidasiDelTeam(false);
			setNamaTeamValidasi('');
		} else {
			dispatch.team.updateFail(
				'Team failed to deleted, please try again'
			);
		}
	};

	const handleCheckFieldTeamName = async () => {
		dispatch.team.resetState();
		if (namaTeamValidasi != '') {
			setModalDeleteTeam(false);
			setModalValidasiDelTeam(true);
		} else {
			dispatch.team.updateFail('Please input your team name');
		}
	};

	return detail_teams !== '' ? (
		<>
			<div className="grid grid-cols-8 gap-[30px] px-[50px] pt-[50px]">
				<div className="col-span-3">
					<CardTeamLeft data={detail_teams} />
				</div>
				<div className="col-span-5">
					<Card>
						<div className="p-[24px]">
							<div className="flex justify-between">
								<div className="flex">
									<div className="mx-auto">
										<Avatar
											name={detail_teams.team.name}
											width="90px"
											color={
												detail_teams.team.image.color
											}
											image_url={
												detail_teams.team.image.url
													? `https://idea.bigbox.co.id/minio/profile/team/${detail_teams.team.image.url}`
													: null
											}
											fontStyle="head-m-med"
										/>
									</div>
									<div className="mt-[28px] ml-[24px] mr-[22px]">
										<form
											encType="multipart/form-data"
											method="PUT">
											<input
												type="file"
												name="image"
												id="image"
												accept="image/x-png,image/gif,image/jpeg"
												className="w-[0.1px] h-[0.1px] opacity-0 overflow-hidden absolute -z-1"
												onChange={(e) =>
													handleChangeImage(
														e.target.files[0]
													)
												}
											/>
											<div className="btn-change-img cursor-pointer">
												<label
													htmlFor="image"
													className="text-m-med text-neutral-80 cursor-pointer">
													Change Picture
												</label>
											</div>
										</form>
									</div>
									<div className="mt-[35px] cursor-pointer">
										<div
											className="my-auto bi-ic-trash-2 text-neutral-60 w-[20px] h-[20px]"
											onClick={() =>
												handleRemoveImage()
											}></div>
									</div>
								</div>
							</div>
							<p className="text-neutral-100 text-m-reg pt-[24px] ">
								Team Name
							</p>
							<input
								type="text"
								name="name"
								placeholder="Team Name"
								value={teamProfile.name}
								onChange={(e) => handleChange(e)}
							/>
							<p className="text-neutral-100 text-m-reg mt-[20px]">
								Team Description
							</p>
							<input
								type="text"
								name="team_desc"
								placeholder="Team Description"
								value={teamProfile.team_desc}
								onChange={(e) => handleChangeDesc(e)}
							/>
							<div className="flex justify-between">
								{props.role === 'admin' ? (
									<div className="flex mt-[50px]">
										<Tooltip
											text="Only team owner can delete this team"
											placement="bottom">
											<Button
												classBackgroundColor={
													'bg-neutral-20'
												}
												width="126px"
												heigth="40px"
												borderRadius="5px"
												boxShadow="0px 1px 2px rgba(9, 45, 115, 0.12)"
												classTextColor="text-neutral-60"
												classSizeText="text-l-med"
												content={
													<span>Delete Team</span>
												}
											/>
										</Tooltip>
									</div>
								) : (
									<div
										className="flex mt-[50px]"
										onClick={() =>
											setModalDeleteTeam(true)
										}>
										<Button
											classBackgroundColor={
												'bg-surface-primary'
											}
											width="126px"
											heigth="40px"
											borderRadius="5px"
											boxShadow="0px 1px 2px rgba(9, 45, 115, 0.12)"
											classTextColor="text-primary"
											classSizeText="text-l-med"
											content={<span>Delete Team</span>}
										/>
									</div>
								)}

								{modalDeleteTeam && (
									<BoxModalDeleteAccount
										title="Delete Team"
										width="424px"
										widthButton="75px"
										heightButton="32px"
										footer={true}
										actionName="Delete"
										action={() =>
											handleCheckFieldTeamName()
										}
										textAlign="center"
										cancel={() => {
											setModalDeleteTeam(false);
											setNamaTeamValidasi('');
										}}
										handleCancelAction={true}
										headerClose={true}
										content={
											<div className="mt-[16px]">
												<p className="text-m-med text-neutral-80 mb-[16px]">
													This will permanently delete
													the{' '}
													<span className="text-neutral-100">
														{teamProfile.name}
													</span>
													. Deleting this team will
													delete all associated data,
													such as projects, files, for
													all users.
													<br />
													<br />
													You have{' '}
													<span className="text-neutral-100">
														{' '}
														28 days{' '}
													</span>{' '}
													to undo this operation by
													following instructions that
													will be sent to your email.
													<br />
													<br />
													To confirm, please enter the
													name of the team.
												</p>
												<input
													type="text"
													value={namaTeamValidasi}
													placeholder="Team name"
													onChange={(e) =>
														setNamaTeamValidasi(
															e.target.value
														)
													}
													required
												/>
											</div>
										}
									/>
								)}
								{modalValidasiDelTeam && (
									<BoxModalValidChange
										title="Save Changes"
										footer={true}
										action={() => handleDeleteTeam()}
										discard={() => {
											setModalValidasiDelTeam(false);
											setModalDeleteTeam(true);
										}}
										cancel={() => {
											setModalDeleteTeam(false);
											setModalValidasiDelTeam(false);
											setNamaTeamValidasi('');
										}}
										handleCancelAction={true}
										headerClose={true}
										content={
											<div className="mt-[16px]">
												<p className="text-m-med text-neutral-80 mb-[16px]">
													Do you want to save changes
													before leaving this
													<p>page?</p>
												</p>
											</div>
										}
									/>
								)}
								<div className="flex mt-[50px]">
									<div
										className="btn btn-primary bg-primary text-neutral-10 block w-full"
										onClick={() =>
											setModalValidasiChange(true)
										}>
										Save Changes
									</div>
									{modalValidasiChange && (
										<BoxModalValidChange
											title="Save Changes"
											footer={true}
											action={() => handleUpdateTeam()}
											discard={() =>
												setModalValidasiChange(false)
											}
											cancel={() => {
												setModalValidasiChange(false);
											}}
											handleCancelAction={true}
											headerClose={true}
											content={
												<div className="mt-[16px]">
													<p className="text-m-med text-neutral-80 mb-[16px]">
														Do you want to save
														changes before leaving
														this
														<p>page?</p>
													</p>
												</div>
											}
										/>
									)}
								</div>
							</div>
						</div>
					</Card>
				</div>
			</div>
		</>
	) : (
		<div className="h-screen relative">
			<img
				src={`${window.location.origin}/image/illustration/Loader.gif`}
				className="h-[100px] absolute abs-center"
			/>
		</div>
	);
}
