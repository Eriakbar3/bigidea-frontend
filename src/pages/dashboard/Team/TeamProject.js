import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import CardProject from '../../../components/molecules/card/card-project/CardProject';
import Button from '../../../components/elements/Button';

export default function TeamProject() {
	const dispatch = useDispatch();
	const { id } = useParams();
	const { 
		project_team, 
		isUpdateSuccessProject, 
		isDeleteProject 
	} = useSelector((state) => state.project);
	const { detail_teams } = useSelector((state) => state.team);

	const handleCreateProject = () => {
		dispatch.project.addProject({"id":id});
	};
	
	useEffect(() => {
		dispatch.project.getProjectByTeam(id);
	}, [isUpdateSuccessProject, id, isDeleteProject]);

	useEffect(() => {
		let array = [];
		if (project_team.length > 0) {
			for (let i = 0; i < project_team.length; i++) {
				array.push(project_team[i]['_id'])
			}
			dispatch.canvas.getDataBoard({project_id:array})
		}else{
			dispatch.canvas.getDataBoard([])
		}
	},[project_team])

	return detail_teams !== '' ? (
		<>
			{project_team.length > 0 ? (
				<div>
					<p className="text-neutral-100 head-s-med mb-[15px]">
						Projects
					</p>
					<div className="flex flex-wrap gap-[26px]">
						{project_team.map((project) => (
							<CardProject
								id={id}
								project={project}
								key={project._id}
								detail_teams={detail_teams}
							/>
						))}
					</div>
				</div>
			) : (
				<div className="grid grid-cols-11 pt-[150px] text-center">
					<div className="justify-self-center col-start-5 col-span-3 ">
						<div className="h-[238px]">
							<img src="/image/illustration/document.svg" />
						</div>
						<p className="text-l-reg text-neutral-70">
							You don't have any board yet
						</p>
						<div
							className="pt-[24px]"
							onClick={() => handleCreateProject()}>
							<Button
								classBackgroundColor="bg-primary"
								width="w-full"
								height="40px"
								content="Create Online Whiteboard"
								classSizeText="text-l-med"
								classTextColor="text-neutral-10"
							/>
						</div>
					</div>
				</div>
			)}
		</>
	) : (
		<div className="h-screen relative">
			<img
				src={`${window.location.origin}/image/illustration/Loader.gif`}
				className="h-[100px] absolute abs-center"
			/>
		</div>
	);
}
