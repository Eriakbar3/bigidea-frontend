import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Process from '../../components/atoms/process';
import CardProjectAll from '../../components/molecules/card/card-project-all';
import LastModifiedDropdown from '../../components/molecules/dropdown/last-modified';

export default function MyProjects() {
	const dispatch = useDispatch()
	const { list_teams } = useSelector((state) => state.team);
	const { project_all_user, isUpdateSuccessProject } = useSelector((state) => state.project);
	const auth = useSelector((state) => state.auth);
	const isLoading = useSelector((state) => state.loading.models.project);
	const [arrayProjectAll, setArrayProjectAll] = useState('')

	useEffect(() => {
		let array = [];
		if (list_teams.length > 0) {
			for (let i = 0; i < list_teams.length; i++) {
				array.push(list_teams[i]['_id'])
			}
			dispatch.project.getAllUserProjectByTeam({ 'team': array });
		}
	}, [list_teams, auth.userData, isUpdateSuccessProject]);

	useEffect(() => {
		const array = []
		if (project_all_user.length > 0) {
			for (let i = 0; i < project_all_user.length; i++) {
				for (let j = 0; j < project_all_user[i].length; j++) {
					array.push(project_all_user[i][j])
				}
			}
		}
		setArrayProjectAll(array)
	}, [project_all_user, isUpdateSuccessProject])

	useEffect(() => {
		let array = [];
		if (arrayProjectAll.length > 0) {
			for (let i = 0; i < arrayProjectAll.length; i++) {
				array.push(arrayProjectAll[i]['_id'])
			}
			dispatch.canvas.getDataBoard({ project_id: array })
		} else {
			dispatch.canvas.getDataBoard(array)
		}
	}, [arrayProjectAll])

	return (
		<>
			{isLoading ? <Process /> : <></>}
			{arrayProjectAll.length > 0 ? (
				<div className="px-[32px] pb-[30px]">
					<div className='flex justify-between'>
						<p className="text-neutral-100 head-s-med mt-[26px] mb-[26px]">
							My Projects
						</p>
						<div className='mt-[26px]'>
							<LastModifiedDropdown />
						</div>
					</div>
					<div className="flex flex-wrap gap-[26px]">
						{arrayProjectAll.map((project) => (
							<CardProjectAll
								project={project}
								key={project._id}
								detail_teams={list_teams}
							/>
						))}
					</div>
				</div>
			) : (
				<div className="grid grid-cols-9 pt-[150px] text-center">
					<div className="justify-self-center col-start-4 col-span-3 ">
						<div className="h-[238px] ml-[40px]">
							<img src="/image/illustration/document.svg" />
						</div>
						<p className="text-l-reg text-neutral-70">
							You don't have any recent project opened yet
						</p>
					</div>
				</div>
			)}

		</>
	);
}
