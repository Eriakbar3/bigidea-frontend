import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Outlet, useNavigate } from 'react-router-dom';
import DashboardLayout from '../../components/tempalates/dashboard-layout/DashboardLayout';
import Alert from '../../components/atoms/alert';

export default function Dashboard() {
	const dispatch = useDispatch();
	const navigate = useNavigate();
	const auth = useSelector((state) => state.auth);
	const {
		errorMessageProject,
		successMessageProject,
		isPinProject,
		project_team
	} = useSelector((state) => state.project);
	const { list_teams } = useSelector((state) => state.team);

	useEffect(() => {
		if (!localStorage.getItem('token')) {
			navigate('/signin');
		}
	}, [auth.isAuthenticate, auth.error]);

	useEffect(() => {
		if (!auth.isLoginSuccess) {
			dispatch.auth.userProfile();
		}
	}, ['']);

	useEffect(() => {
		dispatch.project.getPinProjectTeam();
	}, [isPinProject]);

	useEffect(() => {
		if (auth.userData) {
			if (auth.userData.onboarding < 2) {
				setTimeout(function () {
					navigate('/onboarding');
				}, 1000);
			}
		}
	}, [auth.userData]);

	useEffect(() => {
		if (errorMessageProject?.length) {
			const alert = new Alert();
			alert.error(errorMessageProject);
		}
		if (successMessageProject?.length) {
			const alert = new Alert();
			alert.success(successMessageProject);
		}
		dispatch.project.resetState();
	}, [errorMessageProject, successMessageProject]);

	useEffect(() => {
		let array = [];
		if (list_teams.length > 0) {
			for (let i = 0; i < list_teams.length; i++) {
				array.push(list_teams[i]['_id']);
			}
			dispatch.project.getAllUserProjectByTeam({ team: array });
		}
	}, [list_teams, project_team]);

	return auth.isAuthenticate && auth?.userData?.onboarding > 1 ? (
		<DashboardLayout>
			<Outlet />
		</DashboardLayout>
	) : (
		<div className="h-screen relative">
			<img
				src={`${window.location.origin}/image/illustration/Loader.gif`}
				className="h-[100px] absolute abs-center"
			/>
		</div>
	);
}
