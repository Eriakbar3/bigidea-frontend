import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Process from '../../components/atoms/process';
import CardProjectAll from '../../components/molecules/card/card-project-all';


export default function Pinned() {
	const dispatch = useDispatch();
	const { project_pinned, project_by_id, isPinProject, isDeleteProject } = useSelector((state) => state.project);
	const { list_teams } = useSelector((state) => state.team);
	const isLoading = useSelector((state) => state.loading.global);

	useEffect(() => {
		dispatch.project.getPinProjectTeam();
	},[isPinProject, isDeleteProject])
	
	useEffect(() => {
		if (project_pinned.length > 0) {
			dispatch.project.getProjectId(project_pinned)
		}else{
			dispatch.project.emptyPinProject()
		}
	}, [isPinProject, project_pinned, isDeleteProject])

	useEffect(() => {
		let array = [];
		if (project_pinned.length > 0) {
			for (let i = 0; i < project_pinned.length; i++) {
				array.push(project_pinned[i]['project'])
			}
			dispatch.canvas.getDataBoard({project_id:array})
		}else{
			dispatch.canvas.getDataBoard(array)
		}
	},[project_pinned])

	return (
		<>	
			{isLoading ? <Process /> : <></>}
			{project_by_id.length > 0 ? (
				<div className="px-[32px] pb-[30px]">
					<p className="text-neutral-100 head-s-med mt-[26px] mb-[26px]">
						Pinned
					</p>
					<div className="flex flex-wrap gap-[26px]">
						{project_by_id.map((project) => (
							<CardProjectAll
								project={project}
								key={project._id}
								detail_teams={list_teams}
							/>
						))}
					</div>
				</div>
				):(
				<div className="grid grid-cols-9 pt-[150px] text-center">
					<div className="justify-self-center col-start-4 col-span-3 ">
						<div className="h-[238px]">
							<img src="/image/illustration/document.svg" />
						</div>
						<p className="text-l-reg text-neutral-70">
							You don't have any pinned board yet
						</p>
					</div>
				</div>
			)}
		</>
	);
}
