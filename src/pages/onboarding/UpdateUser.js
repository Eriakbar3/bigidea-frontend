import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Button from '../../components/atoms/button/index.js';
import InputSelect from '../../components/atoms/input/InputSelect.js';

export default function UpdateUser() {
	const [name, setName] = useState('');
	const [role, setRole] = useState('');
	const [other, setOther] = useState('');

	const roles = [
		'Project Manager',
		'Product Manager',
		'Engineering',
		'Scrum Master',
		'UX Reseacher',
		'UX Designer',
		'Marketing',
		'Freelancer',
		'Support',
		'Consultan/Professional Service',
		'IT Services',
		'Student',
		'Teacher',
		'Others'
	];

	const dispatch = useDispatch();
	const isLoading = useSelector((state) => state.loading.models.auth);
	const authState = useSelector((state) => state.auth);

	const handleSubmit = (e) => {
		e.preventDefault();
		if (role.length && name.length)
			dispatch.auth.updateUserProfile({
				name,
				role: role === 'Other' ? other : role,
				onboarding: 1
			});
		else dispatch.auth.updateFail('Your name / role cannot be empty.');
	};

	return (
		<>
			{authState.error && !isLoading ? (
				<p className="auth-error-message text-l-medium ">
					{authState.error}
				</p>
			) : (
				<></>
			)}
			<div className="slide-in">
				<p className="head-m-med text-center mb-[50px]">
					Welcome to BigIdea!
				</p>
				<form onSubmit={handleSubmit}>
					<input
						type="text"
						placeholder="What is your full name?"
						onChange={(e) => setName(e.target.value)}
					/>
					<InputSelect
						value={role}
						options={roles}
						placeholder="What is your role in the team?"
						onChange={(e) => setRole(e)}
					/>
					{role === 'Others' ? (
						<input
							type="text"
							placeholder="Other"
							value={other}
							onChange={(e) => setOther(e.target.value)}
						/>
					) : (
						<></>
					)}
					<Button
						htmlType="submit"
						type="primary"
						fluid={true}
						isLoading={isLoading}>
						save
					</Button>
				</form>
			</div>
		</>
	);
}
