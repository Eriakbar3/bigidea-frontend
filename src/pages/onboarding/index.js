import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import SetupTeam from './SetupTeam';
import UpdateUser from './UpdateUser';

export default function OnBoarding() {
	const auth = useSelector((state) => state.auth);
	const navigate = useNavigate();

	const [current, setCurrent] = useState(0);

	const contents = [
		{ content: <UpdateUser />, name: 'user' },
		{ content: <SetupTeam />, name: 'team' }
	];

	useEffect(() => {
		if (auth?.userData?.onboarding < 2)
			setCurrent(auth.userData.onboarding);
		else navigate('/dashboard');
	}, [auth.userData]);

	return (
		<div className="flex min-h-screen">
			<div className="bg-surface-primary flex basis-1/2">
				<img
					src="image/illustration/onboarding.svg"
					className="m-auto slide-in"
				/>
			</div>
			<div className="relative flex basis-1/2">
				<div className="m-auto w-[380px] carousel">
					<div className="navigation ">
						{contents.map((i) => (
							<div
								key={i.name}
								className={`item ${
									contents[current].name === i.name &&
									'active'
								}`}></div>
						))}
					</div>
					<div className="mt-[42px]">{contents[current].content}</div>
				</div>
			</div>
		</div>
	);
}
