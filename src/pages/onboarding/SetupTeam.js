import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Button from '../../components/atoms/button';

export default function SetupTeam() {
	const [teamName, setTeamName] = useState('');

	const dispatch = useDispatch();
	const team = useSelector((state) => state.team);
	const isLoading = useSelector((state) => state.loading.models.team);

	const handleSubmit = (e) => {
		e.preventDefault();
		if (teamName.length) {
			dispatch.team.addTeam({ name: teamName });
		} else {
			dispatch.team.requestFail('Team name cannot be empty.');
		}
	};

	useEffect(() => {
		if (team.list_teams.length) {
			dispatch.auth.updateUserProfile({
				onboarding: 2
			});
		}
	}, [team.list_teams]);

	return (
		<>
			{team.error && !isLoading ? (
				<p className="auth-error-message text-l-medium ">
					{team.error}
				</p>
			) : (
				<></>
			)}
			<div className="slide-in">
				<p className="head-m-med text-center">Set up your team!</p>
				<p className="text-m-reg text-neutral-80 text-center mt-[14.5px] mb-[15.5px]">
					What is your team name or company?
				</p>
				<form onSubmit={handleSubmit}>
					<input
						type="text"
						placeholder="e.g. BigBox"
						value={teamName}
						onChange={(e) => setTeamName(e.target.value)}
					/>
					<Button
						htmlType="submit"
						type="primary"
						fluid={true}
						isLoading={isLoading}>
						Save
					</Button>
				</form>
			</div>
		</>
	);
}
