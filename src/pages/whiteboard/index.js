import React, { useEffect } from 'react';
import Header from '../../components/molecules/header/whiteboard-header';
import Canvas from './Canvas/index.js';
import './index.css';
import ToolBar from '../../components/molecules/toolbar';
import InteractiveToolbar from '../../components/molecules/interactive-toolbar';
import { useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
export default function Whiteboard() {
	const { id } = useParams();
	const dispatch = useDispatch();
	const isLoading = useSelector(
		(state) => state.loading.effects.canvas.getBoard
	);
	const { boardExist } = useSelector((state) => state.canvas);
	const navigate = useNavigate();

	useEffect(() => {
		dispatch.canvas.getBoard(id);
	}, []);

	useEffect(() => {
		if (!boardExist) navigate('/dashboard');
	}, [boardExist]);

	return (
		<>
			{isLoading && !boardExist ? (
				<div className="h-screen relative">
					<img
						src={`${window.location.origin}/image/illustration/Loader.gif`}
						className="h-[100px] absolute abs-center"
					/>
				</div>
			) : (
				<></>
			)}
			<>
				<Header />
				{/* <ToolZoom /> */}
				<ToolBar />
				<InteractiveToolbar />
				<Canvas />
			</>
		</>
	);
}
