import React, {
	useCallback,
	useEffect,
	useLayoutEffect,
	useRef,
	useState
} from 'react';
import './index.css';
import { Grid2 } from '../../../utils/objectCanvas';
import ZoomToolbar from '../../../components/molecules/zoom-toolbar';
import { useDispatch, useSelector } from 'react-redux';
import { createElement, drawElement } from '../../../utils/action/tool';
import {
	handleWheel,
	handleZoomIn,
	handleZoomOut
} from '../../../utils/action/zoom-and-pan';
import { useParams } from 'react-router-dom';
import {
	adjustElementCoordinate,
	cursorForPosition,
	findElements,
	getMousePosisitonOnSelectedElement,
	resizedCoordinates
} from '../../../utils';
import { v4 as uuid } from 'uuid';

export default function Canvas() {
	const canvasRef = useRef(null);
	let canvas = null;
	let ctx = null;

	const {
		cursor,
		scale,
		offset,
		zoomPoint,
		tool,
		action,
		elements,
		selectedElement
	} = useSelector((state) => state.canvas);
	const dispatch = useDispatch();
	const [transform, setTransform] = useState({ x: 0, y: 0 });
	const [posAtEl, setPosAtEl] = useState(null);

	const param = useParams();
	const textAreaRef = useRef(null);

	useLayoutEffect(() => {
		canvas = canvasRef.current;
		// const ratio = window.devicePixelRatio;
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
		ctx = canvas.getContext('2d');
		// ctx.scale(1, 1)
		// ctx.translate(0, 0);
		let left = -(6 * canvas.width) + 0.5;
		let top = -(6 * canvas.height) + 0.5;
		let right = 6 * canvas.width;
		let bottom = 6 * canvas.height;
		ctx.save();
		ctx.translate((1 - scale) * zoomPoint.x, (1 - scale) * zoomPoint.y);
		ctx.scale(scale, scale);
		ctx.translate(offset.x, offset.y);
		Grid2(ctx, left, top, right, bottom);
		console.log('redraw');
		elements.length &&
			elements.forEach((element) => {
				if (
					element.type === 'text' &&
					selectedElement?.id === element?.id &&
					action === 'writing'
				)
					return;
				drawElement(element, ctx);
			});
		// const mySticky = new Sticky(ctx, 100, 100, 200, 200);
		// mySticky.template();
		const { a, e, f } = ctx.getTransform();
		setTransform({ x: e, y: f, scale: a });
		ctx.restore();
		if (selectedElement) {
			if (selectedElement.type === 'line')
				drawLineSelection(ctx, selectedElement);
			else DrawBoundingRect(ctx, selectedElement);
		}
	}, [scale, offset, elements, selectedElement, action]);

	useEffect(() => {
		if (canvasRef.current) {
			canvasRef.current.addEventListener('wheel', (e) => {
				handleWheel(e, dispatch);
			});
		}
	}, []);

	useEffect(() => {
		const textArea = textAreaRef.current;
		if (action === 'writing') {
			textArea.value = selectedElement.text;
			setTimeout(function () {
				textArea.focus();
			}, 100);
		}
	}, [selectedElement, action]);

	const DrawBoundingRect = (ctx, element) => {
		const { x1, y1, x2, y2 } = element;
		const x1new = x1 - 1;
		const y1new = y1 - 1;
		const w = x2 - x1 + 2;
		const h = y2 - y1 + 2;
		ctx.save();
		ctx.translate((1 - scale) * zoomPoint.x, (1 - scale) * zoomPoint.y);
		ctx.scale(scale, scale);
		ctx.translate(offset.x, offset.y);
		ctx.strokeStyle = 'rgba(5, 73, 207, 1)';
		ctx.lineWidth = 0.5;
		ctx.strokeRect(x1new, y1new, w, h);
		ctx.strokeRect(x1new - 4, y1new - 4, 8, 8);
		ctx.strokeRect(x1new + w - 4, y1new + -4, 8, 8);
		ctx.strokeRect(x1new - 4, y1new + h + -4, 8, 8);
		ctx.strokeRect(x1new + w - 4, y1new + h + -4, 8, 8);
		ctx.restore();
	};

	const drawLineSelection = (ctx, element) => {
		const { x1, y1, x2, y2 } = element;
		ctx.save();
		ctx.translate((1 - scale) * zoomPoint.x, (1 - scale) * zoomPoint.y);
		ctx.scale(scale, scale);
		ctx.translate(offset.x, offset.y);
		ctx.strokeStyle = 'rgba(5, 73, 207, 1)';
		ctx.fillStyle = '#FFFFFF';
		ctx.lineWidth = 1;
		ctx.beginPath();
		ctx.arc(x1, y1, 4, 0, 2 * Math.PI);
		ctx.stroke();
		ctx.fill();
		ctx.beginPath();
		ctx.arc(x2, y2, 4, 0, 2 * Math.PI);
		ctx.stroke();
		ctx.fill();
		ctx.restore();
	};
	const handelMouseDown = (e) => {
		if (action === 'writing') return;
		const { clientX, clientY } = e;
		if (tool === 'select') {
			const el = findElements(
				(clientX - transform.x) / transform.scale,
				(clientY - transform.y) / transform.scale,
				elements
			);
			dispatch.canvas.setSelectedElement(el);
		} else {
			const id = uuid();
			const x = (clientX - transform.x) / transform.scale;
			const y = (clientY - transform.y) / transform.scale;
			const element = createElement(
				id,
				x,
				y,
				tool === 'text' ? x + 150 : x,
				tool === 'text' ? y + 16 : y,
				tool
			);
			dispatch.canvas.setElements(element);
			if (tool === 'text') dispatch.canvas.setSelectedElement(element);
			dispatch.canvas.setAction(tool === 'text' ? 'writing' : 'drawing');
		}
		if (selectedElement && posAtEl) {
			if (posAtEl === 'inside') {
				const mouseStartX = (clientX - transform.x) / transform.scale;
				const mouseStartY = (clientY - transform.y) / transform.scale;
				dispatch.canvas.setSelectedElement({
					...selectedElement,
					mouseStartX,
					mouseStartY
				});
				dispatch.canvas.setAction('moving');
			} else {
				dispatch.canvas.setSelectedElement(selectedElement);
				dispatch.canvas.setAction('resizing');
			}
		}
	};

	const handleMove = (clientX, clientY) => {
		const { id, x1, y1, x2, y2, mouseStartX, mouseStartY } =
			selectedElement;
		const dx = clientX - mouseStartX;
		const dy = clientY - mouseStartY;
		const index = elements.findIndex((i) => i.id === id);
		const currElements = elements;
		const newEl = {
			...currElements[index],
			x1: x1 + dx,
			y1: y1 + dy,
			x2: x2 + dx,
			y2: y2 + dy
		};
		currElements[index] = newEl;
		dispatch.canvas.updateElements(currElements);
		dispatch.canvas.setSelectedElement({
			...newEl,
			mouseStartX: mouseStartX + dx,
			mouseStartY: mouseStartY + dy
		});
	};

	const handleResize = (clientX, clientY) => {
		const { id, ...coordinates } = selectedElement;
		const { x1, y1, x2, y2 } = resizedCoordinates(
			clientX,
			clientY,
			posAtEl,
			coordinates
		);
		const index = elements.findIndex((i) => i.id === id);
		const currElements = elements;
		const newEl = {
			...currElements[index],
			x1,
			y1,
			x2,
			y2
		};
		currElements[index] = newEl;
		dispatch.canvas.updateElements(currElements);
		dispatch.canvas.setSelectedElement(newEl);
	};

	const handelMouseMove = (e) => {
		const { clientX, clientY } = e;
		if (action === 'drawing') {
			const { x1, y1, id } = elements[0];
			const curElements = elements;
			const x2 = (clientX - transform.x) / transform.scale;
			const y2 = (clientY - transform.y) / transform.scale;
			curElements[0] = createElement(id, x1, y1, x2, y2, tool);
			dispatch.canvas.updateElements(curElements);
		}
		if (action === 'moving') {
			handleMove(
				(clientX - transform.x) / transform.scale,
				(clientY - transform.y) / transform.scale
			);
		}
		if (action === 'resizing') {
			handleResize(
				(clientX - transform.x) / transform.scale,
				(clientY - transform.y) / transform.scale
			);
		}
		if (selectedElement) {
			const pos = getMousePosisitonOnSelectedElement(
				(clientX - transform.x) / transform.scale,
				(clientY - transform.y) / transform.scale,
				selectedElement
			);
			setPosAtEl(pos);
			if (pos) dispatch.canvas.setCursor(cursorForPosition(pos));
			else dispatch.canvas.setCursor('default');
		}
	};

	const handleMouseUp = (e) => {
		if (action === 'drawing') {
			const { x1, y1, x2, y2 } = adjustElementCoordinate(elements[0]);
			const curElements = elements;
			if (Math.abs(x1 - x2) > 0 || Math.abs(y1 - y2) > 0) {
				curElements[0] = createElement(
					curElements[0].id,
					x1,
					y1,
					x2,
					y2,
					tool
				);
			} else {
				curElements[0] = createElement(
					curElements[0].id,
					x1,
					y1,
					x2 + 100,
					y2 + 100,
					tool
				);
			}
			dispatch.canvas.updateElements(curElements);
			dispatch.canvas.setSelectedElement(curElements[0]);
			dispatch.canvas.setTool('select');
			dispatch.canvas.setAction('none');
			dispatch.canvas.setCursor('default');
			dispatch.canvas.updateBoard(param.id);
			// setStartDrawing(false);
		}
		// if(action === "writing")
		if (action === 'moving' || action === 'resizing') {
			dispatch.canvas.setTool('select');
			dispatch.canvas.setAction('none');
			dispatch.canvas.updateBoard(param.id);
		}
		if (tool === 'select' && selectedElement) {
			const { clientX, clientY } = e;
			// if(selectedElement.type === "text"){
			// 	dispatch.canvas.setAction("writing")
			// }
			const pos = getMousePosisitonOnSelectedElement(
				(clientX - transform.x) / transform.scale,
				(clientY - transform.y) / transform.scale,
				selectedElement
			);
			setPosAtEl(pos);
			dispatch.canvas.setCursor(cursorForPosition(pos));
		} else dispatch.canvas.setCursor('default');
	};

	const handleDelete = () => {
		if (selectedElement) {
			const newElements = elements.filter(
				(el) => el.id !== selectedElement.id
			);
			dispatch.canvas.updateElements(newElements);
			dispatch.canvas.setSelectedElement(null);
			dispatch.canvas.updateBoard(param.id);
			dispatch.canvas.setCursor('default');
		}
	};

	const handleKeydown = useCallback(
		(e) => {
			if (e.keyCode === 46) {
				handleDelete();
			}
		},
		[selectedElement]
	);

	document.addEventListener('keydown', handleKeydown);

	const handleBlur = (event) => {
		if (action === 'moving') return;
		canvas = canvasRef.current;
		ctx = canvas.getContext('2d');
		const { id } = selectedElement;
		dispatch.canvas.setAction('none');
		dispatch.canvas.setTool('select');
		const index = elements.findIndex((i) => i.id === id);
		const currElements = elements;
		const newEl = {
			...currElements[index],
			text: event.target.value,
			y2: currElements[index].y1 + inputHeight
		};
		currElements[index] = newEl;
		dispatch.canvas.updateElements(currElements);
		dispatch.canvas.updateBoard(param.id);
	};

	const [inputHeight, setInputHeight] = useState(36);

	const handleDoubleClick = (e) => {
		const { clientX, clientY } = e;
		const el = findElements(
			(clientX - transform.x) / transform.scale,
			(clientY - transform.y) / transform.scale,
			elements
		);
		if (
			tool === 'select' &&
			selectedElement.type === 'text' &&
			selectedElement.id === el.id
		) {
			dispatch.canvas.setAction('writing');
		}
	};

	return (
		<div className="canvas-container">
			{action === 'writing' && selectedElement ? (
				<textarea
					ref={textAreaRef}
					onBlur={handleBlur}
					onChange={(e) => {
						setInputHeight(e.target.scrollHeight);
						dispatch.canvas.setSelectedElement({
							...selectedElement,
							y2: selectedElement.y1 + e.target.scrollHeight,
							text: e.target.value
						});
					}}
					placeholder="Input text here"
					style={{
						position: 'fixed',
						top: selectedElement.y1 - 2,
						left: selectedElement.x1,
						width: `${selectedElement.x2 - selectedElement.x1}px`,
						// height:"auto",
						height: `${selectedElement.y2 - selectedElement.y1}px`,
						font: '16px sans-serif',
						lineHeight: '16px',
						fontWeight: 400,
						margin: 0,
						padding: 0,
						border: 0,
						outline: 0,
						resize: 'none',
						overflow: 'hidden',
						// whiteSpace: "pre",
						background: 'transparent',
						zIndex: 1000
					}}
				/>
			) : null}
			<canvas
				className="canvas"
				ref={canvasRef}
				onMouseDown={handelMouseDown}
				onMouseMove={handelMouseMove}
				onMouseUp={handleMouseUp}
				onDoubleClick={handleDoubleClick}
				style={{
					height: window.innerHeight + 'px',
					width: window.innerWidth + 'px',
					cursor
				}}></canvas>
			<ZoomToolbar
				zoomIn={() => handleZoomIn(dispatch)}
				zoomOut={() => handleZoomOut(dispatch)}
				scale={scale}
			/>
		</div>
	);
}
