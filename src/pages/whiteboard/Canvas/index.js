import React, {
	// useCallback,
	useEffect,
	useLayoutEffect,
	useRef,
	useState
} from 'react';
import './index.css';
import { Grid2 } from '../../../utils/objectCanvas';
import ZoomToolbar from '../../../components/molecules/zoom-toolbar';
import { useDispatch, useSelector } from 'react-redux';
import { createElement, drawElement } from '../../../utils/action/tool';
import {
	handleWheel,
	handleZoomIn,
	handleZoomOut
} from '../../../utils/action/zoom-and-pan';
import { useParams } from 'react-router-dom';
import {
	adjustElementCoordinate,
	cursorForPosition,
	findElements,
	getMousePosisitonOnSelectedElement,
	resizedCoordinates
} from '../../../utils';
import { v4 as uuid } from 'uuid';
import LineToolbox from '../../../components/molecules/toolbox/LineToolbox';
import ShapeToolbox from '../../../components/molecules/toolbox/ShapeToolbox';
import TextToolbox from '../../../components/molecules/toolbox/TextToolbox';
import StickyToolbox from '../../../components/molecules/toolbox/StickyToolbox';

export default function Canvas() {
	const canvasRef = useRef(null);
	let canvas = null;
	let ctx = null;

	const {
		cursor,
		scale,
		offset,
		zoomPoint,
		tool,
		action,
		elements,
		selectedElement,
		initEl
	} = useSelector((state) => state.canvas);
	const dispatch = useDispatch();
	const [transform, setTransform] = useState({ x: 0, y: 0 });
	const [posAtEl, setPosAtEl] = useState(null);
	const [textPos, setTextPos] = useState({
		top: 0,
		left: 0,
		height: 0,
		width: 0
	});

	const param = useParams();
	const textAreaRef = useRef(null);

	useLayoutEffect(() => {
		canvas = canvasRef.current;
		// const ratio = window.devicePixelRatio;
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
		ctx = canvas.getContext('2d');
		// ctx.scale(1, 1)
		// ctx.translate(0, 0);
		let left = -(6 * canvas.width) + 0.5;
		let top = -(6 * canvas.height) + 0.5;
		let right = 6 * canvas.width;
		let bottom = 6 * canvas.height;
		ctx.save();
		ctx.translate((1 - scale) * zoomPoint.x, (1 - scale) * zoomPoint.y);
		ctx.scale(scale, scale);
		ctx.translate(offset.x, offset.y);
		Grid2(ctx, left, top, right, bottom);
		elements.length &&
			elements.forEach((element) => {
				drawElement(
					element,
					ctx,
					selectedElement?.id === element?.id && action === 'writing'
				);
			});
		const { a, e, f } = ctx.getTransform();
		setTransform({ x: e, y: f, scale: a });
		ctx.restore();
		if (selectedElement) {
			if (selectedElement.type === 'line')
				drawLineSelection(ctx, selectedElement);
			else DrawBoundingRect(ctx, selectedElement);
		}
	}, [scale, offset, elements, selectedElement, action]);

	useEffect(() => {
		if (canvasRef.current) {
			canvasRef.current.addEventListener('wheel', (e) => {
				handleWheel(e, dispatch);
			});
		}
	}, []);

	useEffect(() => {
		const textArea = textAreaRef.current;
		if (action === 'writing') {
			textArea.value = selectedElement.text;
			setTextPos(getTextPosition(selectedElement));
			setTimeout(function () {
				textArea.focus();
			}, 100);
		}
	}, [selectedElement, action]);

	const getTextPosition = (el) => {
		const { x1, y1, x2, y2, type } = el;
		const w = x2 - x1;
		const h = y2 - y1;
		switch (type) {
			case 'text':
				return {
					top: y1 - 2,
					left: x1,
					width: w,
					height: h
				};
			case 'sticky':
			case 'rectangle':
			case 'rounded rectangle':
				return {
					top: y1 + 6,
					left: x1 + 8,
					width: w - 16,
					height: h - 14
				};
			case 'triangle':
				return {
					top: y1 + h / 2 + 4,
					left: x1 + w / 4 + 4,
					width: x2 - w / 4 - (x1 + w / 4) - 6,
					height: y1 + h / 2 - y1 - 8
				};
			case 'circle':
				return {
					left: x1 + w / 4 / Math.sqrt(2) + 1,
					top: y1 + h / 4 / Math.sqrt(2),
					width:
						x2 - w / 4 / Math.sqrt(2) - (x1 + w / 4 / Math.sqrt(2)),
					height:
						y2 - h / 4 / Math.sqrt(2) - (y1 + h / 4 / Math.sqrt(2))
				};
			case 'hexagon':
				return {
					top: y1 + 6,
					left: x1 + w / 4,
					width: x2 - w / 4 - (x1 + w / 4),
					height: h - 16
				};
			case 'trapezoid':
				return {
					top: y1 + 6,
					left: x1 + w / 6,
					width: x2 - w / 6 - (x1 + w / 6),
					height: h - 16
				};
			case 'parallelogram':
				return {
					top: y1 + 6,
					left: x1 + 4 + w / 4,
					width: x2 - w / 4 - (x1 + w / 4) - 4,
					height: h - 16
				};
			case 'arrow right':
				return {
					top: y1 + 8 + h / 5,
					left: x1 + 8,
					width: w - 14 - (w * 3) / 20,
					height: h - 14 - (2 * h) / 5
				};
			// x1: x1+8+(x2-x1)*3/10,
			// y1: y1+8+(y2-y1)*3/8,
			// x2: x2-8-(x2-x1)*3/10,
			// y2: y2-8-(y2-y1)*1/4,
			case 'star':
				return {
					top: y1 + 8 + (h * 3) / 8,
					left: x1 + 8 + (w * 3) / 10,
					width: w - 15 - (w * 3) / 5,
					height: h - 14 - h / 4 - (h * 3) / 8
				};
			default:
				return {
					top: 0,
					left: 0,
					width: 0,
					height: 0
				};
		}
	};

	const DrawBoundingRect = (ctx, element) => {
		let { x1, y1, x2, y2 } = element;
		if (element.type === 'pencil') {
			const x = element.points.map((el) => el.x);
			const y = element.points.map((el) => el.y);
			x1 = Math.min(...x);
			y1 = Math.min(...y);
			x2 = Math.max(...x);
			y2 = Math.max(...y);
		}
		const x1new = x1 - 1;
		const y1new = y1 - 1;
		const w = x2 - x1 + 2;
		const h = y2 - y1 + 2;
		ctx.save();
		ctx.translate((1 - scale) * zoomPoint.x, (1 - scale) * zoomPoint.y);
		ctx.scale(scale, scale);
		ctx.translate(offset.x, offset.y);
		ctx.strokeStyle = 'rgba(5, 73, 207, 1)';
		ctx.lineWidth = 0.5;
		ctx.strokeRect(x1new, y1new, w, h);
		ctx.strokeRect(x1new - 4, y1new - 4, 8, 8);
		ctx.strokeRect(x1new + w - 4, y1new + -4, 8, 8);
		ctx.strokeRect(x1new - 4, y1new + h + -4, 8, 8);
		ctx.strokeRect(x1new + w - 4, y1new + h + -4, 8, 8);
		ctx.restore();
	};

	const drawLineSelection = (ctx, element) => {
		const { x1, y1, x2, y2 } = element;
		ctx.save();
		ctx.translate((1 - scale) * zoomPoint.x, (1 - scale) * zoomPoint.y);
		ctx.scale(scale, scale);
		ctx.translate(offset.x, offset.y);
		ctx.strokeStyle = 'rgba(5, 73, 207, 1)';
		ctx.fillStyle = '#FFFFFF';
		ctx.lineWidth = 1;
		ctx.beginPath();
		ctx.arc(x1, y1, 4, 0, 2 * Math.PI);
		ctx.stroke();
		ctx.fill();
		ctx.beginPath();
		ctx.arc(x2, y2, 4, 0, 2 * Math.PI);
		ctx.stroke();
		ctx.fill();
		ctx.restore();
	};
	const handelMouseDown = (e) => {
		if (action === 'writing') return;
		const { clientX, clientY } = e;
		if (tool === 'select') {
			const el = findElements(
				(clientX - transform.x) / transform.scale,
				(clientY - transform.y) / transform.scale,
				elements
			);
			dispatch.canvas.setSelectedElement(el);
		} else {
			const id = uuid();
			const x = (clientX - transform.x) / transform.scale;
			const y = (clientY - transform.y) / transform.scale;
			const element = createElement(
				id,
				x,
				y,
				tool === 'text' ? x + 150 : x,
				tool === 'text' ? y + 16 : y,
				tool,
				initEl?.style
			);
			dispatch.canvas.setElements(element);
			dispatch.canvas.setSelectedElement(element);
			dispatch.canvas.setAction(tool === 'text' ? 'writing' : 'drawing');
		}
		if (selectedElement && posAtEl) {
			if (posAtEl === 'inside') {
				const mouseStartX = (clientX - transform.x) / transform.scale;
				const mouseStartY = (clientY - transform.y) / transform.scale;
				dispatch.canvas.setSelectedElement({
					...selectedElement,
					mouseStartX,
					mouseStartY
				});
				dispatch.canvas.setAction('moving');
			} else {
				dispatch.canvas.setSelectedElement(selectedElement);
				dispatch.canvas.setAction('resizing');
			}
		}
	};

	const handleMove = (clientX, clientY) => {
		const { id, x1, y1, x2, y2, mouseStartX, mouseStartY } =
			selectedElement;
		const dx = clientX - mouseStartX;
		const dy = clientY - mouseStartY;
		const index = elements.findIndex((i) => i.id === id);
		const currElements = elements;
		let newEl;
		if (selectedElement.type === 'pencil') {
			newEl = {
				...currElements[index],
				points: currElements[index].points.map((cur) => ({
					x: cur.x + dx,
					y: cur.y + dy
				}))
			};
		} else {
			newEl = {
				...currElements[index],
				x1: x1 + dx,
				y1: y1 + dy,
				x2: x2 + dx,
				y2: y2 + dy
			};
		}
		currElements[index] = newEl;
		dispatch.canvas.updateElements(currElements);
		dispatch.canvas.setSelectedElement({
			...newEl,
			mouseStartX: mouseStartX + dx,
			mouseStartY: mouseStartY + dy
		});
	};

	const handleResize = (clientX, clientY) => {
		let newEl;
		let currElements;
		let index;
		if (selectedElement.type === 'pencil') {
			const x = selectedElement.points.map((el) => el.x);
			const y = selectedElement.points.map((el) => el.y);
			const x1pen = Math.min(...x);
			const y1pen = Math.min(...y);
			const x2pen = Math.max(...x);
			const y2pen = Math.max(...y);

			const { id } = selectedElement;
			const { x1, y1, x2, y2 } = resizedCoordinates(
				clientX,
				clientY,
				posAtEl,
				{ x1: x1pen, y1: y1pen, x2: x2pen, y2: y2pen }
			);
			console.log(x1, y1, x2, y2);
			index = elements.findIndex((i) => i.id === id);
			currElements = elements;
			const dx = Math.abs(x1 - x1pen) > 0 ? x1 - x1pen : x2 - x2pen;
			const dy = Math.abs(y1 - y1pen) > 0 ? y1 - y1pen : y2 - y2pen;
			// let newPoints = currElements[index].points.map(i => ({x: i.x+dx, y:i.y+dy}))
			// if(posAtEl === 'tl') newPoints = [...newPoints, cu]]
			newEl = {
				...currElements[index],
				points: currElements[index].points.map((i) => ({
					x: i.x + dx,
					y: i.y + dy
				}))
			};
		} else {
			const { id, ...coordinates } = selectedElement;
			const { x1, y1, x2, y2 } = resizedCoordinates(
				clientX,
				clientY,
				posAtEl,
				coordinates
			);
			index = elements.findIndex((i) => i.id === id);
			currElements = elements;
			newEl = {
				...currElements[index],
				x1,
				y1,
				x2,
				y2
			};
		}
		currElements[index] = newEl;
		dispatch.canvas.updateElements(currElements);
		dispatch.canvas.setSelectedElement(newEl);
	};

	const handelMouseMove = (e) => {
		const { clientX, clientY } = e;
		if (action === 'drawing') {
			const curElements = elements;
			const x2 = (clientX - transform.x) / transform.scale;
			const y2 = (clientY - transform.y) / transform.scale;
			if (elements[0].type === 'pencil') {
				curElements[0] = {
					...elements[0],
					points: [...elements[0].points, { x: x2, y: y2 }]
				};
			} else {
				const { x1, y1, id } = elements[0];
				curElements[0] = createElement(
					id,
					x1,
					y1,
					x2,
					y2,
					tool,
					initEl.style
				);
			}
			dispatch.canvas.updateElements(curElements);
		}
		if (action === 'moving') {
			handleMove(
				(clientX - transform.x) / transform.scale,
				(clientY - transform.y) / transform.scale
			);
		}
		if (action === 'resizing') {
			handleResize(
				(clientX - transform.x) / transform.scale,
				(clientY - transform.y) / transform.scale
			);
		}
		if (selectedElement && action === 'none') {
			const pos = getMousePosisitonOnSelectedElement(
				(clientX - transform.x) / transform.scale,
				(clientY - transform.y) / transform.scale,
				selectedElement
			);
			setPosAtEl(pos);
			if (pos) dispatch.canvas.setCursor(cursorForPosition(pos));
			else dispatch.canvas.setCursor('default');
		}
	};

	const handleMouseUp = (e) => {
		if (action === 'drawing') {
			let curElements;
			if (elements[0].type === 'pencil') {
				const { points } = elements[0];
				curElements = elements;
				if (points.length < 2) {
					curElements.splice(0, 1);
					dispatch.canvas.setSelectedElement(null);
					dispatch.canvas.updateElements(curElements);
					dispatch.canvas.setTool('select');
					dispatch.canvas.setCursor('default');
					dispatch.canvas.setAction('select');
					return;
				}
			} else {
				const { x1, y1, x2, y2 } = adjustElementCoordinate(elements[0]);
				curElements = elements;
				if (Math.abs(x1 - x2) > 0 || Math.abs(y1 - y2) > 0) {
					curElements[0] = createElement(
						curElements[0].id,
						x1,
						y1,
						x2,
						y2,
						tool,
						curElements[0].style
					);
				} else {
					curElements[0] = createElement(
						curElements[0].id,
						x1,
						y1,
						x2 + 100,
						y2 + 100,
						tool,
						curElements[0].style
					);
				}
			}
			dispatch.canvas.updateElements(curElements);
			dispatch.canvas.setSelectedElement(curElements[0]);
			dispatch.canvas.setTool('select');
			dispatch.canvas.setAction(tool === 'sticky' ? 'writing' : 'none');
			dispatch.canvas.setCursor(tool === 'sticky' ? 'text' : 'default');
			dispatch.canvas.updateBoard(param.id);
		}
		// if(action === "writing")
		if (action === 'moving' || action === 'resizing') {
			dispatch.canvas.setTool('select');
			dispatch.canvas.setAction('none');
			dispatch.canvas.updateBoard(param.id);
		}
		if (tool === 'select' && selectedElement) {
			const { clientX, clientY } = e;
			// if(selectedElement.type === "text"){
			// 	dispatch.canvas.setAction("writing")
			// }
			const pos = getMousePosisitonOnSelectedElement(
				(clientX - transform.x) / transform.scale,
				(clientY - transform.y) / transform.scale,
				selectedElement
			);
			setPosAtEl(pos);
			dispatch.canvas.setCursor(cursorForPosition(pos));
		} else dispatch.canvas.setCursor('default');
	};

	const handleDelete = () => {
		if (selectedElement) {
			const newElements = elements.filter(
				(el) => el.id !== selectedElement.id
			);
			dispatch.canvas.updateElements(newElements);
			dispatch.canvas.setSelectedElement(null);
			dispatch.canvas.updateBoard(param.id);
			dispatch.canvas.setCursor('default');
		}
	};

	useEffect(() => {
		const keyDown = (e) => {
			if (e.keyCode === 46) {
				handleDelete();
			}
		};

		document.addEventListener('keydown', keyDown);
		return () => {
			document.removeEventListener('keydown', keyDown);
		};
	}, [handleDelete]);

	const handleBlur = (event) => {
		if (action === 'moving') return;
		if (!selectedElement) return;
		canvas = canvasRef.current;
		ctx = canvas.getContext('2d');
		const { id } = selectedElement;
		dispatch.canvas.setAction('none');
		dispatch.canvas.setTool('select');
		const index = elements.findIndex((i) => i.id === id);
		const currElements = elements;
		const newEl = {
			...currElements[index],
			text: event.target.value,
			y2:
				selectedElement.type === 'text'
					? currElements[index].y1 + inputHeight
					: selectedElement.y2
		};
		currElements[index] = newEl;
		dispatch.canvas.updateElements(currElements);
		dispatch.canvas.updateBoard(param.id);
	};

	const [inputHeight, setInputHeight] = useState(36);

	const handleDoubleClick = () => {
		// const { clientX, clientY } = e;
		// const el = findElements(
		// 	(clientX - transform.x) / transform.scale,
		// 	(clientY - transform.y) / transform.scale,
		// 	elements
		// );
		// if (
		// 	tool === 'select' &&
		// 	(selectedElement.type === 'text' ||
		// 		selectedElement.type === 'sticky'|| selectedElement.type === 'shape') &&
		// 	selectedElement.id === el.id
		// ) {
		if (selectedElement.type !== 'line')
			dispatch.canvas.setAction('writing');
		// }
	};

	const handleUpdateStyle = (style) => {
		const index = elements.findIndex((i) => i.id === selectedElement.id);
		const currElements = elements;
		const newEl = {
			...currElements[index],
			style
		};
		currElements[index] = newEl;
		dispatch.canvas.setSelectedElement(newEl);
		dispatch.canvas.updateElements(currElements);
		dispatch.canvas.updateBoard(param.id);
	};

	const getToolbox = (element) => {
		if (element) {
			if (element.type === 'line')
				return (
					<LineToolbox
						x={element.x1}
						y={element.y1}
						transform={`scale(${transform.scale}) translate(${transform.x}px, ${transform.y}px)`}
						style={selectedElement.style}
						onChange={(e) => handleUpdateStyle(e)}
					/>
				);
			if (element.type === 'sticky')
				return (
					<StickyToolbox
						x={element.x1}
						y={element.y1}
						transform={`scale(${transform.scale}) translate(${transform.x}px, ${transform.y}px)`}
						style={selectedElement.style}
						onChange={(e) => handleUpdateStyle(e)}
					/>
				);
			if (element.type === 'text')
				return (
					<TextToolbox
						x={element.x1}
						y={element.y1}
						transform={`scale(${transform.scale}) translate(${transform.x}px, ${transform.y}px)`}
						style={selectedElement.style}
						onChange={(e) => handleUpdateStyle(e)}
					/>
				);
			return (
				<ShapeToolbox
					x={element.x1}
					y={element.y1}
					transform={`scale(${transform.scale}) translate(${transform.x}px, ${transform.y}px)`}
					style={selectedElement.style}
					onChange={(e) => handleUpdateStyle(e)}
				/>
			);
		}
		return;
	};

	return (
		<div className="canvas-container">
			{action === 'writing' && selectedElement ? (
				<textarea
					ref={textAreaRef}
					onBlur={handleBlur}
					onChange={(e) => {
						if (selectedElement.type === 'text') {
							setInputHeight(e.target.scrollHeight);
							dispatch.canvas.setSelectedElement({
								...selectedElement,
								y2: selectedElement.y1 + e.target.scrollHeight,
								text: e.target.value
							});
						}
					}}
					placeholder="Input text here"
					style={{
						position: 'fixed',
						top: textPos.top,
						left: textPos.left,
						width: `${textPos.width}px`,
						// height:"auto",
						color: selectedElement.style.color,
						height: `${textPos.height}px`,
						font: `${selectedElement.style.fontSize}px ${selectedElement.style.font}`,
						lineHeight: `${selectedElement.style.fontSize}px`,
						fontWeight: selectedElement?.style?.fontWeight
							? selectedElement.style.fontWeight + 100
							: 500,
						margin: 0,
						padding: 0,
						border: 0,
						outline: 0,
						resize: 'none',
						overflow: 'hidden',
						// whiteSpace: "pre",
						background: 'transparent',
						zIndex: 1000,
						// transformOrigin: "50% 50%",
						transform: `scale(${transform.scale}) translate(${transform.x}px, ${transform.y}px)`
					}}
				/>
			) : null}
			<canvas
				className="canvas"
				ref={canvasRef}
				onMouseDown={handelMouseDown}
				onMouseMove={handelMouseMove}
				onMouseUp={handleMouseUp}
				onDoubleClick={handleDoubleClick}
				style={{
					height: window.innerHeight + 'px',
					width: window.innerWidth + 'px',
					cursor
				}}></canvas>
			<ZoomToolbar
				zoomIn={() => handleZoomIn(dispatch)}
				zoomOut={() => handleZoomOut(dispatch)}
				scale={scale}
			/>
			{getToolbox(selectedElement)}
		</div>
	);
}
