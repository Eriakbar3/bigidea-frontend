import React, { useState } from 'react';
import atomList from './atoms';
import Code from './Code';
import './style.css';

export default function Example() {
	// const atomList = [
	//     "Button",
	//     "Input Select",
	//     "Input Password",
	//     "Input Search",
	//     "Avatar"
	// ]
	const moleculeList = ['Project Card'];

	const [active, setActive] = useState(atomList[0]);

	return (
		<div className="layout">
			<div className="sidenav">
				<h1 className="page-title">Component Documentation</h1>
				<div className="item">
					<p className="title">Atom Components</p>
					<div className="sub-item">
						{atomList.map((i) => (
							<p
								key={i.name}
								className={`menu ${active === i && 'active'}`}
								onClick={() => setActive(i)}>
								{i.name}
							</p>
						))}
					</div>
				</div>
				<div className="item" style={{ marginTop: '25px' }}>
					<p className="title">Molecule Components</p>
					<div className="sub-item">
						{moleculeList.map((i) => (
							<p
								key={i}
								className={`menu ${
									active.name === i.name && 'active'
								}`}
								onClick={() => setActive(i)}>
								{i}
							</p>
						))}
					</div>
				</div>
			</div>
			<div className="content">
				<h1 className="title">{active.name}</h1>
				<p className="description">{active.description}</p>
				<div className="example">
					<div className="my-[50px]">{active.example}</div>
					<Code code={active.code} />
				</div>
				<table className="w-full description-table table-auto">
					<thead>
						<tr>
							{Object.keys(active.poperty[0]).map((i) => (
								<th key={i}>{i}</th>
							))}
						</tr>
					</thead>
					<tbody>
						{active.poperty.map((i, idx) => (
							<tr
								key={idx}
								className="hover:bg-neutral-20 cursor-auto">
								{Object.keys(i).map((x, idx) => (
									<td key={idx}>{i[x]}</td>
								))}
							</tr>
						))}
					</tbody>
				</table>
			</div>
		</div>
	);
}
