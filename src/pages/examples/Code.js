import React, { useEffect } from 'react';
import Prism from 'prismjs';
import 'prismjs/themes/prism-tomorrow.css';
import reactElementToJSXString from 'react-element-to-jsx-string';

export default function Code({ code }) {
	useEffect(() => {
		Prism.highlightAll();
	}, []);
	return (
		<div className="Code">
			<pre>
				<code className={`language-javascript`}>
					{reactElementToJSXString(code)}
				</code>
			</pre>
		</div>
	);
}
