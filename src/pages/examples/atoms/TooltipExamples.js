import React from 'react';
import Tooltip from '../../../components/atoms/tooltip/Tooltip';

const code = (
	<div style={{ display: 'flex', gap: '10px' }}>
		<Tooltip text="bottom sfsdfsd fsdfdsf" placement="bottom">
			<u>hover me</u>
		</Tooltip>
		<Tooltip text="top" placement="top">
			<u>hover me</u>
		</Tooltip>
		<Tooltip text="left" placement="left">
			<u>hover me</u>
		</Tooltip>
		<Tooltip text="right" placement="right">
			<u>hover me</u>
		</Tooltip>
	</div>
);

function TooltipExamples() {
	return code;
}

const tooltipContent = {
	name: 'Tooltip',
	example: <TooltipExamples />,
	code,
	description: 'A simple text popup tip.',
	poperty: [
		{
			property: 'placement',
			description: 'Can be set to bottom, top, right, and left',
			type: 'string',
			default: 'bottom'
		},
		{
			property: 'text',
			description: 'The text shown in the tooltip',
			type: 'string',
			default: "''"
		}
	]
};

export default tooltipContent;
