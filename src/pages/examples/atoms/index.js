import buttonContent from './ButtonExamples.js';
import tooltipContent from './TooltipExamples.js';

const atomList = [tooltipContent, buttonContent];

export default atomList;
