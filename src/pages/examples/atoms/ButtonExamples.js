import React from 'react';
import Button from '../../../components/atoms/button';

const code = (
	<div style={{ display: 'flex', gap: '10px' }}>
		<Button type="primary">Primary</Button>
		<Button type="primary" isLoading={true}>
			Primary
		</Button>
		<Button type="secondary" isLoading={true} spinner="inherit">
			Secondary
		</Button>
		<Button type="secondary">Secondary</Button>
	</div>
);

function ButtonExample() {
	return code;
}

const buttonContent = {
	name: 'Button',
	example: <ButtonExample />,
	code,
	description: 'To trigger an operation. You can add any child as anchor tag',
	poperty: [
		{
			property: 'type',
			description: 'Can be set to primary, secondary, transparent',
			type: 'string',
			default: '-'
		},
		{
			property: 'fluid',
			description:
				'Adjust the width of the button according to the container size',
			type: 'boolean',
			default: 'false'
		},
		{
			property: 'htmlType',
			description:
				"Set the original html type of button, ex: 'button', 'submit', 'reset'",
			type: 'string',
			default: 'button'
		},
		{
			property: 'isLoading',
			description: 'loading spinner display',
			type: 'boolean',
			default: 'false'
		},
		{
			property: 'spinner',
			description: "spinner color type: 'inherit' and 'invert'",
			type: 'string',
			default: 'invert'
		},
		{
			property: 'onClick',
			description: 'Set the handler to handle click event',
			type: '(event: MouseEvent) => void',
			default: '-'
		}
	]
};

export default buttonContent;
