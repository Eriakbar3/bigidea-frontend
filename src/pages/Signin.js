import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import InputCheckbox from '../components/atoms/input/InputCheckbox';
import InputPassword from '../components/atoms/input/InputPassword';
import Button from '../components/atoms/button';

export default function Signin() {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const dispatch = useDispatch();
	const navigate = useNavigate();

	const authState = useSelector((state) => state.auth);
	const isLoading = useSelector((state) => state.loading.models.auth);

	const handleSubmit = (e) => {
		e.preventDefault();
		if (email.length && password.length)
			dispatch.auth.login({
				email,
				password
			});
		else {
			dispatch.auth.loginFail('Username or password cannot be empty.');
		}
	};

	useEffect(() => {
		dispatch.auth.updateError('');
		if (localStorage.getItem('token')) navigate('/dashboard');
	}, [authState.isLoginSuccess]);

	return (
		<>
			{authState.error && !isLoading ? (
				<p className="auth-error-message text-l-medium ">
					{authState.error}
				</p>
			) : (
				<></>
			)}
			<div className="flex min-h-screen">
				<div className="bg-surface-primary flex basis-1/2">
					<img
						src="image/illustration/signin.svg"
						className="m-auto slide-in"
					/>
				</div>
				<div className="relative flex basis-1/2">
					<div className="m-auto w-[380px] slide-in">
						<p className="head-m-med text-neutral-100 text-center mb-[50px]">
							Sign In to BigIdea
						</p>
						<form className="mb-[30px]" onSubmit={handleSubmit}>
							<input
								type="email"
								placeholder="Email"
								value={email}
								onChange={(e) => setEmail(e.target.value)}
							/>
							<InputPassword
								placeholder="Password"
								value={password}
								onChange={(e) => setPassword(e)}
							/>
							<div className="flex justify-between mb-[50px]">
								<div className="flex">
									<InputCheckbox />
									<p className="text-m-reg neutral-100 my-auto px-[8px]">
										Remember me
									</p>
								</div>
								<p className="text-primary my-auto text-m-reg">
									Forgot password?
								</p>
							</div>
							<Button
								htmlType="submit"
								type="primary"
								fluid={true}
								isLoading={isLoading}>
								Sign in
							</Button>
						</form>
						<p className="text-neutral-80 text-m-reg text-center">
							Don't have an account?
							<a href="/signup" className="text-primary">
								{' '}
								Sign up
							</a>
						</p>
					</div>
				</div>
			</div>
		</>
	);
}
