import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import Button from '../components/atoms/button';
import InputPassword from '../components/atoms/input/InputPassword';

export default function Signup() {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');

	const dispatch = useDispatch();
	const authState = useSelector((state) => state.auth);
	const isLoading = useSelector((state) => state.loading.models.auth);

	const navigate = useNavigate();

	const handleSubmit = (e) => {
		e.preventDefault();
		if (email.length && password.length) {
			if (password !== confirmPassword) {
				dispatch.auth.loginFail('Password do not match.');
			} else {
				dispatch.auth.signup({
					email,
					password
				});
			}
		} else {
			dispatch.auth.loginFail('Username or password cannot be empty.');
		}
	};

	useEffect(() => {
		if (localStorage.getItem('token')) navigate('/dashboard');
	}, [authState.isSignupSuccess]);

	return (
		<>
			{authState.error && !isLoading ? (
				<p className="auth-error-message text-l-medium ">
					{authState.error}
				</p>
			) : (
				<></>
			)}
			<div className="min-h-screen flex">
				<div className="bg-surface-primary flex basis-1/2">
					<img
						src="image/illustration/signin.svg"
						className="m-auto slide-in"
					/>
				</div>
				<div className="relative flex basis-1/2">
					<div className="m-auto w-[380px] slide-in">
						<p className="head-m-med text-neutral-100 text-center mb-[50px]">
							Sign Up to BigIdea
						</p>
						<form className="mb-[30px]" onSubmit={handleSubmit}>
							<input
								type="email"
								placeholder="Email"
								value={email}
								onChange={(e) => setEmail(e.target.value)}
							/>
							<InputPassword
								placeholder="Password"
								value={password}
								onChange={(e) => setPassword(e)}
							/>
							<InputPassword
								placeholder="Confirm Password"
								value={confirmPassword}
								onChange={(e) => setConfirmPassword(e)}
							/>
							<Button
								htmlType="submit"
								type="primary"
								fluid={true}
								isLoading={isLoading}>
								Sign up
							</Button>
						</form>
						<p className="text-neutral-80 text-m-reg text-center">
							Don't have an account?
							<a href="/signin" className="text-primary">
								{' '}
								Sign In
							</a>
						</p>
					</div>
				</div>
			</div>
		</>
	);
}
