import React from 'react';

export default function PageNotfound() {
	return (
		<div className="relative h-screen bg-surface-primary">
			<div className="absolute abs-center">
				<img
					src={`${window.location.origin}/image/illustration/page-not-found.svg`}
				/>
				<div>
					<p className="text-l-med mt-[50px] text-center">
						Looks like you are lost, the page you looking for is not
						found.
						<br />
						You can go back to{' '}
						<span className="text-primary">previous page.</span>
					</p>
				</div>
			</div>
		</div>
	);
}
