import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router';
import DashboardHeader from '../../components/molecules/header/dashboard-header/DashboardHeader';
import CardProfileLeft from '../../components/molecules/card/card-profile-left';
import CardProfileRight from '../../components/molecules/card/card-profile-right';
import Alert from '../../components/atoms/alert';
import Process from '../../components/atoms/process';

export default function MyProfile() {
	const dispatch = useDispatch();
	const navigate = useNavigate();
	const { userData, errorMessage, successMessage, isDeleteUser } = useSelector(
		(state) => state.auth
	);
	const isLoading = useSelector((state) => state.loading.global);

	useEffect(() => {
		dispatch.auth.userProfile();
	}, []);

	useEffect(() => {
		if (userData === null){
			navigate('/signin');
		}
	},[userData])

	useEffect(() => {
		if (isDeleteUser === true){
			navigate('/signin');
			dispatch.auth.resetState();
		}
	},[isDeleteUser])

	// useEffect(() => {
	// 	if (isDeleteUser === true){
	// 		dispatch.auth.signOut()
	// 		navigate('/signin');
	// 		dispatch.auth.resetState()
	// 	}
	// },[isDeleteUser])

	useEffect(() => {
		if (errorMessage?.length) {
			const alert = new Alert();
			alert.error(errorMessage);
		}
		if (successMessage?.length) {
			const alert = new Alert();
			alert.success(successMessage);
		}
	}, [errorMessage, successMessage]);

	return userData !== null ? (
		<>
			{isLoading ? <Process /> : <></>}
			<DashboardHeader />
			<div style={{ height: 'calc(100vh - 70px)', overflow: 'auto' }}>
				<Link to={`/dashboard/my-projects`}>
					<div className="flex text-primary text-l-med mb-[24px] px-[180px] pt-[32px] cursor-pointer">
						<div className="bi-ic-corner-up-left mt-[4px] mr-[10px]"></div>
						<p>Back to my Projects</p>
					</div>
				</Link>
				<div className="grid grid-cols-8 gap-[30px] px-[180px]">
					<div className="col-span-3">
						<CardProfileLeft />
					</div>
					<div className="col-span-5">
						<CardProfileRight />
					</div>
				</div>
			</div>
		</>
	) : (
		<div className="h-screen relative">
			<img
				src={`${window.location.origin}/image/illustration/Loader.gif`}
				className="h-[100px] absolute abs-center"
			/>
		</div>
	);
}
