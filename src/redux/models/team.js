import {
	getListTeams,
	getTeamMember,
	serviceAddTeam,
	getDetailTeam,
	updateTeam,
	ServiceRemoveImageTeam,
	ServiceDeleteTeam,
	ServiceJoinTeam,
	ServiceEditRoleMember,
	ServiceRemoveMember,
	ServiceInviteMember
} from '../../service/team';

export const teamModel = {
	state: {
		list_teams: [],
		team_members: [],
		detail_teams: '',
		errorMessage: '',
		successMessage: '',
		error: '',
		isUpdateSuccess: false,
		isDeleteTeam: false
	},
	reducers: {
		requestSuccess(state) {
			return {
				...state,
				error: ''
			};
		},
		requestFail(state, error) {
			return {
				...state,
				error
			};
		},
		updateTeamMembers(state, members) {
			return {
				...state,
				team_members: [...state.team_members, ...members]
			};
		},
		updateViewTeamMembers(state, members) {
			return {
				...state,
				// team_members: [...state.team_members, ...members]
				team_members: [...members]
			};
		},
		teamList(state, list_teams) {
			return {
				...state,
				list_teams
			};
		},
		updateListTeams(state, team) {
			return {
				...state,
				list_teams: [...state.list_teams, team]
			};
		},
		updateDetailTeam(state, detail_teams) {
			return {
				...state,
				detail_teams
			};
		},
		updateDetail(state, payload) {
			return {
				...state,
				...payload
			};
		},
		updateSuccess(state, successMessage) {
			return {
				...state,
				isUpdateSuccess: true,
				error: '',
				successMessage
			};
		},
		updateFail(state, error) {
			return {
				...state,
				isUpdateSuccess: false,
				error,
				errorMessage: error
			};
		},
		deleteSuccess(state, successMessage) {
			return {
				...state,
				isUpdateSuccess: true,
				error: '',
				isDeleteTeam: true,
				successMessage
			};
		},
		resetState(state) {
			return {
				...state,
				errorMessage: '',
				successMessage: '',
				isUpdateSuccess: false,
				isDeleteTeam: false
			};
		},
		clearMessage(state) {
			return {
				...state,
				errorMessage: '',
				successMessage: ''
			};
		},
		emptyListTeam(state) {
			return {
				...state,
				list_teams: []
			};
		}
	},
	effects: () => ({
		async getMembers(payload) {
			this.resetState();
			const { data } = await getTeamMember(payload);
			if (!data.error) {
				this.updateViewTeamMembers(data);
				this.requestSuccess();
			} else {
				this.requestFail(data.message);
			}
		},
		async listTeams() {
			this.resetState();
			const { data } = await getListTeams();
			if (!data.error) {
				this.requestSuccess();
				this.teamList(data);
			} else {
				this.requestFail(data.message);
			}
		},
		async addTeam(payload) {
			this.resetState();
			const { data } = await serviceAddTeam(payload);
			if (!data.error) {
				this.requestSuccess();
				this.updateListTeams(data);
				this.updateSuccess('Your Team has succesfully create');
			} else {
				this.requestFail(data.message);
				this.updateFail(data.message);
			}
		},
		async getDetailTeams(payload) {
			this.resetState();
			const { data } = await getDetailTeam(payload);
			if (!data.error) {
				this.requestSuccess();
				this.updateDetailTeam(data);
			} else {
				this.requestFail(data.message);
			}
		},
		async updateTeamProfile(payload) {
			this.resetState();
			const { data } = await updateTeam(payload);
			if (!data.error) {
				this.updateDetail({
					detail_teams: data,
					isAuthenticate: true
				});
				this.updateSuccess('Saved Changes');
			} else {
				this.updateFail(data.message);
			}
		},
		async removeImageTeam(payload) {
			this.resetState();
			const { data } = await ServiceRemoveImageTeam(payload);
			if (!data.error) {
				this.updateDetail({
					detail_teams: data,
					isAuthenticate: true
				});
				this.updateSuccess('Saved Changes');
			} else {
				this.updateFail(data.message);
			}
		},
		async deleteTeam(payload) {
			this.resetState();
			const { data } = await ServiceDeleteTeam(payload);
			if (!data.error) {
				// this.updateDetail({
				// 	detail_teams: data,
				// 	isAuthenticate: true
				// });
				this.deleteSuccess('Team Deleted');
				// this.deleteTeam()
			} else {
				this.updateFail('Team failed to deleted, please try again');
			}
		},
		async joinTeam(payload) {
			this.resetState();
			const { data } = await ServiceJoinTeam(payload);
			if (!data.error) {
				this.updateSuccess(data.message);
			} else {
				this.updateFail('Failed to join the team, please try again');
			}
		},
		async editRoleMember(payload) {
			this.resetState();
			const { data } = await ServiceEditRoleMember(payload);
			if (!data.error) {
				this.updateSuccess(data.message);
			} else {
				this.updateFail(
					'Member role failed to change, please try again'
				);
			}
		},
		async removeMember(payload) {
			this.resetState();
			const { data } = await ServiceRemoveMember(payload);
			if (!data.error) {
				this.updateSuccess(data.message);
			} else {
				this.updateFail('Member failed to remove, please try again');
			}
		},
		async inviteMember(payload) {
			this.resetState();
			const { data } = await ServiceInviteMember(payload);
			if (!data.error) {
				this.updateSuccess('Member added successfully');
			} else {
				this.updateFail('Member failed to add, please try again');
			}
		}
	})
};
