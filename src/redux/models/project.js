import {
	serviceAddProject,
	getProjectTeam,
	serviceGetDeleteProjectTeam,
	serviceDeleteProjectTeam,
	servicePinProjectTeam,
	serviceUnpinProjectTeam,
	serviceGetPinProjectTeam,
	serviceGetProjectID,
	serviceRestoreProject,
	serviceDeleteRecently,
	serviceGetManyProjectTeam,
	serviceDuplicateProject,
	serviceUpdateProject
} from '../../service/project';

export const projectModel = {
	state: {
		error: '',
		errorMessageProject: '',
		successMessageProject: '',
		isUpdateSuccessProject: false,
		isDeleteProject: false,
		isRestoreProject: false,
		isPinProject: false,
		list_project: [],
		project_team: [],
		project_pinned: [],
		project_all_user: [],
		project_delete: [],
		project_by_id: [],
		project_create: [],

	},
	reducers: {
		requestSuccess(state) {
			return {
				...state,
				error: ''
			};
		},
		requestFail(state, error) {
			return {
				...state,
				error
			};
		},
		updateViewProjectAllUser(state, projectAllUser) {
			return {
				...state,
				project_all_user: [...projectAllUser]
			};
		},
		updateProjectTeams(state, projectTeam) {
			return {
				...state,
				project_team: [...state.project_team, ...projectTeam]
			};
		},
		updateViewProjectTeams(state, projectTeam) {
			return {
				...state,
				project_team: [...projectTeam]
			};
		},
		updateViewProjectDelete(state, projectDelete) {
			return {
				...state,
				project_delete: [...projectDelete]
			};
		},
		updateListProject(state, projectUser) {
			return {
				...state,
				list_project: [...state.list_project, ...projectUser]
			};
		},
		updateProjectPinned(state, projectPinned) {
			return {
				...state,
				project_pinned: [...projectPinned]
			};
		},
		updateProjectId(state, projectId) {
			return {
				...state,
				project_by_id: [...projectId]
			};
		},
		updateProjectCreate(state, projectCreate) {
			return {
				...state,
				project_create: [...projectCreate]
			};
		},
		deleteSuccess(state, successMessageProject) {
			return {
				...state,
				isUpdateSuccessProject: true,
				error: '',
				successMessageProject,
				isDeleteProject: true
			};
		},
		restoreSuccess(state, successMessageProject) {
			return {
				...state,
				isUpdateSuccessProject: true,
				error: '',
				successMessageProject,
				isRestoreProject: true
			};
		},
		pinSuccess(state, successMessageProject) {
			return {
				...state,
				isUpdateSuccessProject: true,
				error: '',
				successMessageProject,
				isPinProject: true
			};
		},
		updateSuccess(state, successMessageProject) {
			return {
				...state,
				isUpdateSuccessProject: true,
				error: '',
				successMessageProject
			};
		},
		updateFail(state, error) {
			return {
				...state,
				isUpdateSuccessProject: false,
				error,
				errorMessageProject: error
			};
		},
		resetState(state) {
			return {
				...state,
				errorMessageProject: '',
				successMessageProject: '',
				isUpdateSuccess: false,
				pinSuccess: false,
				isDeleteProject: false,
				isRestoreProject: false
			};
		},
		clearMessage(state) {
			return {
				...state,
				errorMessageProject: '',
				successMessageProject: '',
			};
		},
		emptyPinProject(state) {
			return {
				...state,
				project_by_id: []
			}
		},
		emptyProject(state) {
			return {
				...state,
				project_by_id: [],
				project_all_user: [],
				project_pinned: []
			}
		}
	},
	effects: () => ({
		async getProjectByTeam(payload) {
			this.resetState()
			const { data } = await getProjectTeam(payload);
			if (!data.error) {
				this.updateViewProjectTeams(data);
				this.requestSuccess();
			} else {
				this.requestFail(data.message);
			}
		},
		async getAllUserProjectByTeam(payload) {
			this.resetState();
			const { data } = await serviceGetManyProjectTeam(payload);
			if (!data.error) {
				this.updateViewProjectAllUser([data]);
				this.requestSuccess();
			} else {
				this.requestFail("Failed to get your project");
			}
		},
		async addProject(payload) {
			this.resetState()
			const { data } = await serviceAddProject(payload);
			if (!data.error) {
				this.requestSuccess();
				this.updateProjectTeams([data]);
				this.updateProjectCreate([data]);
			} else {
				this.requestFail(data.message);
			}
		},
		async duplicateProject(payload) {
			this.resetState()
			const { data } = await serviceDuplicateProject (payload);
			if (!data.error) {
				this.requestSuccess();
				this.updateProjectTeams([data]);
				this.updateSuccess('Your project has successfully duplicated');
			} else {
				this.requestFail(data.message);
			}
		},
		async getProjectDeleteByTeam(payload) {
			this.resetState()
			const { data } = await serviceGetDeleteProjectTeam(payload);
			if (!data.error) {
				this.updateViewProjectDelete(data);
				this.requestSuccess();
			} else {
				this.requestFail(data.message);
			}
		},
		async deleteProjectTeam(payload) {
			this.resetState()
			const { data } = await serviceDeleteProjectTeam(payload);
			if (!data.error) {
				const { data } = await getProjectTeam(payload);
				this.updateViewProjectTeams(data);
				this.deleteSuccess("Project Deleted");
			} else {
				this.updateFail(data.message);
			}
		},
		async restoreProjectTeam(payload) {
			this.resetState()
			const { data } = await serviceRestoreProject(payload);
			if (!data.error) {
				this.restoreSuccess("Project Restored");
			} else {
				this.updateFail(data.message);
			}
		},
		async deletedRecentlyProject(payload) {
			this.resetState()
			const { data } = await serviceDeleteRecently(payload);
			if (!data.error) {
				this.deleteSuccess("Project Deleted Successfully");
			} else {
				this.updateFail(data.message);
			}
		},
		async pinProjectTeam(payload) {
			this.resetState()
			const { data } = await servicePinProjectTeam(payload);
			if (!data.error) {
				this.updateProjectPinned([data]);
				this.pinSuccess("Project has pinned successfully");
			} else {
				this.updateFail(data.message);
			}
		},
		async unpinProjectTeam(payload) {
			this.resetState()
			const { data } = await serviceUnpinProjectTeam(payload);
			if (!data.error) {
				const { data } = await serviceGetPinProjectTeam();
				if (!data.error) {
					this.updateProjectPinned(data);
					this.pinSuccess("Project has unpinned successfully");
				} else {
					this.updateFail(data.message);
				}
			} else {
				this.updateFail(data.message);
			}
		},
		async getPinProjectTeam() {
			this.resetState()
			const { data } = await serviceGetPinProjectTeam();
			if (!data.error) {
				this.updateProjectPinned(data);
				this.requestSuccess();
			} else {
				this.updateFail(data.message);
			}
		},
		async getProjectId(payload) {
			this.resetState()
			let array = [];
			let array2 = [];
			if (payload.length > 0) {
				for (let i = 0; i < payload.length; i++) {
					const { data } = await serviceGetProjectID(payload[i]['project']);
					if (data.length > 0) {
						array.push(data)
					}
				}
				for (let i = 0; i < array.length; i++) {
					for (let j = 0; j < array[i].length; j++) {
						array2.push(array[i][j])
					}
				}
				this.updateProjectId(array2)
				this.requestSuccess();
			}
			else {
				this.updateFail("Failed to get your project");
			}
		},
		async updateProject(payload) {
			this.resetState()
			const { data } = await serviceUpdateProject(payload);
			if (!data.error) {
				this.updateSuccess('Update Project Successfully');
			} else {
				this.updateFail('Update Project Failed');
			}
		},
	})
};
