import { 
    serviceCreateFeedback
} from "../../service/feedback";

export const feedbackModel = {
	state: {
		error: '',
		errorMessageFeedback: '',
		successMessageFeedback: '',
		isUpdateSuccessFeedback: false,
	},
	reducers: {
		requestSuccess(state) {
			return {
				...state,
				error: ''
			};
		},
		requestFail(state, error) {
			return {
				...state,
				error
			};
		},
		
		updateSuccess(state, successMessageFeedback) {
			return {
				...state,
				isUpdateSuccessFeedback: true,
				error: '',
				successMessageFeedback
			};
		},
		updateFail(state, error) {
			return {
				...state,
				isUpdateSuccessFeedback: false,
				error,
				errorMessageFeedback: error
			};
		},
		resetState(state) {
			return {
				...state,
				errorMessageFeedback: '',
				successMessageFeedback: '',
				isUpdateSuccessFeedback: false,
			};
		},
		clearMessage(state) {
			return {
				...state,
				errorMessageFeedback: '',
				successMessageFeedback: '',
			};
		},
	},
	effects: () => ({
		async addFeedback(payload) {
			this.resetState()
			const { data } = await serviceCreateFeedback(payload);
			if (!data.error) {
				this.requestSuccess();
                this.updateSuccess('Feedback successfully send')
			} else {
				this.requestFail(data.message);
			}
		},
	})
};