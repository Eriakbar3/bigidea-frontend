import {
	serviceGetBoard,
	serviceUpdateBoard,
	serviceGetBoardMany
} from '../../service/whiteboard';

export const canvasModel = {
	state: {
		cursor: 'default',
		tool: 'select',
		action: 'none',
		elements: [],
		initEl: null,
		selectedElement: null,
		scale: 1,
		offset: { x: 0, y: 0 },
		zoomPoint: { x: window.innerWidth / 2, y: window.innerHeight / 2 },
		saved: false,
		boardExist: true,
		dataBoard: [],
		dataBoardMany: []
	},
	reducers: {
		setScale(state, scale) {
			return {
				...state,
				scale
			};
		},
		scaleUp(state, step, zoomPoint) {
			return {
				...state,
				scale:
					state.scale < 4
						? parseFloat((state.scale + step).toFixed(2))
						: state.scale,
				zoomPoint
			};
		},
		scaleDown(state, step, zoomPoint) {
			return {
				...state,
				scale:
					state.scale > 0.1
						? parseFloat((state.scale - step).toFixed(2))
						: state.scale,
				zoomPoint
			};
		},
		setZoomPoint(state, zoomPoint) {
			return {
				...state,
				zoomPoint
			};
		},
		setOffset(state, { x, y }) {
			return {
				...state,
				offset: {
					x: state.offset.x + x,
					y: state.offset.y + y
				}
			};
		},
		setCursor(state, cursor) {
			return {
				...state,
				cursor
			};
		},
		setTool(state, tool) {
			return {
				...state,
				tool
			};
		},
		setAction(state, action) {
			return {
				...state,
				action
			};
		},
		setElements(state, element) {
			return {
				...state,
				elements: [element, ...state.elements]
			};
		},
		updateElements(state, elements) {
			return {
				...state,
				elements: [...elements]
			};
		},
		setSelectedElement(state, selectedElement) {
			return {
				...state,
				selectedElement
			};
		},
		saveStatus(state, status) {
			return {
				...state,
				saved: status
			};
		},
		setBoardExist(state, boardExist, data) {
			return {
				...state,
				boardExist,
				dataBoard: [data]
			};
		},
		setBoardMany(state, data) {
			return {
				...state,
				dataBoardMany: [...data]
			};
		},
		setInitEl(state, initEl) {
			return {
				...state,
				initEl
			};
		}
	},
	effects: {
		async getBoard(project_id) {
			this.saveStatus(true);
			const { data } = await serviceGetBoard(project_id);
			if (data && !data.error) {
				this.updateElements(data.node);
				this.saveStatus(true);
				this.setBoardExist(true, data);
			} else this.setBoardExist(false, '');
		},
		async getDataBoard(payload) {
			const { data } = await serviceGetBoardMany(payload);
			if (data) {
				this.setBoardMany(data);
			} else {
				this.setBoardExist(false, '');
				this.setBoardMany([]);
			}
		},
		async updateBoard(project_id, state) {
			this.saveStatus(false);
			const { data } = await serviceUpdateBoard(project_id, {
				node: state.canvas.elements
			});
			if (data && !data.error) {
				this.saveStatus(true);
			}
		}
	}
};
