import { authModel } from './auth';
import { canvasModel } from './canvas';
import { teamModel } from './team';
import { projectModel } from './project';
import { feedbackModel } from './feedback';

export const auth = authModel;
export const team = teamModel;
export const project = projectModel;
export const canvas = canvasModel;
export const feedback = feedbackModel;
