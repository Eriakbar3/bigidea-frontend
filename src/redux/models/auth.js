import {
	getUserProfile,
	serviceSignin,
	serviceSignout,
	updateImageUserProfile,
	serviceSignup,
	serviceUpdateUserProfile,
	updateUserPassword,
	serviceRemoveImageProfile,
	serviceDeleteUser
} from '../../service/auth';

const initialState = {
	isLoginSuccess: false,
	isSignupSuccess: false,
	isUpdateSuccess: false,
	isAuthenticate: false,
	isDeleteUser: false,
	userData: null,
	error: '',
	errorMessage: '',
	successMessage: ''
};

export const authModel = {
	state: initialState,
	reducers: {
		loginSuccess(state) {
			return {
				...state,
				isLoginSuccess: true,
				isAuthenticate: true,
				error: ''
			};
		},
		loginFail(state, message) {
			return {
				...state,
				isLoginSuccess: false,
				isAuthenticate: false,
				userData: {},
				error: message
			};
		},
		signupSuccess(state) {
			return {
				...state,
				isSignupSuccess: true,
				error: ''
			};
		},
		signupFail(state, message) {
			return {
				...state,
				isSignupSuccess: false,
				error: message
			};
		},
		updateSuccess(state, successMessage) {
			return {
				...state,
				isUpdateSuccess: true,
				error: '',
				successMessage
			};
		},
		updateFail(state, error) {
			return {
				...state,
				isUpdateSuccess: false,
				error,
				errorMessage: error
			};
		},
		updateUser(state, payload) {
			return {
				...state,
				...payload
			};
		},
		updateError(state, error) {
			return {
				...state,
				error,
				errorMessage: error
			};
		},
		deleteSuccess(state, successMessage) {
			return {
				...state,
				isUpdateSuccess: true,
				error: '',
				isDeleteUser:true,
				successMessage
			};
		},
		resetState(state) {
			return {
				...state,
				errorMessage: '',
				successMessage: '',
				isUpdateSuccess: false,
				isDeleteUser:false
			};
		},
		clearMessage(state) {
			return {
				...state,
				errorMessage:'',
				successMessage:'',
			};
		},
		
	},
	effects: () => ({
		async login(payload) {
			const { data } = await serviceSignin(payload);
			if (!data.error) {
				localStorage.setItem('token', data.token);
				this.updateUser({
					userData: data.user
				});
				this.loginSuccess();
			} else {
				this.loginFail(data.message);
			}
		},
		async signup(payload) {
			const { data } = await serviceSignup(payload);
			if (!data.error) {
				localStorage.setItem('token', data.token);
				this.signupSuccess();
			} else {
				this.signupFail(data.message);
			}
		},
		async signOut() {
			const { data } = await serviceSignout();
			if (!data.error) {
				this.updateUser(initialState);
				localStorage.removeItem('token');
			}
		},
		async userProfile() {
			const { data } = await getUserProfile();
			if (!data.error)
				this.updateUser({
					userData: data,
					isAuthenticate: true
				});
			else {
				localStorage.removeItem('token');
				this.updateUser({
					userData: null,
					isAuthenticate: false
				});
				this.updateError(data.message);
			}
		},
		async updateImageUser(payload) {
			this.resetState();
			const { data } = await updateImageUserProfile(payload);
			if (!data.error){
				this.updateUser({
					userData: data,
					isAuthenticate: true,
				});
				this.updateSuccess('Saved Changes');
			}else {
				this.updateUser({
					userData: null,
					isAuthenticate: false
				});
				localStorage.removeItem('token');
			}
		},
		async updateUserProfile(payload) {
			this.resetState();
			const { data } = await serviceUpdateUserProfile(payload);
			if (!data.error) {
				this.updateUser({
					userData: data
				});
				this.updateSuccess('Saved Changes');
			} else {
				this.updateFail(data.message);
			}
		},
		async updatePassword(payload) {
			this.resetState();
			const { data } = await updateUserPassword(payload);
			if (!data.error) {
				this.updateSuccess("Password Changed");
			} else {
				this.updateFail(data.message);
			}
		},
		async removeImageProfile(payload) {
			this.resetState();
			const { data } = await serviceRemoveImageProfile(payload);
			if (!data.error) {
				this.updateUser({
					userData: data,
					isAuthenticate: true
				});
				this.updateSuccess('Saved Changes');
			} else {
				this.updateFail(data.message);
			}
		},
		async deleteUser(payload) {
			const { data } = await serviceDeleteUser(payload);
			if (!data.error) {
				await serviceSignout();
				this.deleteSuccess('Account Deleted')
				this.updateUser(initialState);
				localStorage.removeItem('token');
			} else {
				this.updateFail('Account failed to deleted, please try again');
			}
		}
	})
};
