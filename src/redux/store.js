import { init } from '@rematch/core';
import loadingPlugin from '@rematch/loading';
import * as models from './models/index';

const store = init({
	models,
	plugins: [loadingPlugin()]
});

export default store;
