import React from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import Dashboard from './pages/dashboard';
import Whiteboard from './pages/whiteboard';
import MyProjects from './pages/dashboard/MyProjects';
import Pinned from './pages/dashboard/Pinned';
import Team from './pages/dashboard/Team/index';
import Home from './pages/home';
import PageNotfound from './pages/PageNotfound';
import Signin from './pages/Signin';
import Signup from './pages/Signup';
import MyProfile from './pages/MyProfile';
import { Provider } from 'react-redux';
import store from './redux/store';
import OnBoarding from './pages/onboarding/index.js';
import Example from './pages/examples/index.js';

function App() {
	return (
		<Provider store={store}>
			<BrowserRouter>
				<Routes>
					<Route path="/" element={<Home />} />
					<Route path="/signin" element={<Signin />} />
					<Route path="/signup" element={<Signup />} />
					<Route path="/onboarding" element={<OnBoarding />} />
					<Route path="/dashboard" element={<Dashboard />}>
						<Route
							path="/dashboard/my-projects"
							element={<MyProjects />}
						/>
						<Route path="/dashboard/pinned" element={<Pinned />} />
						<Route path="/dashboard/team/:id" element={<Team />} />
						<Route
							path="/dashboard"
							element={
								<Navigate to="/dashboard/my-projects" replace />
							}
						/>
					</Route>
					<Route path="/my-profile" element={<MyProfile />} />
					<Route
						path="/whiteboard/project/:id"
						element={<Whiteboard />}
					/>
					<Route path="/examples" element={<Example />} />
					<Route path="*" element={<PageNotfound />} />
				</Routes>
			</BrowserRouter>
		</Provider>
	);
}

export default App;
