import { getAuthorizationHeader, instance } from '../axios';
import {
	createProjectPath,
	getProjectByTeamPath,
	getDeleteProjectTeamPath,
	deleteProjectPath,
	pinProjectPath,
	unpinProjectPath,
	getPinProjectPath,
	getProjectByIdPath,
	getProjectsDetailTeamPath,
	restoreProjectPath,
	DeletedRecentlyPath,
	GetManyProjectTeamPath,
	duplicateProjectPath,
	updateProjectPath
} from '../path';

export const serviceAddProject = (payload) =>
	instance({
		url: createProjectPath,
		method: 'post',
		data: payload,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const getProjectTeam = (params) =>
	instance({
		url: getProjectByTeamPath + '/' + params,
		method: 'get',
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceGetDeleteProjectTeam = (params) =>
	instance({
		url: getDeleteProjectTeamPath + '/' + params,
		method: 'get',
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceDeleteProjectTeam = (params) =>
	instance({
		url: deleteProjectPath + '/' + params,
		method: 'post',
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const servicePinProjectTeam = (params) =>
	instance({
		url: pinProjectPath + '/' + params,
		method: 'post',
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceUnpinProjectTeam = (params) =>
	instance({
		url: unpinProjectPath + '/' + params,
		method: 'post',
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceGetPinProjectTeam = () =>
	instance({
		url: getPinProjectPath,
		method: 'get',
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceGetProjectID = (params) =>
	instance({
		url: getProjectByIdPath + '/' + params,
		method: 'get',
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceGetProjectDetailTeam = (params) =>
	instance({
		url: getProjectsDetailTeamPath + '/' + params,
		method: 'get',
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceRestoreProject = (payload) =>
	instance({
		url: restoreProjectPath,
		method: 'post',
		data: payload,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceDeleteRecently = (payload) =>
	instance({
		url: DeletedRecentlyPath,
		method: 'delete',
		data: payload,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceGetManyProjectTeam = (payload) =>
	instance({
		url: GetManyProjectTeamPath,
		method: 'post',
		data: payload,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceDuplicateProject = (payload) =>
	instance({
		url: duplicateProjectPath,
		method: 'post',
		data: payload,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceUpdateProject = (payload) =>
	instance({
		url: updateProjectPath,
		method: 'put',
		data: payload,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

