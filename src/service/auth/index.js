import { getAuthorizationHeader, instance } from '../axios';
import {
	loginPath,
	profilePath,
	signoutPath,
	signupPath,
	updateUserProfilePath,
	updatePasswordPath,
	updateProfileImagePath,
	removeImageUserPath,
	deleteUserPath
} from '../path';
export const serviceSignin = ({ email, password }) =>
	instance({
		url: loginPath,
		method: 'post',
		data: { email, password }
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceSignup = ({ email, password }) =>
	instance({
		url: signupPath,
		method: 'post',
		data: { email, password }
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceSignout = () =>
	instance({
		url: signoutPath,
		method: 'post',
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const getUserProfile = () =>
	instance({
		url: profilePath,
		method: 'get',
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const updateUserPassword = ({ password, currentPassword }) =>
	instance({
		url: updatePasswordPath,
		method: 'put',
		data: { password, currentPassword },
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const updateImageUserProfile = (payload) =>
	instance({
		url: updateProfileImagePath,
		method: 'put',
		data: payload,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceUpdateUserProfile = (data) =>
	instance({
		url: updateUserProfilePath,
		method: 'put',
		data,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceRemoveImageProfile = () =>
	instance({
		url: removeImageUserPath,
		method: 'post',
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceDeleteUser = (payload) =>
	instance({
		url: deleteUserPath,
		method: 'post',
		data: payload,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));
