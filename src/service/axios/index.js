import axios from 'axios';

let baseUrl =
	process.env.REACT_APP_MODE === 'local'
		? process.env.REACT_APP_API_HOST
		: process.env.REACT_APP_API_PROD_HOST;

export const getAuthorizationHeader = () =>
	`Bearer ${localStorage.getItem('token')}`;

export const instance = axios.create({
	baseURL: baseUrl,
	timeout: 30000
});

instance.interceptors.response.use(
	(response) => response,
	(error) => {
		if (error.response) {
			throw error.response.data;
		} else {
			throw error;
		}
	}
);

// cancel request (https://axios-http.com/docs/cancellation)
export const controller = new AbortController();
