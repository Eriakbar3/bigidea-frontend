import { getAuthorizationHeader, instance } from '../axios';
import { createFeedbackPath } from '../path';

export const serviceCreateFeedback = (payload) =>
	instance({
		url: createFeedbackPath,
		method: 'post',
		data:payload,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

