import { getAuthorizationHeader, instance } from '../axios';

export const serviceGetBoard = (project_id) =>
	instance({
		url: `api/board/${project_id}`,
		method: 'get',
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceGetBoardMany = (payload) =>
	instance({
		url: `api/board`,
		method: 'post',
		data:payload,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const serviceUpdateBoard = (project_id, data) =>
	instance({
		url: `api/board/${project_id}`,
		method: 'put',
		data,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));
