import { getAuthorizationHeader, instance } from '../axios';
import {
	addTeamPath,
	getListTeamsPath,
	getTeamPath,
	getDetailTeamPath,
	updateTeamProfilePath,
	removeImageTeamPath,
	deleteTeamPath,
	joinTeamPath,
	editRoleMemberPath,
	removeMemberPath,
	inviteMemberPath
} from '../path';

export const serviceAddTeam = (data) =>
	instance({
		url: addTeamPath,
		method: 'post',
		data,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const getListTeams = () =>
	instance({
		url: getListTeamsPath,
		method: 'get',
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const getTeamMember = (params) =>
	instance({
		url: getTeamPath + '/' + params,
		method: 'get',
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const getDetailTeam = (params) =>
	instance({
		url: getDetailTeamPath + '/' + params,
		method: 'get',
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const updateTeam = (payload) =>
	instance({
		url: updateTeamProfilePath,
		method: 'put',
		data: payload,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const ServiceRemoveImageTeam = (payload) =>
	instance({
		url: removeImageTeamPath,
		method: 'post',
		data: payload,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const ServiceDeleteTeam = (payload) =>
	instance({
		url: deleteTeamPath,
		method: 'post',
		data: payload,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const ServiceJoinTeam = (payload) =>
	instance({
		url: joinTeamPath,
		method: 'post',
		data: payload,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const ServiceEditRoleMember = (payload) =>
	instance({
		url: editRoleMemberPath,
		method: 'put',
		data: payload,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const ServiceRemoveMember = (payload) =>
	instance({
		url: removeMemberPath,
		method: 'delete',
		data: payload,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));

export const ServiceInviteMember = (payload) =>
	instance({
		url: inviteMemberPath,
		method: 'post',
		data: payload,
		headers: {
			Authorization: getAuthorizationHeader()
		}
	})
		.then((res) => Promise.resolve(res))
		.catch((err) => Promise.reject(err));


