/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./src/**/*.{js,jsx,ts,tsx}'],
	theme: {
		extend: {
			colors: {
				'neutral-10': '#FFFFFF',
				'neutral-20': '#F6F6F6',
				'neutral-30': '#EFEFEF',
				'neutral-40': '#E4E4E4',
				'neutral-50': '#C9C9C9',
				'neutral-60': '#AAAAAA',
				'neutral-70': '#858585',
				'neutral-80': '#737373',
				'neutral-100': '#1E1E1E',
				primary: '#0549CF',
				'primary-40': '#E1E9F9',
				'focus-primary': '#CDDBF5',
				'surface-primary': '#F0F5FF',
				'border-primary': '#ACC2EF',
				danger: '#F54A45',
				success: '#03A08B',
				'surface-success': '#F0FFFD',
				'outline-success': '#03A08B'
			},
			dropShadow: {
				xs: '0px 1px 2px rgba(9, 45, 115, 0.12)'
			}
		}
	},
	plugins: []
};
